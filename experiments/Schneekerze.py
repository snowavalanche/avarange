# -*- coding: utf-8 -*-
"""
Created on Mon May 30 16:57:28 2022

@author: neuhauser
"""

import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from datetime import datetime, time
import matplotlib.dates as dates

# Stationäre Messung Schneekerze
path_sk = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-05-30_static_temp_Seegrube\ds18b20_log.csv"
sk_data = pd.read_csv(path_sk, delimiter=(','))
sk_data = sk_data.replace(-9999, np.NaN)

sk_data["Date"] = pd.to_datetime(sk_data["Date"], format='%d/%m/%Y %H:%M:%S')
date = dates.date2num(sk_data["Date"])


fig, ax = plt.subplots()

date_form = dates.DateFormatter("%d/%m")
ax.xaxis.set_major_formatter(date_form)
fig.autofmt_xdate(rotation=45)

color_list = []
for i in range(15):
    color = (0, 0+i/15, 1-i/15)
    color_list.append(color)
for i in range(15):
    color = (0+i/15, 1-i/15, 0)
    color_list.append(color)

ax.plot(date, sk_data["0"], label="0 cm", color=color_list[0])
ax.plot(date, sk_data["10"], label="10 cm", color=color_list[1])
ax.plot(date, sk_data["20"], label="20 cm", color=color_list[2])
ax.plot(date, sk_data["30"], label="30 cm", color=color_list[3])
ax.plot(date, sk_data["40"], label="40 cm", color=color_list[4])
ax.plot(date, sk_data["50"], label="50 cm", color=color_list[5])
ax.plot(date, sk_data["55"], label="55 cm", color=color_list[6])
ax.plot(date, sk_data["60"], label="60 cm", color=color_list[7])
ax.plot(date, sk_data["65"], label="65 cm", color=color_list[8])
ax.plot(date, sk_data["70"], label="70 cm", color=color_list[9])
ax.plot(date, sk_data["75"], label="75 cm", color=color_list[10])
ax.plot(date, sk_data["80"], label="80 cm", color=color_list[11])
ax.plot(date, sk_data["85"], label="85 cm", color=color_list[12])
ax.plot(date, sk_data["90"], label="90 cm", color=color_list[13])
ax.plot(date, sk_data["95"], label="95 cm", color=color_list[14])
ax.plot(date, sk_data["100"], label="100 cm", color=color_list[15])
ax.plot(date, sk_data["105"], label="105 cm", color=color_list[16])
ax.plot(date, sk_data["110"], label="110 cm", color=color_list[17])
ax.plot(date, sk_data["115"], label="115 cm", color=color_list[18])
ax.plot(date, sk_data["120"], label="120 cm", color=color_list[19])
ax.plot(date, sk_data["125"], label="125 cm", color=color_list[20])
ax.plot(date, sk_data["130"], label="130 cm", color=color_list[21])
ax.plot(date, sk_data["135"], label="135 cm", color=color_list[22])
ax.plot(date, sk_data["140"], label="140 cm", color=color_list[23])
ax.plot(date, sk_data["145"], label="145 cm", color=color_list[24])
ax.plot(date, sk_data["150"], label="150 cm", color=color_list[25])
ax.plot(date, sk_data["160"], label="160 cm", color=color_list[26])
ax.plot(date, sk_data["170"], label="170 cm", color=color_list[27])
ax.plot(date, sk_data["180"], label="180 cm", color=color_list[28])
ax.plot(date, sk_data["190[cm]"], label="190 cm", color=color_list[29])

ax.grid()
ax.legend()