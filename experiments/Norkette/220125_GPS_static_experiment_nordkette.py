# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 10:22:03 2022

@author: neuhauser
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from geopy import distance

from classes.GPS_Class import GPSData
from classes.IMU_Class import ImuData

from classes.gps_imu_tools import transform_gps_to_local

# Node | Depth[cm]
# C05  | 0
# C06  | 50
# C07  | 100
# C04  | 150

#C05
gps_c05 = GPSData()
path_c05 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-01-25_Nordkette_GPS_snow\AvaNodes\C05\GPS\C05Gps002.txt"
gps_c05.path = path_c05
gps_c05.read_data()

long_c05 = 11.38079685
lat_c05 = 47.30754946

rtk_pos = (lat_c05, long_c05)
ds_c05 = []
for idx, lat in enumerate(gps_c05.data_pos["latitude[deg]"]):
    node_pos = (lat, gps_c05.data_pos["longitude[deg]"].iloc[idx])
    ds = distance.distance(rtk_pos, node_pos).m
    ds_c05.append(ds)

#C06
gps_c06 = GPSData()
path_c06 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-01-25_Nordkette_GPS_snow\AvaNodes\C06\GPS\C06Gps002.txt"
gps_c06.path = path_c05
gps_c06.read_data()

long_c06 = 11.38081259
lat_c06 = 47.30754739

rtk_pos = (lat_c06, long_c06)
ds_c06 = []
for idx, lat in enumerate(gps_c06.data_pos["latitude[deg]"]):
    node_pos = (lat, gps_c06.data_pos["longitude[deg]"].iloc[idx])
    ds = distance.distance(rtk_pos, node_pos).m
    ds_c06.append(ds)

#C07
gps_c07 = GPSData()
path_c07 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-01-25_Nordkette_GPS_snow\AvaNodes\C07\GPS\C07Gps002.txt"
gps_c07.path = path_c07
gps_c07.read_data()

long_c07 = 11.38082538
lat_c07 = 47.30754915

rtk_pos = (lat_c07, long_c07)
ds_c07 = []
for idx, lat in enumerate(gps_c07.data_pos["latitude[deg]"]):
    node_pos = (lat, gps_c07.data_pos["longitude[deg]"].iloc[idx])
    ds = distance.distance(rtk_pos, node_pos).m
    ds_c07.append(ds)

#C04
gps_c04 = GPSData()
path_c04 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-01-25_Nordkette_GPS_snow\AvaNodes\C04\GPS\C04Gps001.txt"
gps_c04.path = path_c04
gps_c04.read_data()

long_c04 = 11.38083684
lat_c04 = 47.30755049

rtk_pos = (lat_c04, long_c04)
ds_c04 = []
for idx, lat in enumerate(gps_c04.data_pos["latitude[deg]"]):
    node_pos = (lat, gps_c04.data_pos["longitude[deg]"].iloc[idx])
    ds = distance.distance(rtk_pos, node_pos).m
    ds_c04.append(ds)

# Preparing time format
#date_c05 = dates.date2num(gps_c05.data_pos[gps_c05.utc])
#date_c06 = dates.date2num(gps_c06.data_pos[gps_c06.utc])
#date_c07 = dates.date2num(gps_c07.data_pos[gps_c07.utc])
#date_c04 = dates.date2num(gps_c04.data_pos[gps_c04.utc])
# Define the date format
#date_form = dates.DateFormatter("%H:%M")

# =============================================================================
# #PLot Number of Satellites
# fig, ax = plt.subplots()
# ax.xaxis.set_major_formatter(date_form)
# 
# ax.scatter(date_c05, gps_c05.data_pos["numSV[]"], color='blue', label='0 cm', marker='x')
# ax.scatter(date_c06, gps_c06.data_pos["numSV[]"], color='green', label = '50 cm')
# ax.scatter(date_c07, gps_c07.data_pos["numSV[]"], color='orange', label='100 cm')
# ax.scatter(date_c04, gps_c04.data_pos["numSV[]"], color='red', label='150 cm')
# 
# ax.grid()
# ax.legend()
# 
# # Plot Psoition Deviation
# fig, ax = plt.subplots()
# 
# ax.plot(ds_c05, color='blue', label='0 cm')
# ax.plot(ds_c06, color='green', label = '50 cm')
# ax.plot(ds_c07, color='orange', label='100 cm')
# ax.plot(ds_c04, color='red', label='150 cm')
# 
# ax.legend()
# =============================================================================

# Boxplot Pos. Deviation
data = [ds_c05, ds_c06, ds_c07, ds_c04]
labels = ['0 cm', '50 cm', '100 cm', '150 cm']

fig1, ax1 = plt.subplots()
ax1.hlines(2.5,0.5,4.5, color='red')
bp = ax1.boxplot(data,
            vert=True,  # vertical box alignment
            patch_artist=True,  # fill with color
            labels=labels,
            showfliers=False)  # will be used to label x-ticks

ax1.set_xlabel('snow cover')
ax1.set_ylabel('Deviation [m]')
#ax1.set_xticklabels([5, 6, 7, 8, 9, 10])
ax1.grid()
ax1.set_title('Deviation of GNSS Position')
#ax1.set_ylim([-1,26])
fig1.savefig('220125_pos_deviation.png', dpi=300)

medians = [item.get_ydata()[0] for item in bp['medians']]
means = [item.get_ydata()[0] for item in bp['means']]
print(f'Medians: {medians}\n'
      f'Means:   {means}')

# Boxplot Number of Satellites
data = [gps_c05.data_pos["numSV[]"], gps_c06.data_pos["numSV[]"], gps_c07.data_pos["numSV[]"], gps_c04.data_pos["numSV[]"]]
labels = ['0 cm', '50 cm', '100 cm', '150 cm']

fig1, ax1 = plt.subplots()
ax1.boxplot(data,
            vert=True,  # vertical box alignment
            patch_artist=True,  # fill with color
            labels=labels,
            showfliers=False)  # will be used to label x-ticks
ax1.set_xlabel('snow cover')
ax1.set_ylabel('Nr. of Sat.')
#ax1.set_xticklabels([5, 6, 7, 8, 9, 10])
ax1.grid()
ax1.set_title('Number of Satellites')
fig1.savefig('220125_nrsat_deviation.png', dpi=300)

# Boxplot Velocity
c05_vel = np.linalg.norm([gps_c05.data_pos["velD[mm/s]"].values, gps_c05.data_pos["velE[mm/s]"].values, gps_c05.data_pos["velN[mm/s]"].values], axis=0)/1000
c06_vel = np.linalg.norm([gps_c06.data_pos["velD[mm/s]"].values, gps_c06.data_pos["velE[mm/s]"].values, gps_c06.data_pos["velN[mm/s]"].values], axis=0)/1000
c07_vel = np.linalg.norm([gps_c07.data_pos["velD[mm/s]"].values, gps_c07.data_pos["velE[mm/s]"].values, gps_c07.data_pos["velN[mm/s]"].values], axis=0)/1000
c04_vel = np.linalg.norm([gps_c04.data_pos["velD[mm/s]"].values, gps_c04.data_pos["velE[mm/s]"].values, gps_c04.data_pos["velN[mm/s]"].values], axis=0)/1000

data = [c05_vel, c06_vel, c07_vel, c04_vel]
labels = ['0 cm', '50 cm', '100 cm', '150 cm']

fig1, ax1 = plt.subplots()
bp2 = ax1.boxplot(data,
            vert=True,  # vertical box alignment
            patch_artist=True,  # fill with color
            labels=labels,
            showfliers=False)  # will be used to label x-ticks
ax1.set_xlabel('snow cover')
ax1.set_ylabel('Velocity [m/s]')
#ax1.set_xticklabels([5, 6, 7, 8, 9, 10])
ax1.grid()
ax1.set_title('Velocity Deviation')
#fig1.savefig('220125_vel_deviation.png', dpi=300)

medians2 = [item.get_ydata()[0] for item in bp2['medians']]
means2 = [item.get_ydata()[0] for item in bp2['means']]
print(f'Medians: {medians2}\n'
      f'Means:   {means2}')