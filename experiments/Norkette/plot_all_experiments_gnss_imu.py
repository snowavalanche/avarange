# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 11:56:08 2022

@author: neuhauser
"""

#%% Libs
import matplotlib.pyplot as plt
from datetime import datetime
import numpy as np

from classes.GPS_Class import GPSData
from classes.IMU_Class import ImuData

#%% Settings

save_path = r"C:\git_rep\dissertation\papers\GPS_paper\figures\exp"
# Colors
color_imu = 'black'
color_doppler = 'red'
color_position = 'blue'
color_rotation = 'green'
# Linestyles
ls_imu = 'solid'
ls_pos = 'dashed'
ls_doppler = 'dotted'
# Save
save = False

#%% 15.03.2022 Seilbahn

# Experiment 15.03.2021
# C01
# Seilbahnrinne
gps_c01 = GPSData()
path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-15_Nordkette_avalanche\C01\GPS\ava210315_C01.txt'
gps_c01.path = path_c01
gps_c01.read_data()
gps_c01.calc_pos_vel()

imu_c01 = ImuData()
imu_path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-15_Nordkette_avalanche\C01\Leader\ava210315_C01_Leader.txt'
imu_c01.path = imu_path_c01
imu_c01.read_data_pd()

# Calcuations 
gps_acc = gps_c01.data_pos['v_total_pos'].diff() / 0.1

# Settings
imu_start_idx = 14800
imu_end_idx = 32400
#imu_start_idx = 0
#imu_end_idx = -1
imu_time = imu_c01.time[imu_start_idx:imu_end_idx]
gps_time = gps_c01.data['#Data contains: GPS time of week of the navigation epoch (ms)'] / 1000 - gps_c01.data['#Data contains: GPS time of week of the navigation epoch (ms)'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]

fig, ((ax_acc, ax_vel, ax_dist)) = plt.subplots(3,1, figsize=(30, 15))
ax_gyro = ax_acc.twinx()

ax_gyro.set_ylim([-50,2550])
ax_acc.set_ylim([-50, 220])
ax_vel.set_ylim([-0.5, 22])
ax_dist.set_ylim([-5, 450])

# Plots
acc1, = ax_acc.plot(imu_time, imu_c01.acc_tot[imu_start_idx:imu_end_idx] - 9.81, color=color_imu, linestyle=ls_imu, label='IMU Acc')
acc2, = ax_acc.plot(gps_time, gps_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='GNSS Acc')

gyro1, = ax_gyro.plot(imu_time, imu_c01.gyro_tot[imu_start_idx:imu_end_idx], color=color_rotation, label='Gyro')

ax_vel.plot(gps_time, gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos)

ax_dist.plot(gps_time, gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos)

print("Total Distance = " , gps_c01.data_pos['tot_dist'].iloc[int(imu_end_idx/40)])

# Title
#ax_acc.set_title('Accelerometer')
#ax_gyro.set_title('Gyrometer')
#ax_vel.set_title('Velocity')
#ax_dist.set_title('Distance')
fig.suptitle('Avalanche 15.03.2021 - AvaNode C01 - Seilbahnrinne')

# Axes
ax_acc.set_ylabel('Acceleration [m/s²]')
#ax_acc.set_xlabel('Time [sec]')
ax_gyro.set_ylabel('Rotation [°/s]')

ax_vel.set_ylabel('Velocity [m/s]')
ax_dist.set_ylabel('Distance [m]')

ax_dist.set_xlabel('Time [sec]')

# Legend
ax_acc.legend()
ax_acc.legend([acc1, acc2, gyro1], ["IMU Acc", "Pos. Acc", "Rotation"])

# Save

fig.tight_layout()
if save:
    fig.savefig(save_path + "/210315_Seilbahnrinne_C01_alldata.png", dpi=300)
# Time if needed
start = datetime(2021,3,15,7,51,15)
end = datetime(2021,3,15,7,52,30)
# Compare Plot
fig_compare, ax_comp = plt.subplots()
s = gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()

ax_comp.scatter(time,v, label='2021.03.15 - Seilbahn')
ax_comp.set_xlabel('s/s_max')
ax_comp.set_xlabel('t/t_max')
ax_comp.set_ylabel('v/v_max')


#%% 15.03.2022 Julius

# Juliusrinne
gps_c01 = GPSData()
path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-15_Nordkette_avalanche\C01\GPS\210315_julius_C01.txt'
gps_c01.path = path_c01
gps_c01.read_data()
gps_c01.calc_pos_vel()

imu_c01 = ImuData()
imu_path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-15_Nordkette_avalanche\C01\Leader\210315_julius_C01_imu.txt'
imu_c01.path = imu_path_c01
imu_c01.read_data_pd()

# Calcuations 
gps_acc = gps_c01.data_pos['v_total_pos'].diff() / 0.1

# Settings
imu_start_idx = 150*400
imu_end_idx = 230*400
#imu_start_idx = 0
#imu_end_idx = -1
imu_time = imu_c01.time[imu_start_idx:imu_end_idx]
gps_time = gps_c01.data['#Data contains: GPS time of week of the navigation epoch (ms)'] / 1000 - gps_c01.data['#Data contains: GPS time of week of the navigation epoch (ms)'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]

fig, ((ax_acc, ax_vel, ax_dist)) = plt.subplots(3,1, figsize=(30, 15))
ax_gyro = ax_acc.twinx()

ax_gyro.set_ylim([-50,2550])
ax_acc.set_ylim([-50, 220])
ax_vel.set_ylim([-0.5, 22])
ax_dist.set_ylim([-5, 450])

# Plots
acc1, = ax_acc.plot(imu_time, imu_c01.acc_tot[imu_start_idx:imu_end_idx] - 9.81, color=color_imu, linestyle=ls_imu, label='IMU Acc')
acc2, = ax_acc.plot(gps_time, gps_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='GNSS Acc')

gyro1, = ax_gyro.plot(imu_time, imu_c01.gyro_tot[imu_start_idx:imu_end_idx], color=color_rotation, label='Rotation')

ax_vel.plot(gps_time, gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos)

ax_dist.plot(gps_time, gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos)

print("Total Distance = " , gps_c01.data_pos['tot_dist'].iloc[int(imu_end_idx/40)])

# Title
#ax_acc.set_title('Accelerometer')
#ax_gyro.set_title('Gyrometer')
#ax_vel.set_title('Velocity')
#ax_dist.set_title('Distance')
fig.suptitle('Avalanche 15.03.2021 - AvaNode C01 - Juliusrinne')

# Axes
ax_acc.set_ylabel('Acceleration [m/s²]')
#ax_acc.set_xlabel('Time [sec]')
ax_gyro.set_ylabel('Rotation [°/s]')

ax_vel.set_ylabel('Velocity [m/s]')
ax_dist.set_ylabel('Distance [m]')

ax_dist.set_xlabel('Time [sec]')

# Legend
ax_acc.legend()
ax_acc.legend([acc1, acc2, gyro1], ["IMU Acc", "Pos. Acc", "Rotation"])

# Save
fig.tight_layout()
if save:
    fig.savefig(save_path + "/210315_Juliusrinne_C01_alldata.png", dpi=300)

# Compare
s = gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()

#ax_comp.scatter(s,v, label='2021.03.15 - Julius')

#start = datetime(2021,3,15,8,32,30)
#end = datetime(2021,3,15,8,35,0)
#%% 16.03.2022 C01

# Experiment 16.03.2021
# C01
# Seilbahnrinne
gps_c01 = GPSData()
path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-16_Nordkette_avalanche\C01\GPS\ava210316_C01_GPS.txt'
gps_c01.path = path_c01
gps_c01.read_data()
gps_c01.calc_pos_vel()

imu_c01 = ImuData()
imu_path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-16_Nordkette_avalanche\C01\Leader\ava210316_C01_Leader.txt'
imu_c01.path = imu_path_c01
imu_c01.read_data_pd()

# Calcuations 
gps_acc = gps_c01.data_pos['v_total_pos'].diff() / 0.1

# Settings
imu_start_idx = 0
imu_end_idx = -1
imu_start_idx = 38*400
imu_end_idx = 85*400

imu_time = imu_c01.time[imu_start_idx:imu_end_idx]
gps_time = gps_c01.data['#Data contains: GPS time of week of the navigation epoch (ms)'] / 1000 - gps_c01.data['#Data contains: GPS time of week of the navigation epoch (ms)'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]

fig, ((ax_acc, ax_vel, ax_dist)) = plt.subplots(3,1, figsize=(30, 15))
ax_gyro = ax_acc.twinx()

ax_gyro.set_ylim([-50,2550])
ax_acc.set_ylim([-50, 220])
ax_vel.set_ylim([-0.5, 22])
ax_dist.set_ylim([-5, 450])

# Plots
acc1, = ax_acc.plot(imu_time, imu_c01.acc_tot[imu_start_idx:imu_end_idx] - 9.81, color=color_imu, linestyle=ls_imu, label='IMU Acc')
acc2, = ax_acc.plot(gps_time, gps_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='GNSS Acc')

gyro1, = ax_gyro.plot(imu_time, imu_c01.gyro_tot[imu_start_idx:imu_end_idx], color=color_rotation, label='Gyro')

ax_vel.plot(gps_time, gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos)

ax_dist.plot(gps_time, gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos)

print("Total Distance = " , gps_c01.data_pos['tot_dist'].iloc[int(imu_end_idx/40)]) 

# Title
#ax_acc.set_title('Accelerometer')
#ax_gyro.set_title('Gyrometer')
#ax_vel.set_title('Velocity')
#ax_dist.set_title('Distance')
fig.suptitle('Avalanche 16.03.2021 - AvaNode C01 - Seilbahnrinne')

# Axes
ax_acc.set_ylabel('Acceleration [m/s²]')
#ax_acc.set_xlabel('Time [sec]')
ax_gyro.set_ylabel('Rotation [°/s]')

ax_vel.set_ylabel('Velocity [m/s]')
ax_dist.set_ylabel('Distance [m]')

ax_dist.set_xlabel('Time [sec]')

# Legend
ax_acc.legend()
ax_acc.legend([acc1, acc2, gyro1], ["IMU Acc", "Pos. Acc", "Rotation"])

# Save
fig.tight_layout()
if save:
    fig.savefig(save_path + "/210316_Seilbahnrinne_C01_alldata.png", dpi=300)
    
# Compare
s = gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()

#ax_comp.scatter(time,v, label='2021.03.16 - Seilbahn - C01')

#%% 16.03.2022 C03
# C03
# Seilbahnrinne
gps_c03 = GPSData()
path_c03 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-16_Nordkette_avalanche\C03\GPS\ava210316_C03_GPS.txt'
gps_c03.path = path_c03
gps_c03.read_data()
gps_c03.calc_pos_vel()

imu_c03 = ImuData()
imu_path_c03 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-16_Nordkette_avalanche\C03\Leader\ava210316_C03_Leader.txt'
imu_c03.path = imu_path_c03
imu_c03.read_data_pd()

# Calcuations 
gps_acc = gps_c03.data_pos['v_total_pos'].diff() / 0.1

# Settings
imu_start_idx = 0
imu_end_idx = -1
imu_start_idx = 70*400
imu_end_idx = 115*400

imu_time = imu_c03.time[imu_start_idx:imu_end_idx]
gps_time = gps_c03.data['#Data contains:GPS time of week of the navigation epoch (ms)'] / 1000 - gps_c03.data['#Data contains:GPS time of week of the navigation epoch (ms)'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]

fig, ((ax_acc, ax_vel, ax_dist)) = plt.subplots(3,1, figsize=(30, 15))
ax_gyro = ax_acc.twinx()

ax_gyro.set_ylim([-50,2550])
ax_acc.set_ylim([-50, 220])
ax_vel.set_ylim([-0.5, 22])
ax_dist.set_ylim([-5, 450])

# Plots
acc1, = ax_acc.plot(imu_time, imu_c03.acc_tot[imu_start_idx:imu_end_idx] - 9.81, color=color_imu, linestyle=ls_imu, label='IMU Acc')
acc2, = ax_acc.plot(gps_time, gps_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='GNSS Acc')

gyro1, = ax_gyro.plot(imu_time, imu_c03.gyro_tot[imu_start_idx:imu_end_idx], color=color_rotation, label='Gyro')

ax_vel.plot(gps_time, gps_c03.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos)

ax_dist.plot(gps_time, gps_c03.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos)

print("Total Distance = " , gps_c03.data_pos['tot_dist'].iloc[int(imu_end_idx/40)]) 

# Title
#ax_acc.set_title('Accelerometer')
#ax_gyro.set_title('Gyrometer')
#ax_vel.set_title('Velocity')
#ax_dist.set_title('Distance')
fig.suptitle('Avalanche 16.03.2021 - AvaNode C03 - Seilbahnrinne')

# Axes
ax_acc.set_ylabel('Acceleration [m/s²]')
#ax_acc.set_xlabel('Time [sec]')
ax_gyro.set_ylabel('Rotation [°/s]')

ax_vel.set_ylabel('Velocity [m/s]')
ax_dist.set_ylabel('Distance [m]')

ax_dist.set_xlabel('Time [sec]')

# Legend
ax_acc.legend()
ax_acc.legend([acc1, acc2, gyro1], ["IMU Acc", "Pos. Acc", "Rotation"])

# Save
fig.tight_layout()
fig.savefig(save_path + "/210316_Seilbahnrinne_C03_alldata.png", dpi=300)


# Compare
s = gps_c03.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c03.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c03.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c03.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()
ax_comp.scatter(time,v, label='2021.03.16 - Seilbahn - C03')

#%% 23.01.2022

# Experiment 23.01.2022

gps_c01 = GPSData()
path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-01-23_Nordkette_avalanche\C01\GPS\ava220123_C01.txt'
gps_c01.path = path_c01
gps_c01.read_data()
gps_c01.calc_pos_vel()

imu_c01 = ImuData()
imu_path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-01-23_Nordkette_avalanche\C01\Leader\ava220123_C01_Leader.txt'
imu_c01.path = imu_path_c01
imu_c01.read_data_pd()

# Calcuations 
gps_acc = gps_c01.data_pos['v_total_pos'].diff() / 0.1
doppler_acc = gps_c01.data_pos['v_total'].diff() / 0.1

# Settings
imu_start_idx = 45*400
imu_end_idx = 95*400
#imu_start_idx = 0
#imu_end_idx = -1
imu_time = imu_c01.time[imu_start_idx:imu_end_idx]
gps_time = gps_c01.data['#iTow[ms]'] / 1000 - gps_c01.data['#iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]

fig, ((ax_acc, ax_gyro, ax_vel, ax_dist)) = plt.subplots(4,1, figsize=(30, 15))

ax_gyro.set_ylim([-50,2550])
ax_acc.set_ylim([-50, 220])
ax_vel.set_ylim([-0.5, 22])
ax_dist.set_ylim([-5, 450])

# Plots
# Acc
acc1, = ax_acc.plot(imu_time, imu_c01.acc_tot[imu_start_idx:imu_end_idx] - 9.81, color=color_imu, linestyle=ls_imu, label='IMU Acc')
acc2, = ax_acc.plot(gps_time, gps_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='GNSS Acc')
acc3, = ax_acc.plot(gps_time, doppler_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_doppler, linestyle=ls_doppler, label='Doppler Acc')
# Gyro
gyro1, = ax_gyro.plot(imu_time, imu_c01.gyro_tot[imu_start_idx:imu_end_idx], color=color_rotation, label='Gyro')
# Vel
ax_vel.plot(gps_time, gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='Pos. Vel.')
ax_vel.plot(gps_time, gps_c01.data_pos['v_total'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_doppler, linestyle=ls_doppler, label='Doppler Vel.')
# Dist
ax_dist.plot(gps_time, gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos)

print("Total Distance = " , gps_c01.data_pos['tot_dist'].iloc[int(imu_end_idx/40)]) 

# =============================================================================
# fps = 30
# duration = imu_time.iloc[-1] - imu_time.iloc[0]
# 
# for i in range(int(duration * fps)):
# #for i in range(2):
#     fig1, ax1 = plt.subplots(figsize=(16, 4.5))
#     #ax1.plot(imu_time, imu_c01.acc_X[imu_start_idx:imu_end_idx], color='r', linestyle=ls_imu, label='Acc. X')
#     #ax1.plot(imu_time, imu_c01.acc_Y[imu_start_idx:imu_end_idx], color='g', linestyle=ls_imu, label='Acc. Y')
#     #ax1.plot(imu_time, imu_c01.acc_Z[imu_start_idx:imu_end_idx], color='b', linestyle=ls_imu, label='Acc. Z')
#     ax1.plot(imu_time, imu_c01.acc_tot[imu_start_idx:imu_end_idx], color=color_imu, linestyle=ls_imu, label='Acc. tot.')
#     ax1.vlines(imu_time.iloc[0] + i / fps, -10, 200, color='r')
#     ax1.grid()
#     ax1.set_xlabel('Time in sec.')
#     ax1.set_ylabel('Acceleration in m/s²')
#     fig1.savefig(r'C:\Users\neuhauser\Videos\imc22\acc\Frame_{}'.format(i), transparent=True, dpi=120)
#     plt.close()
# 
# for i in range(int(duration * fps)):
# #for i in range(2):
#     fig2, ax2 = plt.subplots(figsize=(16, 4.5))
#     print(i)
#     ax2.set_ylim([-50,500])
#     ax2.plot(imu_time, imu_c01.gyro_tot[imu_start_idx:imu_end_idx], color=color_rotation, label='Gyro')
#     ax2.vlines(imu_time.iloc[0] + i / fps, -100, 1000, color='r')
#     ax2.grid()
#     ax2.set_xlabel('Time in sec.')
#     ax2.set_ylabel('Angular velocity in °/s')
#     fig2.savefig(r'C:\Users\neuhauser\Videos\imc22\gyro\Frame_{}'.format(i), transparent=True, dpi=120)
#     plt.close()
# =============================================================================

# Title
#ax_acc.set_title('Accelerometer')
#ax_gyro.set_title('Gyrometer')
#ax_vel.set_title('Velocity')
#ax_dist.set_title('Distance')
fig.suptitle('Avalanche 23.01.2022 - AvaNode C01 - Seilbahnrinne')

# Axes
ax_acc.set_ylabel('Acceleration [m/s²]')
#ax_acc.set_xlabel('Time [sec]')
ax_gyro.set_ylabel('Rotation [°/s]')

ax_vel.set_ylabel('Velocity [m/s]')
ax_dist.set_ylabel('Distance [m]')

ax_dist.set_xlabel('Time [sec]')

# Legend
ax_acc.legend()
ax_acc.legend([acc1, acc2, acc3], ["IMU Acc", "Pos. Acc", "Doppler Acc"])

ax_vel.legend()

# Save
fig.tight_layout()
fig.savefig(save_path + "/220123_Seilbahnrinne_C01_alldata.png", dpi=300)

# Compare
s = gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()
#ax_comp.scatter(time,v, label='2022.01.23 - Seilbahn - C01')

#start = datetime(2022,1,23,7,30,20)
#end = datetime(2022,1,23,7,31,30)
#%% 03.02.2022

# Experiment 03.02.2022
# C 10
# Seilbahnrinne
gps_c10 = GPSData()
path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-03_Nordkette_avalanche\C10\GPS\ava220203_seilbahn_C10_gnss.txt'
gps_c10.path = path_c10
gps_c10.read_data()
gps_c10.calc_pos_vel()

imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-03_Nordkette_avalanche\C10\Leader\ava220203_seilbahn_C10_imu.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Calcuations 
gps_acc = gps_c10.data_pos['v_total_pos'].diff() / 0.1
doppler_acc = gps_c10.data_pos['v_total'].diff() / 0.1

# Settings
imu_start_idx = 135*400
imu_end_idx = 190*400
#imu_start_idx = 0
#imu_end_idx = -1

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
gps_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]

fig, ((ax_acc, ax_gyro, ax_vel, ax_dist)) = plt.subplots(4,1, figsize=(30, 15))

ax_gyro.set_ylim([-50,2550])
ax_acc.set_ylim([-50, 220])
ax_vel.set_ylim([-0.5, 22])
ax_dist.set_ylim([-5, 450])

# Plots
# Acc
acc1, = ax_acc.plot(imu_time, imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81, color=color_imu, linestyle=ls_imu, label='IMU Acc')
acc2, = ax_acc.plot(gps_time, gps_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='GNSS Acc')
acc3, = ax_acc.plot(gps_time, doppler_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_doppler, linestyle=ls_doppler, label='Doppler Acc')
# Gyro
gyro1, = ax_gyro.plot(imu_time, imu_c10.gyro_tot[imu_start_idx:imu_end_idx], color=color_rotation, label='Gyro')

# Vel
ax_vel.plot(gps_time, gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='Pos. Vel.')
ax_vel.plot(gps_time, gps_c10.data_pos['v_total'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_doppler, linestyle=ls_doppler, label='Doppler Vel.')
# Dist
ax_dist.plot(gps_time, gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos)

print("Total Distance = " , gps_c10.data_pos['tot_dist'].iloc[int(imu_end_idx/40)]) 

# Title
#ax_acc.set_title('Accelerometer')
#ax_gyro.set_title('Gyrometer')
#ax_vel.set_title('Velocity')
#ax_dist.set_title('Distance')
fig.suptitle('Avalanche 03.02.2022 - AvaNode C10 - Seilbahnrinne')

# Axes
ax_acc.set_ylabel('Acceleration [m/s²]')
#ax_acc.set_xlabel('Time [sec]')
ax_gyro.set_ylabel('Rotation [°/s]')

ax_vel.set_ylabel('Velocity [m/s]')
ax_dist.set_ylabel('Distance [m]')

ax_dist.set_xlabel('Time [sec]')

# Legend
ax_acc.legend()
ax_acc.legend([acc1, acc2, acc3], ["IMU Acc", "Pos. Acc", "Doppler Acc"])
ax_vel.legend()

#start = datetime(2022,2,3,6,58,20)
#end = datetime(2022,2,3,6,59,40)

# Save
fig.tight_layout()
fig.savefig(save_path + "/220203_Seilbahnrinne_C10_alldata.png", dpi=300)

# Compare
s = gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()
ax_comp.scatter(time, v, label='2022.02.03 - Seilbahn - C10')

#Juliusrinne

gps_c10 = GPSData()
path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-03_Nordkette_avalanche\C10\GPS\ava220203_julius_C10_gnss.txt'
gps_c10.path = path_c10
gps_c10.read_data()
gps_c10.calc_pos_vel()

imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-03_Nordkette_avalanche\C10\Leader\ava220203_julius_C10_imu.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Calcuations 
gps_acc = gps_c10.data_pos['v_total_pos'].diff() / 0.1
doppler_acc = gps_c10.data_pos['v_total'].diff() / 0.1

# Settings
imu_start_idx = 75*400
imu_end_idx = 105*400
#imu_start_idx = 0
#imu_end_idx = -1

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
gps_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]

fig, ((ax_acc, ax_gyro, ax_vel, ax_dist)) = plt.subplots(4,1, figsize=(30, 15))

ax_gyro.set_ylim([-50,2550])
ax_acc.set_ylim([-50, 220])
ax_vel.set_ylim([-0.5, 22])
ax_dist.set_ylim([-5, 450])

# Plots
# Acc
acc1, = ax_acc.plot(imu_time, imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81, color=color_imu, linestyle=ls_imu, label='IMU Acc')
acc2, = ax_acc.plot(gps_time, gps_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='GNSS Acc')
acc3, = ax_acc.plot(gps_time, doppler_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_doppler, linestyle=ls_doppler, label='Doppler Acc')
# Gyro
gyro1, = ax_gyro.plot(imu_time, imu_c10.gyro_tot[imu_start_idx:imu_end_idx], color=color_rotation, label='Gyro')

# Vel
ax_vel.plot(gps_time, gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='Pos. Vel.')
ax_vel.plot(gps_time, gps_c10.data_pos['v_total'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_doppler, linestyle=ls_doppler, label='Doppler Vel.')
# Dist
ax_dist.plot(gps_time, gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos)

# Title
#ax_acc.set_title('Accelerometer')
#ax_gyro.set_title('Gyrometer')
#ax_vel.set_title('Velocity')
#ax_dist.set_title('Distance')
fig.suptitle('Avalanche 03.02.2022 - AvaNode C10 - Juliusrinne')

# Axes
ax_acc.set_ylabel('Acceleration [m/s²]')
#ax_acc.set_xlabel('Time [sec]')
ax_gyro.set_ylabel('Rotation [°/s]')

ax_vel.set_ylabel('Velocity [m/s]')
ax_dist.set_ylabel('Distance [m]')

ax_dist.set_xlabel('Time [sec]')

# Legend
ax_acc.legend()
ax_acc.legend([acc1, acc2, acc3], ["IMU Acc", "Pos. Acc", "Doppler Acc"])
ax_vel.legend()

# Save
fig.tight_layout()
fig.savefig(save_path + "/220203_Juliusrinne_C10_alldata.png", dpi=300)

# Compare
s = gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()
#ax_comp.scatter(s,v, label='2022.02.03 - Julius - C10')

#start = datetime(2022,2,3,7,40,40)
#end = datetime(2022,2,3,7,41,30)


#%% 07.02.2022

# Experiment 07.02.2022
# troubles with gps signal...solved...nope...
gps_c10 = GPSData()
path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-07_Nordkette_avalanche\C10\GPS\ava220207_selfrelease_avalanche_gnss.txt'
gps_c10.path = path_c10
gps_c10.read_data()
gps_c10.calc_pos_vel()

imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-07_Nordkette_avalanche\C10\Leader\ava220207_selfrelease_avalanche_imu.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Calcuations 
gps_acc = gps_c10.data_pos['v_total_pos'].diff() / 0.1
doppler_acc = gps_c10.data_pos['v_total'].diff() / 0.1

# Settings
imu_start_idx = 75*400
imu_end_idx = 105*400
imu_start_idx = 0
imu_end_idx = -1

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
gps_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]

fig, ((ax_acc, ax_gyro, ax_vel, ax_dist)) = plt.subplots(4,1)


# Plots
# Acc
acc1, = ax_acc.plot(imu_time, imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81, color=color_imu, label='IMU Acc')
acc2, = ax_acc.plot(gps_time, gps_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='GNSS Acc')
acc3, = ax_acc.plot(gps_time, doppler_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_doppler, linestyle=ls_doppler, label='Doppler Acc')
# Gyro
gyro1, = ax_gyro.plot(imu_time, imu_c10.gyro_tot[imu_start_idx:imu_end_idx], color=color_rotation, label='Gyro')
ax_gyro.set_ylim([-50,2550])
# Vel
ax_vel.plot(gps_time, gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='Pos. Vel.')
ax_vel.plot(gps_time, gps_c10.data_pos['v_total'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_doppler, linestyle=ls_doppler, label='Doppler Vel.')
# Dist
ax_dist.plot(gps_time, gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos)

print("Total Distance = " , gps_c10.data_pos['tot_dist'].iloc[int(imu_end_idx/40)]) 

# Title
#ax_acc.set_title('Accelerometer')
#ax_gyro.set_title('Gyrometer')
#ax_vel.set_title('Velocity')
#ax_dist.set_title('Distance')
fig.suptitle('Avalanche 07.02.2022 - AvaNode C10 - Juliusrinne')

# Axes
ax_acc.set_ylabel('Acceleration [m/s²]')
#ax_acc.set_xlabel('Time [sec]')
ax_gyro.set_ylabel('Rotation [°/s]')

ax_vel.set_ylabel('Velocity [m/s]')
ax_dist.set_ylabel('Distance [m]')

ax_dist.set_xlabel('Time [sec]')

# Legend
ax_acc.legend()
ax_acc.legend([acc1, acc2, acc3], ["IMU Acc", "GNSS Acc", "Doppler Acc"])
ax_vel.legend()

#Julius
start = datetime(2022,2,7,7,57,30)
end = datetime(2022,2,7,7,57,50)

# Avalanche in this time period!
start = datetime(2022,2,7,7,37,20)
end = datetime(2022,2,7,7,41,20)

#%% 22.02.2022 C07
# Experiment 22.02.2022

gps_c10 = GPSData()
path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C07\GPS\220222_C07_avalanche_GPS.txt'
gps_c10.path = path_c10
gps_c10.read_data()
gps_c10.calc_pos_vel()

imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C07\Leader\220222_C07_avalanche_Leader.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Calcuations 
gps_acc = gps_c10.data_pos['v_total_pos'].diff() / 0.1
doppler_acc = gps_c10.data_pos['v_total'].diff() / 0.1


# Settings
imu_start_idx = 40*400
imu_end_idx = 85*400
#imu_start_idx = 0
#imu_end_idx = -1

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
gps_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]

imu_vel = np.zeros_like(imu_time)
dt = 1/400
j = 1
for i in range(imu_start_idx+1,imu_end_idx):
    imu_vel[j] = ((imu_c10.acc_tot[i] - 9.81) + (imu_c10.acc_tot[i-1] - 9.81)) / 2 * dt + imu_vel[j-1]
    j += 1

fig, ((ax_acc, ax_gyro, ax_vel, ax_dist)) = plt.subplots(4,1, figsize=(30, 15))

ax_gyro.set_ylim([-50,2550])
ax_acc.set_ylim([-50, 220])
ax_vel.set_ylim([-0.5, 22])
ax_dist.set_ylim([-5, 450])

# Plots
# Acc
acc1, = ax_acc.plot(imu_time, imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81, color=color_imu, linestyle=ls_imu, label='IMU Acc')
acc2, = ax_acc.plot(gps_time, gps_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='GNSS Acc')
acc3, = ax_acc.plot(gps_time, doppler_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_doppler, linestyle=ls_doppler, label='Doppler Acc')
# Gyro
gyro1, = ax_gyro.plot(imu_time, imu_c10.gyro_tot[imu_start_idx:imu_end_idx], color=color_rotation, label='Gyro')
# Vel
ax_vel.plot(gps_time, gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='Pos. Vel.')
ax_vel.plot(gps_time, gps_c10.data_pos['v_total'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_doppler, linestyle=ls_doppler, label='Doppler Vel.')
#ax_vel.plot(imu_time, imu_vel, color=color_imu, linestyle=ls_imu, label='IMU Vel.')
# Dist
ax_dist.plot(gps_time, gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos)

print("Total Distance = " , gps_c10.data_pos['tot_dist'].iloc[int(imu_end_idx/40)]) 

# Title
#ax_acc.set_title('Accelerometer')
#ax_gyro.set_title('Gyrometer')
#ax_vel.set_title('Velocity')
#ax_dist.set_title('Distance')
fig.suptitle('Avalanche 22.02.2022 - AvaNode C07 - Seilbahnrinne')

# Axes
ax_acc.set_ylabel('Acceleration [m/s²]')
#ax_acc.set_xlabel('Time [sec]')
ax_gyro.set_ylabel('Rotation [°/s]')

ax_vel.set_ylabel('Velocity [m/s]')
ax_dist.set_ylabel('Distance [m]')

ax_dist.set_xlabel('Time [sec]')

# Legend
ax_acc.legend()
ax_acc.legend([acc1, acc2, acc3], ["IMU Acc", "Pos. Acc", "Doppler Acc"])
ax_vel.legend()

# Save
#fig.tight_layout()
#fig.savefig(save_path + "/220222_Seilbahnrinne_C07_alldata.png", dpi=300)

# Compare
s = gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()
ax_comp.scatter(time, v, label='2022.02.22 - Seilbahn - C07')

#%% 22.02.2022 C09
# C09
gps_c10 = GPSData()
path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C09\GPS\220222_C09_avalanche_GPS.txt'
gps_c10.path = path_c10
gps_c10.read_data()
gps_c10.calc_pos_vel()

imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C09\Leader\220222_C09_avalanche_Leader.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Calcuations 
gps_acc = gps_c10.data_pos['v_total_pos'].diff() / 0.1
doppler_acc = gps_c10.data_pos['v_total'].diff() / 0.1

# Settings
imu_start_idx = 25*400
imu_end_idx = 70*400
#imu_start_idx = 0
#imu_end_idx = -1

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
gps_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]

fig, ((ax_acc, ax_gyro, ax_vel, ax_dist)) = plt.subplots(4,1, figsize=(30, 15))

ax_gyro.set_ylim([-50,2550])
ax_acc.set_ylim([-50, 220])
ax_vel.set_ylim([-0.5, 22])
ax_dist.set_ylim([-5, 450])

# Plots
# Acc
acc1, = ax_acc.plot(imu_time, imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81, color=color_imu, linestyle=ls_imu, label='IMU Acc')
acc2, = ax_acc.plot(gps_time, gps_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='GNSS Acc')
acc3, = ax_acc.plot(gps_time, doppler_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_doppler, linestyle=ls_doppler, label='Doppler Acc')
# Gyro
gyro1, = ax_gyro.plot(imu_time, imu_c10.gyro_tot[imu_start_idx:imu_end_idx], color=color_rotation, label='Gyro')
# Vel
ax_vel.plot(gps_time, gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='Pos. Vel.')
ax_vel.plot(gps_time, gps_c10.data_pos['v_total'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_doppler, linestyle=ls_doppler, label='Doppler Vel.')
# Dist
ax_dist.plot(gps_time, gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos)

print("Total Distance = " , gps_c10.data_pos['tot_dist'].iloc[int(imu_end_idx/40)]) 

# Title
#ax_acc.set_title('Accelerometer')
#ax_gyro.set_title('Gyrometer')
#ax_vel.set_title('Velocity')
#ax_dist.set_title('Distance')
fig.suptitle('Avalanche 22.02.2022 - AvaNode C09 - Seilbahnrinne')

# Axes
ax_acc.set_ylabel('Acceleration [m/s²]')
#ax_acc.set_xlabel('Time [sec]')
ax_gyro.set_ylabel('Rotation [°/s]')

ax_vel.set_ylabel('Velocity [m/s]')
ax_dist.set_ylabel('Distance [m]')

ax_dist.set_xlabel('Time [sec]')

# Legend
ax_acc.legend()
ax_acc.legend([acc1, acc2, acc3], ["IMU Acc", "Pos. Acc", "Doppler Acc"])
ax_vel.legend()

# Save
fig.tight_layout()
fig.savefig(save_path + "/220222_Seilbahnrinne_C09_alldata.png", dpi=300)

# Compare
s = gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()
ax_comp.scatter(time,v, label='2022.02.22 - Seilbahn - C09')

#%%22.02.2022 C10
# C10
gps_c10 = GPSData()
path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C10\GPS\220222_C10_avalanche_GPS.txt'
gps_c10.path = path_c10
gps_c10.read_data()
gps_c10.calc_pos_vel()

imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C10\Leader\220222_C10_avalanche_Leader.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Calcuations 
gps_acc = gps_c10.data_pos['v_total_pos'].diff() / 0.1
doppler_acc = gps_c10.data_pos['v_total'].diff() / 0.1

# Settings
imu_start_idx = 55*400
imu_end_idx = 100*400
#imu_start_idx = 0
#imu_end_idx = -1

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
gps_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]

fig, ((ax_acc, ax_gyro, ax_vel, ax_dist)) = plt.subplots(4,1, figsize=(30, 15))

ax_gyro.set_ylim([-50,2550])
ax_acc.set_ylim([-50, 220])
ax_vel.set_ylim([-0.5, 22])
ax_dist.set_ylim([-5, 450])

# Plots
# Acc
acc1, = ax_acc.plot(imu_time, imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81, color=color_imu, linestyle=ls_imu, label='IMU Acc')
acc2, = ax_acc.plot(gps_time, gps_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='GNSS Acc')
acc3, = ax_acc.plot(gps_time, doppler_acc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_doppler, linestyle=ls_doppler, label='Doppler Acc')
# Gyro
gyro1, = ax_gyro.plot(imu_time, imu_c10.gyro_tot[imu_start_idx:imu_end_idx], color=color_rotation, label='Gyro')
# Vel
ax_vel.plot(gps_time, gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos, label='Pos. Vel.')
ax_vel.plot(gps_time, gps_c10.data_pos['v_total'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_doppler, linestyle=ls_doppler, label='Doppler Vel.')
# Dist
ax_dist.plot(gps_time, gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)], color=color_position, linestyle=ls_pos)

print("Total Distance = " , gps_c10.data_pos['tot_dist'].iloc[int(imu_end_idx/40)]) 

# Title
#ax_acc.set_title('Accelerometer')
#ax_gyro.set_title('Gyrometer')
#ax_vel.set_title('Velocity')
#ax_dist.set_title('Distance')
fig.suptitle('Avalanche 22.02.2022 - AvaNode C10 - Seilbahnrinne')

# Axes
ax_acc.set_ylabel('Acceleration [m/s²]')
#ax_acc.set_xlabel('Time [sec]')
ax_gyro.set_ylabel('Rotation [°/s]')

ax_vel.set_ylabel('Velocity [m/s]')
ax_dist.set_ylabel('Distance [m]')

ax_dist.set_xlabel('Time [sec]')

# Legend
ax_acc.legend()
ax_acc.legend([acc1, acc2, acc3], ["IMU Acc", "Pos. Acc", "Doppler Acc"])
ax_vel.legend()

# Save
fig.tight_layout()
fig.savefig(save_path + "/220222_Seilbahnrinne_C10_alldata.png", dpi=300)

# Compare
s = gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()
ax_comp.scatter(time,v, label='2022.02.22 - Seilbahn - C10')
ax_comp.legend()