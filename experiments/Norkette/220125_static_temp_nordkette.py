# -*- coding: utf-8 -*-
"""
Created on Tue May 31 16:46:47 2022

@author: neuhauser
"""

import matplotlib.pyplot as plt
import matplotlib.dates as dates

from classes.AvaNode_Temp_Class import AvaNode_TEMP

# Temperatur Messungen Nordkette
#C04 - gleiche Temperatur
gps_c04 = AvaNode_TEMP()
gps_c04.path = r'../../data/2022-01-25_Nordkette_GPS_snow/AvaNodes/C04/GPS/temp_minus_1_point_1.txt'
gps_c04.path = r'../../data/2022-01-25_Nordkette_GPS_snow/AvaNodes/C04/GPS/C04Gps001.txt'
gps_c04.read_data()
gps_c04.get_snow_temp()

fig, ax = plt.subplots()
date = dates.date2num(gps_c04.data["timestamp"])
# Define the date format
date_form = dates.DateFormatter("%H:%M")
ax.xaxis.set_major_formatter(date_form)

ax.plot(date, gps_c04.data["ambientTemp[C]"], label="IR,Ambient", color="orange")
ax.plot(date, gps_c04.data["objectTemp[C]"], label="IR,Object", color="blue")
ax.plot(date, gps_c04.data["corr.snowTemp[C]"], label="Snow", color="red")
ax.hlines(-1.1, date[0], date[-1])

# #C06
gps_c06 = AvaNode_TEMP()
gps_c06.path = r'../../data/2022-01-25_Nordkette_GPS_snow/AvaNodes/C06/GPS/temp_minus_4_point_9.txt'
gps_c06.read_data()
gps_c06.get_snow_temp()

fig, ax = plt.subplots()
date = dates.date2num(gps_c06.data["timestamp"])
# Define the date format
date_form = dates.DateFormatter("%H:%M")
ax.xaxis.set_major_formatter(date_form)

ax.plot(date, gps_c06.data["ambientTemp[C]"], label="IR,Ambient", color="orange")
ax.plot(date, gps_c06.data["objectTemp[C]"], label="IR,Object", color="blue")
ax.plot(date, gps_c06.data["corr.snowTemp[C]"], label="Snow", color="red")
ax.hlines(-4.9, date[0], date[-1])

# #C07
gps_c07 = AvaNode_TEMP()
gps_c07.path = r'../../data/2022-01-25_Nordkette_GPS_snow/AvaNodes/C07/GPS/temp_minus_2_point_9.txt'
gps_c07.read_data()
gps_c07.get_snow_temp()

fig, ax = plt.subplots()
date = dates.date2num(gps_c07.data["timestamp"])
# Define the date format
date_form = dates.DateFormatter("%H:%M")
ax.xaxis.set_major_formatter(date_form)

ax.plot(date, gps_c07.data["ambientTemp[C]"], label="IR,Ambient", color="orange")
ax.plot(date, gps_c07.data["objectTemp[C]"], label="IR,Object", color="blue")
ax.plot(date, gps_c07.data["corr.snowTemp[C]"], label="Snow", color="red")
ax.hlines(-2.9, date[0], date[-1])

# C05
gps_c05 = AvaNode_TEMP()
gps_c05.path = r'../../data/2022-01-25_Nordkette_GPS_snow/AvaNodes/C05/GPS/temp_minus_1_point_6.txt'
gps_c05.read_data()
gps_c05.get_snow_temp()

fig, ax = plt.subplots()
date = dates.date2num(gps_c05.data["timestamp"])
# Define the date format
date_form = dates.DateFormatter("%H:%M")
ax.xaxis.set_major_formatter(date_form)

ax.plot(date, gps_c05.data["ambientTemp[C]"], label="IR,Ambient", color="orange")
ax.plot(date, gps_c05.data["objectTemp[C]"], label="IR,Object", color="blue")
ax.plot(date, gps_c05.data["corr.snowTemp[C]"], label="Snow", color="red")
ax.hlines(-1.6, date[0], date[-1])