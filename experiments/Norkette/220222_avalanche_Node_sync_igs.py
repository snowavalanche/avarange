# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 11:56:08 2022

@author: neuhauser
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from datetime import datetime

from classes.GPS_Class import GPSData
from classes.IMU_Class import ImuData


#C07
gps_c07 = GPSData()
path_c07 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C07\GPS\220222_C07_avalanche_GPS.txt"
gps_c07.path = path_c07
gps_c07.read_data()

imu_c07 = ImuData()
imu_path_c07 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C07\Leader\220222_C07_avalanche_Leader.txt'
imu_c07.path = imu_path_c07
imu_c07.read_data_pd()

#Time Filter
start_idx = np.where(gps_c07.data_pos["timestamp"] == "2022-02-22 07:52:22")[0][0]
end_idx = np.where(gps_c07.data_pos["timestamp"] == "2022-02-22 07:53:10")[0][0]
imu_07_start_idx = start_idx * 40
imu_07_end_idx = end_idx * 40

gps_c07.data_pos = gps_c07.data_pos.iloc[start_idx:end_idx, :]
gps_c07.data = gps_c07.data.iloc[start_idx:end_idx, :]

imu_07_time = imu_c07.time[imu_07_start_idx:imu_07_end_idx] - imu_c07.time[imu_07_start_idx]
gps_07_time = gps_c07.data['iTow[ms]'] / 1000 - gps_c07.data['iTow[ms]'].iloc[0]/1000

# C09
gps_c09 = GPSData()
path_c09 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C09\GPS\220222_C09_avalanche_GPS.txt"
gps_c09.path = path_c09
gps_c09.read_data()

imu_c09 = ImuData()
imu_path_c09 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C09\Leader\220222_C09_avalanche_Leader.txt'
imu_c09.path = imu_path_c09
imu_c09.read_data_pd()

#Time Filter
start_idx = np.where(gps_c09.data_pos["timestamp"] == "2022-02-22 07:52:22")[0][0]
end_idx = np.where(gps_c09.data_pos["timestamp"] == "2022-02-22 07:53:10")[0][0]
imu_09_start_idx = start_idx * 40
imu_09_end_idx = end_idx * 40

gps_c09.data_pos = gps_c09.data_pos.iloc[start_idx:end_idx, :]
gps_c09.data = gps_c09.data.iloc[start_idx:end_idx, :]

imu_09_time = imu_c09.time[imu_09_start_idx:imu_09_end_idx] - imu_c09.time[imu_09_start_idx]
gps_09_time = gps_c09.data['iTow[ms]'] / 1000 - gps_c09.data['iTow[ms]'].iloc[0]/1000

# C10
gps_c10 = GPSData()
path_c10 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C10\GPS\220222_C10_avalanche_GPS.txt"
gps_c10.path = path_c10
gps_c10.read_data()

imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C10\Leader\220222_C10_avalanche_Leader.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()


#Time Filter
start_idx = np.where(gps_c10.data_pos["timestamp"] == "2022-02-22 07:52:22")[0][0]
end_idx = np.where(gps_c10.data_pos["timestamp"] == "2022-02-22 07:53:10")[0][0]
imu_10_start_idx = start_idx * 40
imu_10_end_idx = end_idx * 40

gps_c10.data_pos = gps_c10.data_pos.iloc[start_idx:end_idx, :]
gps_c10.data = gps_c10.data.iloc[start_idx:end_idx, :]

imu_10_time = imu_c10.time[imu_10_start_idx:imu_10_end_idx] - imu_c10.time[imu_10_start_idx]
gps_10_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000


c07_v_tot = np.linalg.norm([gps_c07.data["velD[mm/s]"].values, gps_c07.data["velE[mm/s]"].values, gps_c07.data["velN[mm/s]"].values], axis=0)/1000
c09_v_tot = np.linalg.norm([gps_c09.data["velD[mm/s]"].values, gps_c09.data["velE[mm/s]"].values, gps_c09.data["velN[mm/s]"].values], axis=0)/1000
c10_v_tot = np.linalg.norm([gps_c10.data["velD[mm/s]"].values, gps_c10.data["velE[mm/s]"].values, gps_c10.data["velN[mm/s]"].values], axis=0)/1000

# Read Particle Data from Sim

path_to_sim = r'C:\git_rep\AvaFrame\avaframe\data\seilbahnrinne\Outputs\com1DFA\particlesCSV'
files = os.listdir(path_to_sim)
files.sort(key=lambda f: int(''. join(filter(str. isdigit, f))))


particles = np.zeros((len(files), 3)) # Time, min_v, max_v
time = 0

for file in files:
    data = pd.read_csv(path_to_sim + '/' + file)
    particles[time, 0] = time
    particles[time, 1] = data['velocityMagnitude'].min()
    particles[time, 2] = data['velocityMagnitude'].max()
    time += 1
    
# Plot
fig, (ax_gnss, ax_acc, ax_gyro) = plt.subplots(3,1, figsize=(20, 30))

ax_gnss.plot(gps_07_time, c07_v_tot, color='g', label='C07')
ax_gnss.plot(gps_09_time, c09_v_tot, color='r', label='C09')
ax_gnss.plot(gps_10_time, c10_v_tot, color='orange', label='C10')

# Plot Add-ons
sim_till = 48
ax_gnss.fill_between(particles[:sim_till, 0], particles[:sim_till, 1], particles[:sim_till, 2], color='lightgray', label='Simulation')


ax_gnss.set_ylabel('Velocity [m/s]')
ax_gnss.set_title('Avalanche 22.02.2022 - Measurement - Simulation')

# Plot Acc
ax_acc.plot(imu_07_time, imu_c07.acc_tot[imu_07_start_idx:imu_07_end_idx] - 9.81, color='g', label='C07')
ax_acc.plot(imu_09_time, imu_c09.acc_tot[imu_09_start_idx:imu_09_end_idx] - 9.81, color='r', label='C09')
ax_acc.plot(imu_10_time, imu_c10.acc_tot[imu_10_start_idx:imu_10_end_idx] - 9.81, color='orange', label='C10')

# Plot Gyro
ax_gyro.plot(imu_07_time, imu_c07.gyro_tot[imu_07_start_idx:imu_07_end_idx], color='g', label='C07')
ax_gyro.plot(imu_09_time, imu_c09.gyro_tot[imu_09_start_idx:imu_09_end_idx], color='r', label='C09')
ax_gyro.plot(imu_10_time, imu_c10.gyro_tot[imu_10_start_idx:imu_10_end_idx], color='orange', label='C10')

ax_acc.set_ylim([-50, 220])
ax_gyro.set_ylim([-50,2550])

ax_gnss.grid()
ax_acc.grid()
ax_gyro.grid()
ax_gnss.legend()

ax_acc.set_ylabel('Acceleration [m/s²]')
ax_gyro.set_ylabel('Angular velocity [°/s]')
ax_gyro.set_xlabel('Time [sec.]')

save_path = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange\docs\Conferences\IGS2022\pics\220222_meas_sim.png'
fig.savefig(save_path, dpi=300)

# X lim
#ax_gnss.set_xlim([40, 85])
#ax_acc.set_xlim([40, 85])
#ax_gyro.set_xlim([40, 85])
