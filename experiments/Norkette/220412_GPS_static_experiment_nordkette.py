# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 10:22:03 2022

@author: neuhauser
"""

import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
import matplotlib.dates as dates
from geopy import distance

from GPS_Class import GPSData



# Node | Depth[cm]
# C04  | 0
# C09  | 50
# C07  | 100
# C06  | 150
# C10  | 200

# =============================================================================
# #C04 - Error in SD Card so far...
# gps_c04 = GPSData()
# path_c04 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-01-25_Nordkette\AvaNodes\C05\GPS\C05Gps002.txt"
# gps_c04.path = path_c04
# gps_c04.read_data()
# 
# long_c04 = 11.38079685
# lat_c04 = 47.30754946
# 
# rtk_pos = (lat_c04, long_c04)
# ds_c04 = []
# for idx, lat in enumerate(gps_c04.data_pos["latitude[deg]"]):
#     node_pos = (lat, gps_c04.data_pos["longitude[deg]"].iloc[idx])
#     ds = distance.distance(rtk_pos, node_pos).m
#     ds_c04.append(ds)
# =============================================================================

#C09
gps_c09 = GPSData()
path_c09 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-04-12_Nordkette_GPS_densesnow\C09\GPS\C09Gps000.txt"
gps_c09.path = path_c09
gps_c09.read_data()

long_c09 = 11.37688830
lat_c09 = 47.30572808
rtk_pos = (lat_c09, long_c09)
ds_c09 = []

#Time Filter
start_idx = np.where(gps_c09.data_pos["timestamp"] == "2022-04-12 08:30:00")[0][0]
end_idx = np.where(gps_c09.data_pos["timestamp"] == "2022-04-12 10:35:00")[0][0]

gps_c09.data_pos = gps_c09.data_pos.iloc[start_idx:end_idx, :]

for idx, lat in enumerate(gps_c09.data_pos["latitude[deg]"]):
    node_pos = (lat, gps_c09.data_pos["longitude[deg]"].iloc[idx])
    ds = distance.distance(rtk_pos, node_pos).m
    ds_c09.append(ds)

#C07
gps_c07 = GPSData()
path_c07 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-04-12_Nordkette_GPS_densesnow\C07\GPS\C07Gps001.txt"
gps_c07.path = path_c07
gps_c07.read_data()

long_c07 = 11.37687858
lat_c07 = 47.30572714

rtk_pos = (lat_c07, long_c07)
ds_c07 = []

#Time Filter
start_idx = np.where(gps_c07.data_pos["timestamp"] == "2022-04-12 08:30:00")[0][0]
end_idx = np.where(gps_c07.data_pos["timestamp"] == "2022-04-12 10:35:00")[0][0]

gps_c07.data_pos = gps_c07.data_pos.iloc[start_idx:end_idx, :]

for idx, lat in enumerate(gps_c07.data_pos["latitude[deg]"]):
    node_pos = (lat, gps_c07.data_pos["longitude[deg]"].iloc[idx])
    ds = distance.distance(rtk_pos, node_pos).m
    ds_c07.append(ds)
    
#C06
gps_c06 = GPSData()
path_c06 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-04-12_Nordkette_GPS_densesnow\C06\GPS\C06Gps000.txt"
gps_c06.path = path_c06
gps_c06.read_data()

long_c06 = 11.37687342
lat_c06 = 47.30572597

rtk_pos = (lat_c06, long_c06)
ds_c06 = []

#Time Filter
#start_idx = np.where(gps_c06.data_pos["timestamp"] == "2022-04-12 08:30:00")[0][0]
#end_idx = np.where(gps_c06.data_pos["timestamp"] == "2022-04-12 10:35:00")[0][0]

#gps_c06.data_pos = gps_c06.data_pos.iloc[start_idx:end_idx, :]

for idx, lat in enumerate(gps_c06.data_pos["latitude[deg]"]):
    node_pos = (lat, gps_c06.data_pos["longitude[deg]"].iloc[idx])
    ds = distance.distance(rtk_pos, node_pos).m
    ds_c06.append(ds)

#C10
gps_c10 = GPSData()
path_c10 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-04-12_Nordkette_GPS_densesnow\C10\GPS\C10Gps000.txt"
gps_c10.path = path_c10
gps_c10.read_data()

long_c10 = 11.37685812
lat_c10 = 47.30572013

rtk_pos = (lat_c10, long_c10)
ds_c10 = []

#Time Filter
start_idx = np.where(gps_c10.data_pos["timestamp"] == "2022-04-12 07:35:00")[0][0]
end_idx = np.where(gps_c10.data_pos["timestamp"] == "2022-04-12 10:35:00")[0][0]

gps_c10.data_pos = gps_c10.data_pos.iloc[start_idx:end_idx, :]

for idx, lat in enumerate(gps_c10.data_pos["latitude[deg]"]):
    node_pos = (lat, gps_c10.data_pos["longitude[deg]"].iloc[idx])
    ds = distance.distance(rtk_pos, node_pos).m
    ds_c10.append(ds)

# Preparing time format
#date_c05 = dates.date2num(gps_c05.data_pos[gps_c05.utc])
#date_c06 = dates.date2num(gps_c06.data_pos[gps_c06.utc])
#date_c07 = dates.date2num(gps_c07.data_pos[gps_c07.utc])
#date_c04 = dates.date2num(gps_c04.data_pos[gps_c04.utc])
# Define the date format
#date_form = dates.DateFormatter("%H:%M")

# =============================================================================
# #PLot Number of Satellites
# fig, ax = plt.subplots()
# ax.xaxis.set_major_formatter(date_form)
# 
# ax.scatter(date_c05, gps_c05.data_pos["numSV[]"], color='blue', label='0 cm', marker='x')
# ax.scatter(date_c06, gps_c06.data_pos["numSV[]"], color='green', label = '50 cm')
# ax.scatter(date_c07, gps_c07.data_pos["numSV[]"], color='orange', label='100 cm')
# ax.scatter(date_c04, gps_c04.data_pos["numSV[]"], color='red', label='150 cm')
# 
# ax.grid()
# ax.legend()
# 
# # Plot Psoition Deviation
# fig, ax = plt.subplots()
# 
# ax.plot(ds_c05, color='blue', label='0 cm')
# ax.plot(ds_c06, color='green', label = '50 cm')
# ax.plot(ds_c07, color='orange', label='100 cm')
# ax.plot(ds_c04, color='red', label='150 cm')
# 
# ax.legend()
# =============================================================================

# Boxplot Pos. Deviation
data = [ds_c09, ds_c07, ds_c10]
labels = ['50 cm', '100 cm', '200 cm']

fig1, ax1 = plt.subplots()
bp = ax1.boxplot(data,
            vert=True,  # vertical box alignment
            patch_artist=True,  # fill with color
            labels=labels,
            showfliers=False)  # will be used to label x-ticks
ax1.set_xlabel('snow cover')
ax1.set_ylabel('Deviation [m]')
#ax1.set_xticklabels([5, 6, 7, 8, 9, 10])
ax1.grid()
ax1.set_title('Deviation of GNSS Position')
fig1.savefig('220412_pos_deviation.png', dpi=300)

medians = [item.get_ydata()[0] for item in bp['medians']]
means = [item.get_ydata()[0] for item in bp['means']]
print(f'Medians: {medians}\n'
      f'Means:   {means}')

# Boxplot Number of Satellites
data = [gps_c09.data_pos["numSV[]"], gps_c07.data_pos["numSV[]"], gps_c06.data_pos["numSV[]"], gps_c10.data_pos["numSV[]"]]
labels = ['50 cm', '100 cm', '150 cm', '200 cm']

fig1, ax1 = plt.subplots()
ax1.boxplot(data,
            vert=True,  # vertical box alignment
            patch_artist=True,  # fill with color
            labels=labels,
            showfliers=False)  # will be used to label x-ticks
ax1.set_xlabel('snow cover')
ax1.set_ylabel('Nr. of Sat.')
#ax1.set_xticklabels([5, 6, 7, 8, 9, 10])
ax1.grid()
ax1.set_title('Number of Satellites')
#fig1.savefig('220412_nrsat_deviation.png', dpi=300)

# Boxplot Velocity
c09_vel = np.linalg.norm([gps_c09.data_pos["velD[mm/s]"].values, gps_c09.data_pos["velE[mm/s]"].values, gps_c09.data_pos["velN[mm/s]"].values], axis=0)/1000
#c06_vel = np.linalg.norm([gps_c06.data_pos["velD[mm/s]"].values, gps_c06.data_pos["velE[mm/s]"].values, gps_c06.data_pos["velN[mm/s]"].values], axis=0)/1000
c07_vel = np.linalg.norm([gps_c07.data_pos["velD[mm/s]"].values, gps_c07.data_pos["velE[mm/s]"].values, gps_c07.data_pos["velN[mm/s]"].values], axis=0)/1000
c10_vel = np.linalg.norm([gps_c10.data_pos["velD[mm/s]"].values, gps_c10.data_pos["velE[mm/s]"].values, gps_c10.data_pos["velN[mm/s]"].values], axis=0)/1000

data = [c09_vel, c07_vel, c10_vel]
labels = ['50 cm', '100 cm', '200 cm']

fig1, ax1 = plt.subplots()
bp2 = ax1.boxplot(data,
            vert=True,  # vertical box alignment
            patch_artist=True,  # fill with color
            labels=labels,
            showfliers=False)  # will be used to label x-ticks
ax1.set_xlabel('snow cover')
ax1.set_ylabel('Velocity [m/s]')
#ax1.set_xticklabels([5, 6, 7, 8, 9, 10])
ax1.grid()
ax1.set_title('Velocity Deviation')
#fig1.savefig('220222_vel_deviation.png', dpi=300)

medians2 = [item.get_ydata()[0] for item in bp2['medians']]
means2 = [item.get_ydata()[0] for item in bp2['means']]
print(f'Medians: {medians2}\n'
      f'Means:   {means2}')
