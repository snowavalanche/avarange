# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 11:56:08 2022

@author: neuhauser
"""

import numpy as np
from datetime import datetime

from classes.AvaNode_GNSS_Class import AvaNode_GNSS

#C01
gps_c01 = AvaNode_GNSS()
path_c01 = r"../../data/2021-03-15_Nordkette_avalanche/C01/GPS/ava210315_C01.txt"
gps_c01.path = path_c01
gps_c01.read_data()
starttime = datetime(2021,3,15,7,51,15)
endtime = datetime(2021,3,15,7,52,15)
gps_c01.plot_vel(False, False, True, starttime=starttime, endtime=endtime)

maxi = np.amax(gps_c01.data_pos['v_total_pos'])