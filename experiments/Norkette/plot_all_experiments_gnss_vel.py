# -*- coding: utf-8 -*-
"""
Created on Thu Aug 25 16:14:04 2022

@author: neuhauser
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 11:56:08 2022

@author: neuhauser
"""
import matplotlib
import matplotlib.pyplot as plt
from datetime import datetime
import numpy as np

from classes.GPS_Class import GPSData
from classes.IMU_Class import ImuData
matplotlib.rcParams.update({'font.size': 18})

#%% Settings

save_path = r"C:\git_rep\dissertation\papers\GPS_paper\figures\exp"
# Colors
color_imu = 'black'
color_doppler = 'red'
color_position = 'blue'
color_rotation = 'green'
# Linestyles
ls_imu = 'solid'
ls_pos = 'dashed'
ls_doppler = 'dotted'
# Save
save = False
vel_trashold = 1

#%% 15.03.2022 Seilbahn

# Experiment 15.03.2021
# C01
# Seilbahnrinne
gps_c01 = GPSData()
path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-15_Nordkette_avalanche\C01\GPS\ava210315_C01.txt'
gps_c01.path = path_c01
gps_c01.read_data()
gps_c01.calc_pos_vel()

imu_c01 = ImuData()
imu_path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-15_Nordkette_avalanche\C01\Leader\ava210315_C01_Leader.txt'
imu_c01.path = imu_path_c01
imu_c01.read_data_pd()

# Calcuations 
gps_acc = gps_c01.data_pos['v_total_pos'].diff() / 0.1

# Settings
#gnss_start_index = np.where(gps_c01.data_pos['v_total_pos'] > vel_trashold)[0][0]
imu_start_idx = 38*400
#imu_start_idx = gnss_start_index * 40
imu_end_idx = 80*400
#imu_start_idx = 0
#imu_end_idx = -1
imu_time = imu_c01.time[imu_start_idx:imu_end_idx]
gps_time = gps_c01.data['#Data contains: GPS time of week of the navigation epoch (ms)'] / 1000 - gps_c01.data['#Data contains: GPS time of week of the navigation epoch (ms)'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]


# Compare Plot
fig_compare, ax_comp = plt.subplots(figsize=(20, 10))
s = gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()

ax_comp.scatter(time, v, label='2021.03.15 - Seilbahn - C01')
ax_comp.set_xlabel('s/s_max')
ax_comp.set_xlabel('t/t_max')
ax_comp.set_ylabel('v/v_max')


#%% 15.03.2022 Julius

# Juliusrinne
gps_c01 = GPSData()
path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-15_Nordkette_avalanche\C01\GPS\210315_julius_C01.txt'
gps_c01.path = path_c01
gps_c01.read_data()
gps_c01.calc_pos_vel()

imu_c01 = ImuData()
imu_path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-15_Nordkette_avalanche\C01\Leader\210315_julius_C01_imu.txt'
imu_c01.path = imu_path_c01
imu_c01.read_data_pd()

# Calcuations 
gps_acc = gps_c01.data_pos['v_total_pos'].diff() / 0.1

# Settings
#gnss_start_index = np.where(gps_c01.data_pos['v_total_pos'] > vel_trashold)[0][0]
imu_start_idx = 150*400
#imu_start_idx = gnss_start_index * 40


imu_end_idx = 230*400
#imu_start_idx = 0
#imu_end_idx = -1
imu_time = imu_c01.time[imu_start_idx:imu_end_idx]
gps_time = gps_c01.data['#Data contains: GPS time of week of the navigation epoch (ms)'] / 1000 - gps_c01.data['#Data contains: GPS time of week of the navigation epoch (ms)'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]


# Compare
s = gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()

#ax_comp.scatter(s,v, label='2021.03.15 - Julius')

#start = datetime(2021,3,15,8,32,30)
#end = datetime(2021,3,15,8,35,0)
#%% 16.03.2022 C01

# Experiment 16.03.2021
# C01
# Seilbahnrinne
gps_c01 = GPSData()
path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-16_Nordkette_avalanche\C01\GPS\ava210316_C01_GPS.txt'
gps_c01.path = path_c01
gps_c01.read_data()
gps_c01.calc_pos_vel()

imu_c01 = ImuData()
imu_path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-16_Nordkette_avalanche\C01\Leader\ava210316_C01_Leader.txt'
imu_c01.path = imu_path_c01
imu_c01.read_data_pd()

# Calcuations 
gps_acc = gps_c01.data_pos['v_total_pos'].diff() / 0.1

# Settings
#gnss_start_index = np.where(gps_c01.data_pos['v_total_pos'] > vel_trashold)[0][0]
imu_start_idx = 40*400
#imu_start_idx = gnss_start_index * 40

imu_end_idx = 82*400

imu_time = imu_c01.time[imu_start_idx:imu_end_idx]
gps_time = gps_c01.data['#Data contains: GPS time of week of the navigation epoch (ms)'] / 1000 - gps_c01.data['#Data contains: GPS time of week of the navigation epoch (ms)'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]

# Compare
s = gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()

ax_comp.scatter(time,v, label='2021.03.16 - Seilbahn - C01')

#%% 16.03.2022 C03
# C03
# Seilbahnrinne
gps_c03 = GPSData()
path_c03 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-16_Nordkette_avalanche\C03\GPS\ava210316_C03_GPS.txt'
gps_c03.path = path_c03
gps_c03.read_data()
gps_c03.calc_pos_vel()

imu_c03 = ImuData()
imu_path_c03 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-16_Nordkette_avalanche\C03\Leader\ava210316_C03_Leader.txt'
imu_c03.path = imu_path_c03
imu_c03.read_data_pd()

# Calcuations 
gps_acc = gps_c03.data_pos['v_total_pos'].diff() / 0.1

# Settings
#gnss_start_index = np.where(gps_c01.data_pos['v_total_pos'] > vel_trashold)[0][0]
imu_start_idx = 73*400
#imu_start_idx = gnss_start_index * 40

imu_end_idx = 110*400

imu_time = imu_c03.time[imu_start_idx:imu_end_idx]
gps_time = gps_c03.data['#Data contains:GPS time of week of the navigation epoch (ms)'] / 1000 - gps_c03.data['#Data contains:GPS time of week of the navigation epoch (ms)'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]


# Compare
s = gps_c03.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c03.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c03.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c03.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()
ax_comp.scatter(time,v, label='2021.03.16 - Seilbahn - C03')

#%% 23.01.2022

# Experiment 23.01.2022

gps_c01 = GPSData()
path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-01-23_Nordkette_avalanche\C01\GPS\ava220123_C01.txt'
gps_c01.path = path_c01
gps_c01.read_data()
gps_c01.calc_pos_vel()

imu_c01 = ImuData()
imu_path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-01-23_Nordkette_avalanche\C01\Leader\ava220123_C01_Leader.txt'
imu_c01.path = imu_path_c01
imu_c01.read_data_pd()

# Calcuations 
gps_acc = gps_c01.data_pos['v_total_pos'].diff() / 0.1
doppler_acc = gps_c01.data_pos['v_total'].diff() / 0.1

# Settings
#gnss_start_index = np.where(gps_c01.data_pos['v_total'] > vel_trashold)[0][0]
imu_start_idx = 49*400
#imu_start_idx = gnss_start_index * 40

imu_end_idx = 95*400
#imu_start_idx = 0
#imu_end_idx = -1
imu_time = imu_c01.time[imu_start_idx:imu_end_idx]
gps_time = gps_c01.data['#iTow[ms]'] / 1000 - gps_c01.data['#iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]

# Compare
s = gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()
ax_comp.scatter(time,v, label='2022.01.23 - Seilbahn - C01')

#start = datetime(2022,1,23,7,30,20)
#end = datetime(2022,1,23,7,31,30)
#%% 03.02.2022

# Experiment 03.02.2022
# C 10
# Seilbahnrinne
gps_c10 = GPSData()
path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-03_Nordkette_avalanche\C10\GPS\ava220203_seilbahn_C10_gnss.txt'
gps_c10.path = path_c10
gps_c10.read_data()
gps_c10.calc_pos_vel()

imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-03_Nordkette_avalanche\C10\Leader\ava220203_seilbahn_C10_imu.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Calcuations 
gps_acc = gps_c10.data_pos['v_total_pos'].diff() / 0.1
doppler_acc = gps_c10.data_pos['v_total'].diff() / 0.1

# Settings
#gnss_start_index = np.where(gps_c01.data_pos['v_total'] > vel_trashold)[0][0]
imu_start_idx = 140*400
#imu_start_idx = gnss_start_index * 40

imu_end_idx = 185*400


imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
gps_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]

# Compare
s = gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()
ax_comp.scatter(time, v, label='2022.02.03 - Seilbahn - C10')

#Juliusrinne

gps_c10 = GPSData()
path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-03_Nordkette_avalanche\C10\GPS\ava220203_julius_C10_gnss.txt'
gps_c10.path = path_c10
gps_c10.read_data()
gps_c10.calc_pos_vel()

imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-03_Nordkette_avalanche\C10\Leader\ava220203_julius_C10_imu.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Calcuations 
gps_acc = gps_c10.data_pos['v_total_pos'].diff() / 0.1
doppler_acc = gps_c10.data_pos['v_total'].diff() / 0.1

# Settings
gnss_start_index = np.where(gps_c01.data_pos['v_total'] > 1)[0][0]
#imu_start_idx = 75*400
imu_start_idx = gnss_start_index * 40

imu_end_idx = 105*400
#imu_start_idx = 0
#imu_end_idx = -1

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
gps_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]


# Compare
s = gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()
#ax_comp.scatter(s,v, label='2022.02.03 - Julius - C10')

#start = datetime(2022,2,3,7,40,40)
#end = datetime(2022,2,3,7,41,30)


#%% 22.02.2022 C07
# Experiment 22.02.2022

gps_c10 = GPSData()
path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C07\GPS\220222_C07_avalanche_GPS.txt'
gps_c10.path = path_c10
gps_c10.read_data()
gps_c10.calc_pos_vel()

imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C07\Leader\220222_C07_avalanche_Leader.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Calcuations 
gps_acc = gps_c10.data_pos['v_total_pos'].diff() / 0.1
doppler_acc = gps_c10.data_pos['v_total'].diff() / 0.1


# Settings
#gnss_start_index = np.where(gps_c01.data_pos['v_total'] > vel_trashold)[0][0]
imu_start_idx = 46*400
#imu_start_idx = gnss_start_index * 40

imu_end_idx = 85*400
#imu_start_idx = 0
#imu_end_idx = -1

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
gps_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]

imu_vel = np.zeros_like(imu_time)
dt = 1/400
j = 1
for i in range(imu_start_idx+1,imu_end_idx):
    imu_vel[j] = ((imu_c10.acc_tot[i] - 9.81) + (imu_c10.acc_tot[i-1] - 9.81)) / 2 * dt + imu_vel[j-1]
    j += 1


# Compare
s = gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()
ax_comp.scatter(time, v, label='2022.02.22 - Seilbahn - C07')

#%% 22.02.2022 C09
# C09
gps_c10 = GPSData()
path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C09\GPS\220222_C09_avalanche_GPS.txt'
gps_c10.path = path_c10
gps_c10.read_data()
gps_c10.calc_pos_vel()

imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C09\Leader\220222_C09_avalanche_Leader.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Calcuations 
gps_acc = gps_c10.data_pos['v_total_pos'].diff() / 0.1
doppler_acc = gps_c10.data_pos['v_total'].diff() / 0.1

# Settings
#gnss_start_index = np.where(gps_c01.data_pos['v_total'] > vel_trashold)[0][0]
imu_start_idx = 28*400
#imu_start_idx = gnss_start_index * 40

imu_end_idx = 68*400
#imu_start_idx = 0
#imu_end_idx = -1

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
gps_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]


# Compare
s = gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()
ax_comp.scatter(time,v, label='2022.02.22 - Seilbahn - C09')

#%%22.02.2022 C10
# C10
gps_c10 = GPSData()
path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C10\GPS\220222_C10_avalanche_GPS.txt'
gps_c10.path = path_c10
gps_c10.read_data()
gps_c10.calc_pos_vel()

imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C10\Leader\220222_C10_avalanche_Leader.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Calcuations 
gps_acc = gps_c10.data_pos['v_total_pos'].diff() / 0.1
doppler_acc = gps_c10.data_pos['v_total'].diff() / 0.1

# Settings
#gnss_start_index = np.where(gps_c01.data_pos['v_total'] > vel_trashold)[0][0]
imu_start_idx = 61*400
#imu_start_idx = gnss_start_index * 40

imu_end_idx = 100*400
#imu_start_idx = 0
#imu_end_idx = -1

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
gps_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]

# Compare
s = gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
v = gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c10.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
time = gps_time - gps_time.iloc[0]
time /= time.max()
ax_comp.scatter(time,v, label='2022.02.22 - Seilbahn - C10')
ax_comp.legend()

fig_compare.tight_layout()
fig_compare.savefig('./plots/all_avalanches_v_t.png', dpi=300)
fig_compare.savefig(r'C:\git_rep\dissertation\papers\GPS_paper\figures\exp\all_avalanches_v_t.png', dpi=300)
