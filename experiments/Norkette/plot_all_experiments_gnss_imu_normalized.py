# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 11:56:08 2022

@author: neuhauser
"""

#%% Libs
import matplotlib.pyplot as plt
from datetime import datetime
import numpy as np
import pandas as pd

from classes.GPS_Class import GPSData
from classes.IMU_Class import ImuData

import matplotlib
matplotlib.rcParams.update({
    'font.size' : 32
})

#%% Functions

def moving_average(array, window_size):
  
    i = 0
    # Initialize an empty list to store moving averages
    moving_averages = np.zeros_like(array, dtype = float)
      
    # Loop through the array to
    while i < len(array):
        
        if i < int(window_size/2):
            window_average = round(np.sum(array[:i+1+int(window_size/2)]) / len(array[:i+1+int(window_size/2)]), 2)
            moving_averages[i] = window_average
            i += 1
            
        elif i >= int(window_size/2) and i < len(array) - int(window_size/2):
            window_average = round(np.sum(array[i-int(window_size/2):i+1+int(window_size/2)] / window_size), 2)
            moving_averages[i] = window_average
            i += 1
            
        elif i >= len(array) - int(window_size/2):
            window_average = round(np.sum(array[i-int(window_size/2):] / len(array[i-int(window_size/2):])), 2)
            moving_averages[i] = window_average
            i += 1
        
      
    return moving_averages


#%% Settings

save_path = r"C:\git_rep\dissertation\papers\movement_paper\figures\exp"
# Colors

color_210315 = '#0400fa'
color_210316_C01 = '#fa9f18'
color_210316_C03 = '#18fa2e'
color_220123 = '#fa0005'
color_220203 = '#811df3'
color_220222_C07 = '#00f1ee'
color_220222_C09 = '#ff04ff'
color_220222_C10 = '#ffff00'

# Linestyles
ls_imu = 'solid'
ls_pos = 'dashed'
ls_doppler = 'dotted'
# Save
save = False
v_t_df = pd.DataFrame(data=np.zeros((8,5)) ,columns=["v[m/s]", "t[s]", "v_norm[-]", "t_norm[-]", "t_max[s]"])
# Smoothing
kernel_size = 80
kernel = np.ones(kernel_size) / kernel_size
kernel_size_gps = int(kernel_size/40)
kernel_gps = np.ones(kernel_size_gps) / kernel_size_gps
normalize = False

gnss_acc_roll = 50
#%% Read Data

# Experiment 15.03.2021
# C01
# Seilbahnrinne
gps_c01 = GPSData()
path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-15_Nordkette_avalanche\C01\GPS\ava210315_C01.txt'
gps_c01.path = path_c01
gps_c01.read_data()
gps_c01.calc_pos_vel()


imu_c01 = ImuData()
imu_path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-15_Nordkette_avalanche\C01\Leader\ava210315_C01_Leader.txt'
imu_c01.path = imu_path_c01
imu_c01.read_data_pd()

# Settings
imu_start_idx = 38*400
imu_end_idx = 80*400

gps_v_210315 = gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_v_210315 = np.convolve(gps_v_210315, kernel_gps, mode='same')
# Calc Local Position
gps_c01.to_local()
gps_pos_210315 = gps_c01.pos_df.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_pos_210315 -= gps_pos_210315.iloc[0, :]
gps_v_210315_orig = gps_v_210315


imu_time = imu_c01.time[imu_start_idx:imu_end_idx]
imu_time = (imu_time - imu_time.min())/ (imu_time.max() - imu_time.min())
imu_time_210315 = imu_time

gps_time = gps_c01.data['#Data contains: GPS time of week of the navigation epoch (ms)'] / 1000 - gps_c01.data['#Data contains: GPS time of week of the navigation epoch (ms)'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_time_orig = gps_time - gps_time.min()
gps_time_orig_210315 = gps_time_orig.reset_index(drop=True)
gps_time = (gps_time - gps_time.min()) / (gps_time.max() - gps_time.min())
gps_time = gps_time.reset_index(drop=True)
gps_time_210315 = gps_time
gps_time_210315_orig = gps_time_orig

# Smoothing
imu_acc_210315 = imu_c01.acc_tot[imu_start_idx:imu_end_idx]
#imu_acc_210315 = moving_average(imu_acc_210315, 80)
#imu_acc_210315 = np.convolve(imu_acc_210315, kernel, mode='full')

imu_rot_210315 = imu_c01.gyro_tot[imu_start_idx:imu_end_idx]
imu_rot_210315 = np.convolve(imu_rot_210315, kernel, mode='same')



# Calc Local Position
gps_c01.to_local()
gps_pos_210315 = gps_c01.pos_df.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_pos_210315 -= gps_pos_210315.iloc[0, :]
gps_v_210315_orig = gps_v_210315
gnss_acc_210315 = gps_v_210315_orig / gps_time_orig

# Normalization
if normalize:
    imu_acc_210315 = (imu_acc_210315 - 9.81) / (imu_acc_210315 - 9.81).max()
    imu_rot_210315 = imu_rot_210315 / imu_rot_210315.max()
gps_v_210315 = gps_v_210315 / gps_v_210315.max()

gnss_acc_210315 = pd.DataFrame(np.diff(gps_v_210315_orig) / np.diff(gps_time_orig_210315))
gnss_acc_210315_smooth = gnss_acc_210315.rolling(gnss_acc_roll, min_periods=1).mean()


# Experiment 16.03.2021
# C01
# Seilbahnrinne
gps_c01 = GPSData()
path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-16_Nordkette_avalanche\C01\GPS\ava210316_C01_GPS.txt'
gps_c01.path = path_c01
gps_c01.read_data()
gps_c01.calc_pos_vel()

imu_c01 = ImuData()
imu_path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-16_Nordkette_avalanche\C01\Leader\ava210316_C01_Leader.txt'
imu_c01.path = imu_path_c01
imu_c01.read_data_pd()

# Settings
imu_start_idx = 40*400
imu_end_idx = 82*400

imu_time = imu_c01.time[imu_start_idx:imu_end_idx]
imu_time = (imu_time - imu_time.min())/ (imu_time.max() - imu_time.min())
imu_time_210316 = imu_time

gps_time = gps_c01.data['#Data contains: GPS time of week of the navigation epoch (ms)'] / 1000 - gps_c01.data['#Data contains: GPS time of week of the navigation epoch (ms)'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_time_orig = gps_time - gps_time.min()
gps_time_orig_210316 = gps_time_orig.reset_index(drop=True)
gps_time = (gps_time - gps_time.min()) / (gps_time.max() - gps_time.min())
gps_time_210316 = gps_time

# Smoothing 
imu_acc_210316 = imu_c01.acc_tot[imu_start_idx:imu_end_idx]
imu_acc_210316 = np.convolve(imu_acc_210316, kernel, mode='same')
               
imu_rot_210316 = imu_c01.gyro_tot[imu_start_idx:imu_end_idx]
imu_rot_210316 = np.convolve(imu_rot_210316, kernel, mode='same')

gps_v_210316 = gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_v_210316 = np.convolve(gps_v_210316, kernel_gps, mode='same')

# Calc Local Position
gps_c01.to_local()
gps_pos_210316 = gps_c01.pos_df.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_pos_210316 -= gps_pos_210316.iloc[0, :]
gps_v_210316_orig = gps_v_210316
               
# Nomralization
if normalize:             
    imu_acc_210316 = (imu_c01.acc_tot[imu_start_idx:imu_end_idx] - 9.81) / (imu_c01.acc_tot[imu_start_idx:imu_end_idx] - 9.81).max()
    imu_rot_210316 = imu_c01.gyro_tot[imu_start_idx:imu_end_idx] / imu_c01.gyro_tot[imu_start_idx:imu_end_idx].max()
gps_v_210316 = gps_v_210316 / gps_v_210316.max()

gnss_acc_210316 = pd.DataFrame(np.diff(gps_v_210316_orig) / np.diff(gps_time_orig_210316))
gnss_acc_210316_smooth = gnss_acc_210316.rolling(gnss_acc_roll, min_periods=1).mean()


# C03
# Seilbahnrinne
gps_c03 = GPSData()
path_c03 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-16_Nordkette_avalanche\C03\GPS\ava210316_C03_GPS.txt'
gps_c03.path = path_c03
gps_c03.read_data()
gps_c03.calc_pos_vel()

imu_c03 = ImuData()
imu_path_c03 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-16_Nordkette_avalanche\C03\Leader\ava210316_C03_Leader.txt'
imu_c03.path = imu_path_c03
imu_c03.read_data_pd()

# Settings
imu_start_idx = 73*400
imu_end_idx = 112*400

imu_time = imu_c03.time[imu_start_idx:imu_end_idx]
imu_time = (imu_time - imu_time.min())/ (imu_time.max() - imu_time.min())
imu_time_210316_c03 = imu_time

gps_time = gps_c03.data['#Data contains:GPS time of week of the navigation epoch (ms)'] / 1000 - gps_c03.data['#Data contains:GPS time of week of the navigation epoch (ms)'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_time_orig = gps_time - gps_time.min()
gps_time_orig_210316_c03 = gps_time_orig.reset_index(drop=True)
gps_time = (gps_time - gps_time.min()) / (gps_time.max() - gps_time.min())
gps_time_210316_c03 = gps_time

# Smoothing
imu_acc_210316_c03 = imu_c03.acc_tot[imu_start_idx:imu_end_idx]
imu_acc_210316_c03 = np.convolve(imu_acc_210316_c03, kernel, mode='same')
imu_rot_210316_c03 = imu_c03.gyro_tot[imu_start_idx:imu_end_idx]
imu_rot_210316_c03 = np.convolve(imu_rot_210316_c03, kernel, mode='same')
gps_v_210316_c03 = gps_c03.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_v_210316_c03 = np.convolve(gps_v_210316_c03, kernel_gps, mode='same')

# Calc Local Position
gps_c03.to_local()
gps_pos_210316_c03 = gps_c03.pos_df.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_pos_210316_c03 -= gps_pos_210316_c03.iloc[0, :]
gps_v_210316_c03_orig = gps_v_210316_c03

# Nomralization
if normalize:
    imu_acc_210316_c03 = (imu_c03.acc_tot[imu_start_idx:imu_end_idx] - 9.81) / (imu_c03.acc_tot[imu_start_idx:imu_end_idx] - 9.81).max()
    imu_rot_210316_c03 = imu_c03.gyro_tot[imu_start_idx:imu_end_idx] / imu_c03.gyro_tot[imu_start_idx:imu_end_idx].max()
gps_v_210316_c03 = gps_v_210316_c03 / gps_v_210316_c03.max()

gnss_acc_210316_c03 = pd.DataFrame(np.diff(gps_v_210316_c03_orig) / np.diff(gps_time_orig_210316_c03))
gnss_acc_210316_c03_smooth = gnss_acc_210316_c03.rolling(gnss_acc_roll, min_periods=1).mean()


# Experiment 23.01.2022
# C01
gps_c01 = GPSData()
path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-01-23_Nordkette_avalanche\C01\GPS\ava220123_C01.txt'
gps_c01.path = path_c01
gps_c01.read_data()
gps_c01.calc_pos_vel()

imu_c01 = ImuData()
imu_path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-01-23_Nordkette_avalanche\C01\Leader\ava220123_C01_Leader.txt'
imu_c01.path = imu_path_c01
imu_c01.read_data_pd()

# Settings
imu_start_idx = 49*400
imu_end_idx = 98*400

imu_time = imu_c01.time[imu_start_idx:imu_end_idx]
imu_time = (imu_time - imu_time.min())/ (imu_time.max() - imu_time.min())
imu_time_220123 = imu_time

gps_time = gps_c01.data['#iTow[ms]'] / 1000 - gps_c01.data['#iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_time_orig = gps_time - gps_time.min()
gps_time_orig_220123 = gps_time_orig.reset_index(drop=True)
gps_time = (gps_time - gps_time.min()) / (gps_time.max() - gps_time.min())
gps_time_220123 = gps_time

# Smoothing
imu_acc_220123 = imu_c01.acc_tot[imu_start_idx:imu_end_idx]
imu_acc_220123 = np.convolve(imu_acc_220123, kernel, mode='same')
imu_rot_220123 = imu_c01.gyro_tot[imu_start_idx:imu_end_idx]
imu_rot_220123 = np.convolve(imu_rot_220123, kernel, mode='same')
gps_v_220123 = gps_c01.data_pos['v_total'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_v_220123 = np.convolve(gps_v_220123, kernel_gps, mode='same')

# Calc Local Position
gps_c01.to_local()
gps_pos_220123 = gps_c01.pos_df.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_pos_220123 -= gps_pos_220123.iloc[0, :]
gps_pos_220123["h_dist"] = np.linalg.norm([gps_pos_220123["X"], gps_pos_220123["Y"]], axis=0)
gps_v_220123_orig = gps_v_220123

# Normalization
if normalize:
    imu_acc_220123 = (imu_c01.acc_tot[imu_start_idx:imu_end_idx] - 9.81) / (imu_c01.acc_tot[imu_start_idx:imu_end_idx] - 9.81).max()
    imu_rot_220123 = imu_c01.gyro_tot[imu_start_idx:imu_end_idx] / imu_c01.gyro_tot[imu_start_idx:imu_end_idx].max()
gps_v_220123 = gps_v_220123 / gps_v_220123.max()

gnss_acc_220123 = pd.DataFrame(np.diff(gps_v_220123_orig) / np.diff(gps_time_orig_220123))
gnss_acc_220123_smooth = gnss_acc_220123.rolling(gnss_acc_roll, min_periods=1).mean()


# Experiment 03.02.2022
# C 10
# Seilbahnrinne
gps_c10 = GPSData()
path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-03_Nordkette_avalanche\C10\GPS\ava220203_seilbahn_C10_gnss.txt'
gps_c10.path = path_c10
gps_c10.read_data()
gps_c10.calc_pos_vel()

imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-03_Nordkette_avalanche\C10\Leader\ava220203_seilbahn_C10_imu.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Settings
imu_start_idx = 140*400
imu_end_idx = 189*400

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
imu_time = (imu_time - imu_time.min())/ (imu_time.max() - imu_time.min())
imu_time_220203 = imu_time

gps_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_time_orig = gps_time - gps_time.min()
gps_time_orig_220203 = gps_time_orig.reset_index(drop=True)
gps_time = (gps_time - gps_time.min()) / (gps_time.max() - gps_time.min())
gps_time_220203 = gps_time

# Smoothing
imu_acc_220203 = imu_c10.acc_tot[imu_start_idx:imu_end_idx]
imu_acc_220203 = np.convolve(imu_acc_220203, kernel, mode='same')                 
imu_rot_220203 = imu_c10.gyro_tot[imu_start_idx:imu_end_idx]
imu_rot_220203 = np.convolve(imu_rot_220203, kernel, mode='same')                 
gps_v_220203 = gps_c10.data_pos['v_total'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_v_220203 = np.convolve(gps_v_220203, kernel_gps, mode='same')

# Calc Local Position
gps_c10.to_local()
gps_pos_220203 = gps_c10.pos_df.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_pos_220203 -= gps_pos_220203.iloc[0, :]
gps_v_220203_orig = gps_v_220203

# Nomalization   
if normalize:            
    imu_acc_220203 = (imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81) / (imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81).max()
    imu_rot_220203 = imu_c10.gyro_tot[imu_start_idx:imu_end_idx] / imu_c10.gyro_tot[imu_start_idx:imu_end_idx].max()
gps_v_220203 = gps_v_220203 / gps_v_220203.max()

gnss_acc_220203 = pd.DataFrame(np.diff(gps_v_220203_orig) / np.diff(gps_time_orig_220203))
gnss_acc_220203_smooth = gnss_acc_220203.rolling(gnss_acc_roll, min_periods=1).mean()


# Experiment 22.02.2022
# C07
gps_c10 = GPSData()
path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C07\GPS\220222_C07_avalanche_GPS.txt'
gps_c10.path = path_c10
gps_c10.read_data()
gps_c10.calc_pos_vel()

imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C07\Leader\220222_C07_avalanche_Leader.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Settings
imu_start_idx = 46*400
imu_end_idx = 85*400

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
imu_time = (imu_time - imu_time.min())/ (imu_time.max() - imu_time.min())
imu_time_220222_c07 = imu_time

gps_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_time_orig = gps_time - gps_time.min()
gps_time_orig_220222_c07 = gps_time_orig.reset_index(drop=True)
gps_time = (gps_time - gps_time.min()) / (gps_time.max() - gps_time.min())
gps_time_220222_c07 = gps_time

# Smoothing
imu_acc_220222_c07 = imu_c10.acc_tot[imu_start_idx:imu_end_idx]
imu_acc_220222_c07 = np.convolve(imu_acc_220222_c07, kernel, mode='same')
imu_rot_220222_c07 = imu_c10.gyro_tot[imu_start_idx:imu_end_idx]
imu_rot_220222_c07 = np.convolve(imu_rot_220222_c07, kernel, mode='same')
gps_v_220222_c07 = gps_c10.data_pos['v_total'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_v_220222_c07 = np.convolve(gps_v_220222_c07, kernel_gps, mode='same')

# Calc Local Position
gps_c10.to_local()
gps_pos_220222_c07 = gps_c10.pos_df.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_pos_220222_c07 -= gps_pos_220222_c07.iloc[0, :]
gps_v_220222_c07_orig = gps_v_220222_c07

# Normalization
if normalize:
    imu_acc_220222_c07 = (imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81) / (imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81).max()
    imu_rot_220222_c07 = imu_c10.gyro_tot[imu_start_idx:imu_end_idx] / imu_c10.gyro_tot[imu_start_idx:imu_end_idx].max()
gps_v_220222_c07 = gps_v_220222_c07 / gps_v_220222_c07.max()

gnss_acc_220222_c07 = pd.DataFrame(np.diff(gps_v_220222_c07_orig) / np.diff(gps_time_orig_220222_c07))
gnss_acc_220222_c07_smooth = gnss_acc_220222_c07.rolling(gnss_acc_roll, min_periods=1).mean()

# C09
gps_c10 = GPSData()
path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C09\GPS\220222_C09_avalanche_GPS.txt'
gps_c10.path = path_c10
gps_c10.read_data()
gps_c10.calc_pos_vel()

imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C09\Leader\220222_C09_avalanche_Leader.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Settings
imu_start_idx = 28*400
imu_end_idx = 70*400

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
imu_time = (imu_time - imu_time.min())/ (imu_time.max() - imu_time.min())
imu_time_220222_c09 = imu_time

gps_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_time_orig = gps_time - gps_time.min()
gps_time_orig_220222_c09 = gps_time_orig.reset_index(drop=True)
gps_time = (gps_time - gps_time.min()) / (gps_time.max() - gps_time.min())
gps_time_220222_c09 = gps_time

# Smoothing
imu_acc_220222_c09 = imu_c10.acc_tot[imu_start_idx:imu_end_idx]
imu_acc_220222_c09 = np.convolve(imu_acc_220222_c09, kernel, mode='same')
imu_rot_220222_c09 = imu_c10.gyro_tot[imu_start_idx:imu_end_idx]
imu_rot_220222_c09 = np.convolve(imu_rot_220222_c09, kernel, mode='same')
gps_v_220222_c09 = gps_c10.data_pos['v_total'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_v_220222_c09 = np.convolve(gps_v_220222_c09, kernel_gps, mode='same')

# Calc Local Position
gps_c10.to_local()
gps_pos_220222_c09 = gps_c10.pos_df.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_pos_220222_c09 -= gps_pos_220222_c09.iloc[0, :]
gps_v_220222_c09_orig = gps_v_220222_c09

# Nomralization
if normalize:
    imu_acc_220222_c09 = (imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81) / (imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81).max()
    imu_rot_220222_c09 = imu_c10.gyro_tot[imu_start_idx:imu_end_idx] / imu_c10.gyro_tot[imu_start_idx:imu_end_idx].max()
gps_v_220222_c09 = gps_v_220222_c09 / gps_v_220222_c09.max()

gnss_acc_220222_c09 = pd.DataFrame(np.diff(gps_v_220222_c09_orig) / np.diff(gps_time_orig_220222_c09))
gnss_acc_220222_c09_smooth = gnss_acc_220222_c09.rolling(gnss_acc_roll, min_periods=1).mean()

# C10
gps_c10 = GPSData()
path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C10\GPS\220222_C10_avalanche_GPS.txt'
gps_c10.path = path_c10
gps_c10.read_data()
gps_c10.calc_pos_vel()

imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C10\Leader\220222_C10_avalanche_Leader.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Settings
imu_start_idx = 61*400
imu_end_idx = 102*400

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
imu_time = (imu_time - imu_time.min())/ (imu_time.max() - imu_time.min())
imu_time_220222_c10 = imu_time

gps_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000
gps_time = gps_time.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_time_orig = gps_time - gps_time.min()
gps_time_orig_220222_c10 = gps_time_orig.reset_index(drop=True)
gps_time = (gps_time - gps_time.min()) / (gps_time.max() - gps_time.min())
gps_time_220222_c10 = gps_time

# Smoothing
imu_acc_220222_c10 = imu_c10.acc_tot[imu_start_idx:imu_end_idx]
imu_acc_220222_c10 = np.convolve(imu_acc_220222_c10, kernel, mode='same')
imu_rot_220222_c10 = imu_c10.gyro_tot[imu_start_idx:imu_end_idx]
imu_rot_220222_c10 = np.convolve(imu_rot_220222_c10, kernel, mode='same')
gps_v_220222_c10 = gps_c10.data_pos['v_total'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_v_220222_c10 = np.convolve(gps_v_220222_c10, kernel_gps, mode='same')

# Calc Local Position
gps_c10.to_local()
gps_pos_220222_c10 = gps_c10.pos_df.iloc[int(imu_start_idx/40):int(imu_end_idx/40)]
gps_pos_220222_c10 -= gps_pos_220222_c10.iloc[0, :]
gps_v_220222_c10_orig = gps_v_220222_c10

# Nomralization
if normalize:
    imu_acc_220222_c10 = (imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81) / (imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81).max()
    imu_rot_220222_c10 = imu_c10.gyro_tot[imu_start_idx:imu_end_idx] / imu_c10.gyro_tot[imu_start_idx:imu_end_idx].max()
gps_v_220222_c10 = gps_v_220222_c10 / gps_v_220222_c10.max()

gnss_acc_220222_c10 = pd.DataFrame(np.diff(gps_v_220222_c10_orig) / np.diff(gps_time_orig_220222_c10))
gnss_acc_220222_c10_smooth = gnss_acc_220222_c10.rolling(gnss_acc_roll, min_periods=1).mean()

#%% Plot Data
#Direct input 

fig, (ax_gnss, ax_bxplt) = plt.subplots(2,1, figsize=(30, 15), gridspec_kw={'height_ratios': [3, 1]})
 
# Plots
ax_gnss.plot(gps_time_210315, gps_v_210315, label="210315", color=color_210315)
ax_gnss.plot(gps_time_210316, gps_v_210316, label="210316_C01", color=color_210316_C01)
ax_gnss.plot(gps_time_210316_c03, gps_v_210316_c03, label="210316_C03", color=color_210316_C03)
ax_gnss.plot(gps_time_220123, gps_v_220123, label="220123", color=color_220123)
ax_gnss.plot(gps_time_220203, gps_v_220203, label="220203", color=color_220203)
ax_gnss.plot(gps_time_220222_c07, gps_v_220222_c07, label="220222_C07", color=color_220222_C07)
ax_gnss.plot(gps_time_220222_c09, gps_v_220222_c09, label="220222_C09", color=color_220222_C09)
ax_gnss.plot(gps_time_220222_c10, gps_v_220222_c10, label="220222_C10", color=color_220222_C10)

ax_gnss.grid()
ax_gnss.legend(bbox_to_anchor=(1, 1))
ax_gnss.set_ylabel('v/v_max')




# Analyze when v_max happens

t_max = []
gps_time_210315.reset_index(drop=True)
t_max.append(gps_time_210315.iloc[gps_v_210315.argmax()])

gps_time_210316.reset_index(drop=True)
t_max.append(gps_time_210316.iloc[gps_v_210316.argmax()])

gps_time_210316_c03.reset_index(drop=True)
t_max.append(gps_time_210316_c03.iloc[gps_v_210316_c03.argmax()])

gps_time_220123.reset_index(drop=True)
t_max.append(gps_time_220123.iloc[gps_v_220123.argmax()])

gps_time_220203.reset_index(drop=True)
t_max.append(gps_time_220203.iloc[gps_v_220203.argmax()])

gps_time_220222_c07.reset_index(drop=True)
t_max.append(gps_time_220222_c07.iloc[gps_v_220222_c07.argmax()])

gps_time_220222_c09.reset_index(drop=True)
t_max.append(gps_time_220222_c09.iloc[gps_v_220222_c09.argmax()])

gps_time_220222_c10.reset_index(drop=True)
t_max.append(gps_time_220222_c10.iloc[gps_v_220222_c10.argmax()])

#dydx = np.diff(gps_v_210315)/np.diff(gps_time_210315)

data = [t_max]

ax_bxplt.boxplot(data, vert=False, labels=["v_max"], autorange=True)
#ax_bxplt.minorticks_on()
#ax_bxplt.tick_params(axis='x', which='minor', bottom=False)
ax_bxplt.set_xticks([0.1, 0.3, 0.5, 0.7, 0.9], minor=True)
ax_bxplt.set_xlim(ax_gnss.get_xlim())
ax_bxplt.grid(axis='x', which='both')
ax_bxplt.set_xlabel('t/t_max')

fig.tight_layout()
fig.savefig(save_path + "/all_avalanches_v_t_smoothing_window.png")

acc_boundary = 0.3541
flow_boundary = 0.541

#%% Plot Analysis
alpha_one = 0.1
alpha_two = 0.3
alpha_three = 0.5


fig, ((ax_gnss, ax_acc, ax_rot)) = plt.subplots(3,1, figsize=(30, 30))
 
# Plots
ax_gnss.plot(gps_time_210315, gps_v_210315, label='210315', color=color_210315)
ax_acc.plot(imu_time_210315, imu_acc_210315, color=color_210315)
ax_rot.plot(imu_time_210315, imu_rot_210315, color=color_210315)

ax_gnss.plot(gps_time_210316, gps_v_210316, label='210316_C01', color=color_210316_C01)
ax_acc.plot(imu_time_210316, imu_acc_210316, color=color_210316_C01)
ax_rot.plot(imu_time_210316, imu_rot_210316, color=color_210316_C01)

ax_gnss.plot(gps_time_210316_c03, gps_v_210316_c03, label='210316_C03', color=color_210316_C03)
ax_acc.plot(imu_time_210316_c03, imu_acc_210316_c03, color=color_210316_C03)
ax_rot.plot(imu_time_210316_c03, imu_rot_210316_c03, color=color_210316_C03)

ax_gnss.plot(gps_time_220123, gps_v_220123, label='220123', color=color_220123)
ax_acc.plot(imu_time_220123, imu_acc_220123, color=color_220123)
ax_rot.plot(imu_time_220123, imu_rot_220123, color=color_220123)

ax_gnss.plot(gps_time_220203, gps_v_220203, label='220203', color=color_220203)
ax_acc.plot(imu_time_220203, imu_acc_220203, color=color_220203)
ax_rot.plot(imu_time_220203, imu_rot_220203, color=color_220203)

ax_gnss.plot(gps_time_220222_c07, gps_v_220222_c07, label='220222_C07', color=color_220222_C07)
ax_acc.plot(imu_time_220222_c07, imu_acc_220222_c07, color=color_220222_C07)
ax_rot.plot(imu_time_220222_c07, imu_rot_220222_c07, color=color_220222_C07)

ax_gnss.plot(gps_time_220222_c09, gps_v_220222_c09, label='220222_C09', color=color_220222_C09)
ax_acc.plot(imu_time_220222_c09, imu_acc_220222_c09, color=color_220222_C09)
ax_rot.plot(imu_time_220222_c09, imu_rot_220222_c09, color=color_220222_C09)

ax_gnss.plot(gps_time_220222_c10, gps_v_220222_c10, label='220222_C10', color=color_220222_C10)
ax_acc.plot(imu_time_220222_c10, imu_acc_220222_c10, color=color_220222_C10)
ax_rot.plot(imu_time_220222_c10, imu_rot_220222_c10, color=color_220222_C10)

ax_gnss.fill_between(np.array((0,acc_boundary)), 0, 1, color='gray', alpha=alpha_one)
ax_gnss.fill_between(np.array((acc_boundary, flow_boundary)), 0, 1, color='gray', alpha=alpha_two)
ax_gnss.fill_between(np.array((flow_boundary, 1)), 0, 1, color='gray', alpha=alpha_three)

ax_acc.fill_between(np.array((0,acc_boundary)), 0, 160, color='gray', alpha=alpha_one)
ax_acc.fill_between(np.array((acc_boundary, flow_boundary)), 0, 160, color='gray', alpha=alpha_two)
ax_acc.fill_between(np.array((flow_boundary, 1)), 0, 160, color='gray', alpha=alpha_three)

ax_rot.fill_between(np.array((0,acc_boundary)), 0, 2000, color='gray', alpha=alpha_one)
ax_rot.fill_between(np.array((acc_boundary, flow_boundary)), 0, 2000, color='gray', alpha=alpha_two)
ax_rot.fill_between(np.array((flow_boundary, 1)), 0, 2000, color='gray', alpha=alpha_three)

# Set Titles
#ax_acc.set_title('Accelerations')
#ax_rot.set_title('Rotation Rates')
#ax_gnss.set_title('GNSS Velocities')

# Set Axes
ax_rot.set_xlabel('t/t_max')
ax_gnss.set_ylabel('v/v_max')
if normalize:
    ax_acc.set_ylabel("acc/acc_max")
else:
    ax_acc.set_ylabel("Acceleration [m/s²]")

if normalize:
    ax_rot.set_ylabel("rot/rot_max")
else:
    ax_rot.set_ylabel("Rotation [°/s]")
    
ax_gnss.legend(bbox_to_anchor=(1, 1))
fig.tight_layout()
fig.savefig(save_path + "/all_avalanches_gnss_imu.png")

#%% Analysis

matplotlib.rcParams.update({
    'font.size' : 22
})

# Accelerations
# Acceleration Phase
acc_data = 0
flow_data = 0
deacc_data = 0

boundary = acc_boundary
idx = np.where(imu_time_210315 <= boundary)[0][-1]
acc_data = np.hstack([imu_acc_210315[:idx]])
idx = np.where(imu_time_210316 <= boundary)[0][-1]
acc_data = np.hstack([acc_data, imu_acc_210316[:idx]])
idx = np.where(imu_time_210316_c03 <= boundary)[0][-1]
acc_data = np.hstack([acc_data, imu_acc_210316_c03[:idx]])
idx = np.where(imu_time_220123 <= boundary)[0][-1]
acc_data = np.hstack([acc_data, imu_acc_220123[:idx]])
idx = np.where(imu_time_220203 <= boundary)[0][-1]
acc_data = np.hstack([acc_data, imu_acc_220203[:idx]])
idx = np.where(imu_time_220222_c07 <= boundary)[0][-1]
acc_data = np.hstack([acc_data, imu_acc_220222_c07[:idx]])
idx = np.where(imu_time_220222_c09 <= boundary)[0][-1]
acc_data = np.hstack([acc_data, imu_acc_220222_c09[:idx]])
idx = np.where(imu_time_220222_c10 <= boundary)[0][-1]
acc_data = np.hstack([acc_data, imu_acc_220222_c10[:idx]])

# Flow Phase
low_boundary = acc_boundary
boundary = flow_boundary

idx = np.where((imu_time_210315 <= boundary) & (imu_time_210315 > low_boundary))[0]
flow_data = np.hstack([imu_acc_210315[idx[0]:idx[-1]]])

idx = np.where((imu_time_210316 <= boundary) & (imu_time_210316 > low_boundary))[0]
flow_data = np.hstack([flow_data, imu_acc_210316[idx[0]:idx[-1]]])

idx = np.where((imu_time_210316_c03 <= boundary) & (imu_time_210316_c03 > low_boundary))[0]
flow_data = np.hstack([flow_data, imu_acc_210316_c03[idx[0]:idx[-1]]])

idx = np.where((imu_time_220123 <= boundary) & (imu_time_220123 > low_boundary))[0]
flow_data = np.hstack([flow_data, imu_acc_220123[idx[0]:idx[-1]]])

idx = np.where((imu_time_220203 <= boundary) & (imu_time_220203 > low_boundary))[0]
flow_data = np.hstack([flow_data, imu_acc_220203[idx[0]:idx[-1]]])

idx = np.where((imu_time_220222_c07 <= boundary) & (imu_time_220222_c07 > low_boundary))[0]
flow_data = np.hstack([flow_data, imu_acc_220222_c07[idx[0]:idx[-1]]])

idx = np.where((imu_time_220222_c09 <= boundary) & (imu_time_220222_c09 > low_boundary))[0]
flow_data = np.hstack([flow_data, imu_acc_220222_c09[idx[0]:idx[-1]]])

idx = np.where((imu_time_220222_c10 <= boundary) & (imu_time_220222_c10 > low_boundary))[0]
flow_data = np.hstack([flow_data, imu_acc_220222_c10[idx[0]:idx[-1]]])

# Deacc Phase
low_boundary = flow_boundary

idx = np.where(imu_time_210315 > low_boundary)[0][0]
deacc_data = np.hstack([imu_acc_210315[idx:]])
idx = np.where(imu_time_210316 > low_boundary)[0][0]
deacc_data = np.hstack([deacc_data, imu_acc_210316[idx:]])
idx = np.where(imu_time_210316_c03 > low_boundary)[0][0]
deacc_data = np.hstack([deacc_data, imu_acc_210316_c03[idx:]])
idx = np.where(imu_time_220123 > low_boundary)[0][0]
deacc_data = np.hstack([deacc_data, imu_acc_220123[idx:]])
idx = np.where(imu_time_220203 > low_boundary)[0][0]
deacc_data = np.hstack([deacc_data, imu_acc_220203[idx:]])
idx = np.where(imu_time_220222_c07 > low_boundary)[0][0]
deacc_data = np.hstack([deacc_data, imu_acc_220222_c07[idx:]])
idx = np.where(imu_time_220222_c09 > low_boundary)[0][0]
deacc_data = np.hstack([deacc_data, imu_acc_220222_c09[idx:]])
idx = np.where(imu_time_220222_c10 > low_boundary)[0][0]
deacc_data = np.hstack([deacc_data, imu_acc_220222_c10[idx:]])

data = [acc_data, flow_data, deacc_data]
labels = ["Acceleration", "Flow", "Deceleration"]

fig, ax = plt.subplots(figsize=(8,6))
ax.boxplot(data, showfliers=False, labels=labels)

if normalize:
    ax.set_ylabel("acc/acc_max")
else:
    ax.set_ylabel("Acceleration [m/s²]")
ax.grid()
fig.tight_layout()
fig.savefig(save_path + "/all_avalanches_imu_acc.png")

# Rotations
# Acceleration Phase
acc_data = 0
flow_data = 0
deacc_data = 0

boundary = acc_boundary
idx = np.where(imu_time_210315 <= boundary)[0][-1]
acc_data = np.hstack([imu_rot_210315[:idx]])
idx = np.where(imu_time_210316 <= boundary)[0][-1]
acc_data = np.hstack([acc_data, imu_rot_210316[:idx]])
idx = np.where(imu_time_210316_c03 <= boundary)[0][-1]
acc_data = np.hstack([acc_data, imu_rot_210316_c03[:idx]])
idx = np.where(imu_time_220123 <= boundary)[0][-1]
acc_data = np.hstack([acc_data, imu_rot_220123[:idx]])
idx = np.where(imu_time_220203 <= boundary)[0][-1]
acc_data = np.hstack([acc_data, imu_rot_220203[:idx]])
idx = np.where(imu_time_220222_c07 <= boundary)[0][-1]
acc_data = np.hstack([acc_data, imu_rot_220222_c07[:idx]])
idx = np.where(imu_time_220222_c09 <= boundary)[0][-1]
acc_data = np.hstack([acc_data, imu_rot_220222_c09[:idx]])
idx = np.where(imu_time_220222_c10 <= boundary)[0][-1]
acc_data = np.hstack([acc_data, imu_rot_220222_c10[:idx]])

# Flow Phase
low_boundary = acc_boundary
boundary = flow_boundary

idx = np.where((imu_time_210315 <= boundary) & (imu_time_210315 > low_boundary))[0]
flow_data = np.hstack([imu_rot_210315[idx[0]:idx[-1]]])

idx = np.where((imu_time_210316 <= boundary) & (imu_time_210316 > low_boundary))[0]
flow_data = np.hstack([flow_data, imu_rot_210316[idx[0]:idx[-1]]])

idx = np.where((imu_time_210316_c03 <= boundary) & (imu_time_210316_c03 > low_boundary))[0]
flow_data = np.hstack([flow_data, imu_rot_210316_c03[idx[0]:idx[-1]]])

idx = np.where((imu_time_220123 <= boundary) & (imu_time_220123 > low_boundary))[0]
flow_data = np.hstack([flow_data, imu_rot_220123[idx[0]:idx[-1]]])

idx = np.where((imu_time_220203 <= boundary) & (imu_time_220203 > low_boundary))[0]
flow_data = np.hstack([flow_data, imu_rot_220203[idx[0]:idx[-1]]])

idx = np.where((imu_time_220222_c07 <= boundary) & (imu_time_220222_c07 > low_boundary))[0]
flow_data = np.hstack([flow_data, imu_rot_220222_c07[idx[0]:idx[-1]]])

idx = np.where((imu_time_220222_c09 <= boundary) & (imu_time_220222_c09 > low_boundary))[0]
flow_data = np.hstack([flow_data, imu_rot_220222_c09[idx[0]:idx[-1]]])

idx = np.where((imu_time_220222_c10 <= boundary) & (imu_time_220222_c10 > low_boundary))[0]
flow_data = np.hstack([flow_data, imu_rot_220222_c10[idx[0]:idx[-1]]])

# Deacc Phase
low_boundary = flow_boundary

idx = np.where(imu_time_210315 > low_boundary)[0][0]
deacc_data = np.hstack([imu_rot_210315[idx:]])
idx = np.where(imu_time_210316 > low_boundary)[0][0]
deacc_data = np.hstack([deacc_data, imu_rot_210316[idx:]])
idx = np.where(imu_time_210316_c03 > low_boundary)[0][0]
deacc_data = np.hstack([deacc_data, imu_rot_210316_c03[idx:]])
idx = np.where(imu_time_220123 > low_boundary)[0][0]
deacc_data = np.hstack([deacc_data, imu_rot_220123[idx:]])
idx = np.where(imu_time_220203 > low_boundary)[0][0]
deacc_data = np.hstack([deacc_data, imu_rot_220203[idx:]])
idx = np.where(imu_time_220222_c07 > low_boundary)[0][0]
deacc_data = np.hstack([deacc_data, imu_rot_220222_c07[idx:]])
idx = np.where(imu_time_220222_c09 > low_boundary)[0][0]
deacc_data = np.hstack([deacc_data, imu_rot_220222_c09[idx:]])
idx = np.where(imu_time_220222_c10 > low_boundary)[0][0]
deacc_data = np.hstack([deacc_data, imu_rot_220222_c10[idx:]])

data = [acc_data, flow_data, deacc_data]

fig, ax = plt.subplots(figsize=(8,6))
ax.boxplot(data, showfliers=False, labels=labels)

if normalize:
    ax.set_ylabel("rot/rot_max")
else:
    ax.set_ylabel("Rotation [°/s]")
ax.grid()
fig.tight_layout()
fig.savefig(save_path + "/all_avalanches_imu_rot.png")

#%% Plot over Dropheight

fig, (ax_gnss, ax_bxplt) = plt.subplots(2,1, figsize=(30, 15), gridspec_kw={'height_ratios': [3, 1]})
 
# Plots

ax_gnss.plot(gps_pos_210315["h_dist"]/gps_pos_210315["Z"].iloc[-1], gps_v_210315_orig/np.sqrt(9.81 * gps_pos_210315["Z"].iloc[-1]), label="210315", color=color_210315)
ax_gnss.plot(gps_pos_210316["h_dist"]/gps_pos_210316["Z"].iloc[-1], gps_v_210316_orig/np.sqrt(9.81 * gps_pos_210316["Z"].iloc[-1]), label="210316_C01", color=color_210316_C01)
ax_gnss.plot(gps_pos_210316_c03["h_dist"]/gps_pos_210316_c03["Z"].iloc[-1], gps_v_210316_c03_orig/np.sqrt(9.81 * gps_pos_210316_c03["Z"].iloc[-1]), label="210316_C03", color=color_210316_C03)
ax_gnss.plot(gps_pos_220123["h_dist"]/gps_pos_220123["Z"].iloc[-1], gps_v_220123_orig/np.sqrt(9.81 * gps_pos_220123["Z"].iloc[-1]), label="220123", color=color_220123)
ax_gnss.plot(gps_pos_220203["h_dist"]/gps_pos_220203["Z"].iloc[-1], gps_v_220203_orig/np.sqrt(9.81 * gps_pos_220203["Z"].iloc[-1]), label="220203", color=color_220203)
ax_gnss.plot(gps_pos_220222_c07["h_dist"]/gps_pos_220222_c07["Z"].iloc[-1], gps_v_220222_c07_orig/np.sqrt(9.81 * gps_pos_220222_c07["Z"].iloc[-1]), label="220222_C07", color=color_220222_C07)
ax_gnss.plot(gps_pos_220222_c09["h_dist"]/gps_pos_220222_c09["Z"].iloc[-1], gps_v_220222_c09_orig/np.sqrt(9.81 * gps_pos_220222_c09["Z"].iloc[-1]), label="220222_C09", color=color_220222_C09)
ax_gnss.plot(gps_pos_220222_c10["h_dist"]/gps_pos_220222_c10["Z"].iloc[-1], gps_v_220222_c10_orig/np.sqrt(9.81 * gps_pos_220222_c10["Z"].iloc[-1]), label="220222_C10", color=color_220222_C10)

ax_gnss.grid()
ax_gnss.legend(bbox_to_anchor=(1, 1))
ax_gnss.set_ylabel('U/(g H_sc)^0.5')
ax_gnss.set_xlabel("x/H_sc (-)")
ax_gnss.set_ylim(0,1)
ax_gnss.set_xlim(0,2)

# =============================================================================
# ax_z = ax_gnss.twinx()
# ax_z.set_ylabel("z/H_sc")
# 
# ax_z.plot(gps_pos_210315["h_dist"]/gps_pos_210315["Z"].iloc[-1], (gps_pos_210315["Z"].iloc[-1] - gps_pos_210315["Z"])/gps_pos_210315["Z"].iloc[-1], label="210315", color=color_210315)
# ax_z.plot(gps_pos_210316["h_dist"]/gps_pos_210316["Z"].iloc[-1], (gps_pos_210316["Z"].iloc[-1] - gps_pos_210316["Z"])/gps_pos_210316["Z"].iloc[-1], label="210316_C01", color=color_210316_C01)
# ax_z.plot(gps_pos_210316_c03["h_dist"]/gps_pos_210316_c03["Z"].iloc[-1], (gps_pos_210316_c03["Z"].iloc[-1] - gps_pos_210316_c03["Z"])/gps_pos_210316_c03["Z"].iloc[-1], label="210316_C03", color=color_210316_C03)
# ax_z.plot(gps_pos_220123["h_dist"]/gps_pos_220123["Z"].iloc[-1], (gps_pos_220123["Z"].iloc[-1] - gps_pos_220123["Z"])/gps_pos_220123["Z"].iloc[-1], label="220123", color=color_220123)
# ax_z.plot(gps_pos_220203["h_dist"]/gps_pos_220203["Z"].iloc[-1], (gps_pos_220203["Z"].iloc[-1] - gps_pos_220203["Z"])/gps_pos_220203["Z"].iloc[-1], label="220203", color=color_220203)
# ax_z.plot(gps_pos_220222_c07["h_dist"]/gps_pos_220222_c07["Z"].iloc[-1], (gps_pos_220222_c07["Z"].iloc[-1] - gps_pos_220222_c07["Z"])/gps_pos_220222_c07["Z"].iloc[-1], label="220222_C07", color=color_220222_C07)
# ax_z.plot(gps_pos_220222_c09["h_dist"]/gps_pos_220222_c09["Z"].iloc[-1], (gps_pos_220222_c09["Z"].iloc[-1] - gps_pos_220222_c09["Z"])/gps_pos_220222_c09["Z"].iloc[-1], label="220222_C09", color=color_220222_C09)
# ax_z.plot(gps_pos_220222_c10["h_dist"]/gps_pos_220222_c10["Z"].iloc[-1], (gps_pos_220222_c10["Z"].iloc[-1] - gps_pos_220222_c10["Z"])/gps_pos_220222_c10["Z"].iloc[-1], label="220222_C10", color=color_220222_C10)
# #ax_z = 
# =============================================================================

# Analyze when v_max happens

pos_max = []
gps_pos_210315.reset_index(drop=True)
pos_max.append(gps_pos_210315["h_dist"].iloc[gps_v_210315.argmax()]/gps_pos_210315["Z"].iloc[-1])

gps_pos_210316.reset_index(drop=True)
pos_max.append(gps_pos_210316["h_dist"].iloc[gps_v_210316.argmax()]/gps_pos_210316["Z"].iloc[-1])

gps_pos_210316_c03.reset_index(drop=True)
pos_max.append(gps_pos_210316_c03["h_dist"].iloc[gps_v_210316_c03.argmax()]/gps_pos_210316_c03["Z"].iloc[-1])

gps_pos_220123.reset_index(drop=True)
pos_max.append(gps_pos_220123["h_dist"].iloc[gps_v_220123.argmax()]/gps_pos_220123["Z"].iloc[-1])

gps_pos_220203.reset_index(drop=True)
pos_max.append(gps_pos_220203["h_dist"].iloc[gps_v_220203.argmax()]/gps_pos_220203["Z"].iloc[-1])

gps_pos_220222_c07.reset_index(drop=True)
pos_max.append(gps_pos_220222_c07["h_dist"].iloc[gps_v_220222_c07.argmax()]/gps_pos_220222_c07["Z"].iloc[-1])

gps_pos_220222_c09.reset_index(drop=True)
pos_max.append(gps_pos_220222_c09["h_dist"].iloc[gps_v_220222_c09.argmax()]/gps_pos_220222_c09["Z"].iloc[-1])

gps_pos_220222_c10.reset_index(drop=True)
pos_max.append(gps_pos_220222_c10["h_dist"].iloc[gps_v_220222_c10.argmax()]/gps_pos_220222_c10["Z"].iloc[-1])

data = [np.array(pos_max)]

ax_bxplt.boxplot(data, vert=False, labels=["v_max"], autorange=True)
#ax_bxplt.minorticks_on()
#ax_bxplt.tick_params(axis='x', which='minor', bottom=False)
#ax_bxplt.set_xticks([0.1, 0.3, 0.5, 0.7, 0.9], minor=True)
ax_bxplt.set_xlim(ax_gnss.get_xlim())
ax_bxplt.grid(axis='x', which='both')
ax_bxplt.set_xlabel('x/H_sc (-)')

fig.tight_layout()
fig.savefig(save_path + "/all_avalanches_peter.png")

#%% Plot GNSS Accelerations

mean = []

max_range = 50
for i in range(1, max_range):
    idx_list = np.where((gps_time_210315[:-1] < i/max_range) & (gps_time_210315[:-1] > (i-1)/max_range))[0]
    array = gnss_acc_210315_smooth.iloc[idx_list].to_numpy()
    idx_list = np.where((gps_time_210316[:-1] < i/max_range) & (gps_time_210316[:-1] > (i-1)/max_range))[0]
    array = np.append(array, gnss_acc_210316_smooth.iloc[idx_list].to_numpy())
    idx_list = np.where((gps_time_210316_c03[:-1] < i/max_range) & (gps_time_210316_c03[:-1] > (i-1)/max_range))[0]
    array = np.append(array, gnss_acc_210316_c03_smooth.iloc[idx_list].to_numpy())
    idx_list = np.where((gps_time_220123[:-1] < i/max_range) & (gps_time_220123[:-1] > (i-1)/max_range))[0]
    array = np.append(array, gnss_acc_220123_smooth.iloc[idx_list].to_numpy())
    idx_list = np.where((gps_time_220203[:-1] < i/max_range) & (gps_time_220203[:-1] > (i-1)/max_range))[0]
    array = np.append(array, gnss_acc_220203_smooth.iloc[idx_list].to_numpy())
    idx_list = np.where((gps_time_220222_c07[:-1] < i/max_range) & (gps_time_220222_c07[:-1] > (i-1)/max_range))[0]
    array = np.append(array, gnss_acc_220222_c07_smooth.iloc[idx_list].to_numpy())
    idx_list = np.where((gps_time_220222_c09[:-1] < i/max_range) & (gps_time_220222_c09[:-1] > (i-1)/max_range))[0]
    array = np.append(array, gnss_acc_220222_c09_smooth.iloc[idx_list].to_numpy())
    idx_list = np.where((gps_time_220222_c10[:-1] < i/max_range) & (gps_time_220222_c10[:-1] > (i-1)/max_range))[0]
    array = np.append(array, gnss_acc_220222_c10_smooth.iloc[idx_list].to_numpy())
    
    new_array = array[~np.isnan(array)]
    if i == 18:
        print(array)
    mean.append(np.mean(new_array))

mean_df = pd.DataFrame(mean)
#mean_df = mean_df.rolling(int(max_range/10)).mean()

fig, (ax_gnss, ax_bxplt) = plt.subplots(2,1, figsize=(30, 15), gridspec_kw={'height_ratios': [3, 1]})

ax_gnss.plot(gps_time_210315[:-1], gnss_acc_210315_smooth.iloc[:, 0], label="210315", color=color_210315)
ax_gnss.plot(gps_time_210316[:-1], gnss_acc_210316_smooth.iloc[:, 0], label="210316-C01", color=color_210316_C01)
ax_gnss.plot(gps_time_210316_c03[:-1], gnss_acc_210316_c03_smooth.iloc[:, 0], label="210316-C03", color=color_210316_C03)
ax_gnss.plot(gps_time_220123[:-1], gnss_acc_220123_smooth.iloc[:, 0], label="220123", color=color_220123)
ax_gnss.plot(gps_time_220203[:-1], gnss_acc_220203_smooth.iloc[:, 0], label="220203", color=color_220203)
ax_gnss.plot(gps_time_220222_c07[:-1], gnss_acc_220222_c07_smooth.iloc[:, 0], label="220222_c07", color=color_220222_C07)
ax_gnss.plot(gps_time_220222_c09[:-1], gnss_acc_220222_c09_smooth.iloc[:, 0], label="220222_c09", color=color_220222_C09)
ax_gnss.plot(gps_time_220222_c10[:-1], gnss_acc_220222_c10_smooth.iloc[:, 0], label="220222_c10", color=color_220222_C10)

mean_time = np.linspace(0, 1, len(mean))

ax_gnss.plot(mean_time, mean_df.iloc[:,0], color='black', linewidth=4, label="mean")

ax_gnss.grid()
ax_gnss.legend(bbox_to_anchor=(1, 1))
ax_gnss.set_ylabel('acc_gnss [m/s²]')
#ax_gnss.set_xlabel("t/t_max")


idx_list = np.where(gnss_acc_210315_smooth.iloc[:, 0] > 0)[0]
acc_greater_zero = np.array(gps_time_210315.iloc[idx_list].to_numpy())
idx_list = np.where(gnss_acc_210316_smooth.iloc[:, 0] > 0)[0]
acc_greater_zero = np.hstack([acc_greater_zero, gps_time_210316.iloc[idx_list].to_numpy()])
idx_list = np.where(gnss_acc_210316_c03_smooth.iloc[:, 0] > 0)[0]
acc_greater_zero = np.hstack([acc_greater_zero, gps_time_210316_c03.iloc[idx_list].to_numpy()])
idx_list = np.where(gnss_acc_220123_smooth.iloc[:, 0] > 0)[0]
acc_greater_zero = np.hstack([acc_greater_zero, gps_time_220123.iloc[idx_list].to_numpy()])
idx_list = np.where(gnss_acc_220203_smooth.iloc[:, 0] > 0)[0]
acc_greater_zero = np.hstack([acc_greater_zero, gps_time_220203.iloc[idx_list].to_numpy()])
idx_list = np.where(gnss_acc_220222_c07_smooth.iloc[:, 0] > 0)[0]
acc_greater_zero = np.hstack([acc_greater_zero, gps_time_220222_c07.iloc[idx_list].to_numpy()])
idx_list = np.where(gnss_acc_220222_c09_smooth.iloc[:, 0] > 0)[0]
acc_greater_zero = np.hstack([acc_greater_zero, gps_time_220222_c09.iloc[idx_list].to_numpy()])
idx_list = np.where(gnss_acc_220222_c10_smooth.iloc[:, 0] > 0)[0]
acc_greater_zero = np.hstack([acc_greater_zero, gps_time_220222_c10.iloc[idx_list].to_numpy()])

idx_list = np.where(gnss_acc_210315_smooth.iloc[:, 0] < 0)[0]
acc_lower_zero = np.array(gps_time_210315.iloc[idx_list].to_numpy())
idx_list = np.where(gnss_acc_210316_smooth.iloc[:, 0] < 0)[0]
acc_lower_zero = np.hstack([acc_lower_zero, gps_time_210316.iloc[idx_list].to_numpy()])
idx_list = np.where(gnss_acc_210316_c03_smooth.iloc[:, 0] < 0)[0]
acc_lower_zero = np.hstack([acc_lower_zero, gps_time_210316_c03.iloc[idx_list].to_numpy()])
idx_list = np.where(gnss_acc_220123_smooth.iloc[:, 0] < 0)[0]
acc_lower_zero = np.hstack([acc_lower_zero, gps_time_220123.iloc[idx_list].to_numpy()])
idx_list = np.where(gnss_acc_220203_smooth.iloc[:, 0] < 0)[0]
acc_lower_zero = np.hstack([acc_lower_zero, gps_time_220203.iloc[idx_list].to_numpy()])
idx_list = np.where(gnss_acc_220222_c07_smooth.iloc[:, 0] < 0)[0]
acc_lower_zero = np.hstack([acc_lower_zero, gps_time_220222_c07.iloc[idx_list].to_numpy()])
idx_list = np.where(gnss_acc_220222_c09_smooth.iloc[:, 0] < 0)[0]
acc_lower_zero = np.hstack([acc_lower_zero, gps_time_220222_c09.iloc[idx_list].to_numpy()])
idx_list = np.where(gnss_acc_220222_c10_smooth.iloc[:, 0] < 0)[0]
acc_lower_zero = np.hstack([acc_lower_zero, gps_time_220222_c10.iloc[idx_list].to_numpy()])

boundery_up = 0.25
boundery_low = -0.25

# =============================================================================
# idx_list = np.where((gnss_acc_210315_smooth.iloc[:, 0] < boundery_up) & (gnss_acc_210315_smooth.iloc[:, 0] > boundery_low))[0]
# acc_in_boundary = np.array(gps_time_210315.iloc[idx_list].to_numpy())
# idx_list = np.where((gnss_acc_210316_smooth.iloc[:, 0] < boundery_up) & (gnss_acc_210316_smooth.iloc[:, 0] > boundery_low))[0]
# acc_in_boundary = np.hstack([acc_in_boundary, gps_time_210316.iloc[idx_list].to_numpy()])
# idx_list = np.where((gnss_acc_210316_c03_smooth.iloc[:, 0] < boundery_up) & (gnss_acc_210316_c03_smooth.iloc[:, 0] > boundery_low))[0]
# acc_in_boundary = np.hstack([acc_in_boundary, gps_time_210316_c03.iloc[idx_list].to_numpy()])
# idx_list = np.where((gnss_acc_220123_smooth.iloc[:, 0] < boundery_up) & (gnss_acc_220123_smooth.iloc[:, 0] > boundery_low))[0]
# acc_in_boundary = np.hstack([acc_in_boundary, gps_time_220123.iloc[idx_list].to_numpy()])
# idx_list = np.where((gnss_acc_220203_smooth.iloc[:, 0] < boundery_up) & (gnss_acc_220203_smooth.iloc[:, 0] > boundery_low))[0]
# acc_in_boundary = np.hstack([acc_in_boundary, gps_time_220203.iloc[idx_list].to_numpy()])
# idx_list = np.where((gnss_acc_220222_c07_smooth.iloc[:, 0] < boundery_up) & (gnss_acc_220222_c07_smooth.iloc[:, 0] > boundery_low))[0]
# acc_in_boundary = np.hstack([acc_in_boundary, gps_time_220222_c07.iloc[idx_list].to_numpy()])
# idx_list = np.where((gnss_acc_220222_c09_smooth.iloc[:, 0] < boundery_up) & (gnss_acc_220222_c09_smooth.iloc[:, 0] > boundery_low))[0]
# acc_in_boundary = np.hstack([acc_in_boundary, gps_time_220222_c09.iloc[idx_list].to_numpy()])
# idx_list = np.where((gnss_acc_220222_c10_smooth.iloc[:, 0] < boundery_up) & (gnss_acc_220222_c10_smooth.iloc[:, 0] > boundery_low))[0]
# acc_in_boundary = np.hstack([acc_in_boundary, gps_time_220222_c10.iloc[idx_list].to_numpy()])
# =============================================================================

idx_list = np.where((mean_df.iloc[:,0] < boundery_up) & (mean_df.iloc[:,0] > boundery_low))[0]
acc_in_boundary = np.array(mean_time[idx_list])

print("Mean Value is : {}".format(acc_in_boundary.mean()))
print("Lower Boundary is : {}".format(acc_in_boundary.min()))
print("Upper Boundary is : {}".format(acc_in_boundary.max()))
data = [acc_in_boundary]

ax_bxplt.boxplot(data, vert=False, labels=["< 0.25 & >-0.25"], autorange=True)
#ax_bxplt.minorticks_on()
#ax_bxplt.tick_params(axis='x', which='minor', bottom=False)
#ax_bxplt.set_xticks([0.1, 0.3, 0.5, 0.7, 0.9], minor=True)
ax_bxplt.set_xlim(ax_gnss.get_xlim())
ax_bxplt.grid(axis='x', which='both')
ax_bxplt.set_xlabel('t/t_max')

fig.tight_layout()
fig.savefig(save_path + "/all_avalanches_gnss_acc.png")

