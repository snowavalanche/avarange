# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 10:22:03 2022

@author: neuhauser
"""
import pandas as pd
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt

from classes.AvaNode_GNSS_Class import AvaNode_GNSS

import matplotlib
matplotlib.rcParams.update({'font.size': 22})

#C01 - AVALANCHE

# Experiment 23.01.2022

gps_c01 = AvaNode_GNSS()
path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-01-23_Nordkette_avalanche\C01\GPS\ava220123_C01.txt'
gps_c01.path = path_c01
gps_c01.read_data()
start = datetime(2022,1,23,7,30,20)
end = datetime(2022,1,23,7,31,30)
gps_c01.plot_vel(True,False,True, start, end)

max_pos = gps_c01.data_pos['v_total_pos'].max()
max_doppler = gps_c01.data_pos['v_total'].max()

imu_vel_path =r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-01-23_Nordkette_avalanche\C01\Leader\ava220123_C01_LeaderVelocityOptEF.txt' 
columns = ["Time", "Vel_X", "Vel_Y", "Vel_Z"]
imu_vel = pd.read_csv(imu_vel_path, skiprows=1, names=columns)
imu_vel["vel_tot"] = np.linalg.norm(imu_vel.loc[:, ["Vel_X","Vel_Y","Vel_Z"]], axis=1)

gps_time = gps_c01.data['#iTow[ms]'] / 1000 - gps_c01.data['#iTow[ms]'].iloc[0]/1000


imu_vel["Time"] += 49.4

fig, ax = plt.subplots(figsize=(15,7.5))

ax.plot(gps_time, gps_c01.data_pos["v_total"], color='b', label= "GNSS Doppler")
ax.plot(gps_time, gps_c01.data_pos["v_total_pos"], color="red", label='GNSS Pos.')
ax.plot(imu_vel["Time"], imu_vel["vel_tot"], color='black', label="IMU", linestyle="dashed", linewidth=2)

ax.set_xlim([45, 100])
ax.grid()
ax.legend()
ax.set_xlabel("Time in sec.")
ax.set_ylabel("Velocity in m/s")
ax.set_title("Avalanche 22.01.23 - GNSS and IMU velocities")
