# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 10:22:03 2022

@author: neuhauser
"""
from classes.AvaNode_Temp_Class import AvaNode_TEMP

#C04 - Temp Measurement

temp_c04 = AvaNode_TEMP()
path_c04 = r"../../data/2022-01-23_Nordkette_avalanche\C04\GPS\C04Gps002.txt"
temp_c04.path = path_c04
temp_c04.read_data()
temp_c04.plot_temp()


