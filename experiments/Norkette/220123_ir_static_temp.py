# -*- coding: utf-8 -*-
"""
Created on Mon Apr  4 13:05:22 2022

@author: neuhauser
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as dates

from classes.AvaNode_Temp_Class import AvaNode_TEMP

path = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-01-23_Nordkette_avalanche\C04\GPS\C04Gps002.txt"
gps_c04 = AvaNode_TEMP()
gps_c04.path = path
gps_c04.read_data()

gps_c04.get_snow_temp(1, 0.45)

fig, ax = plt.subplots(figsize=[12.8*1.5, 9.6*1.5])
# Define the date format
date = dates.date2num(gps_c04.data["timestamp"])
date_form = dates.DateFormatter("%H:%M")
ax.xaxis.set_major_formatter(date_form)
#ax.xaxis.set_major_locator(dates.MinuteLocator(byminute=(0, 30)))
#ax.xaxis.set_minor_locator(dates.MinuteLocator(byminute=(15, 45)))

ax.plot(date, gps_c04.data["ambientTemp[C]"], label="IR,Ambient", color="orange")
ax.plot(date, gps_c04.data["objectTemp[C]"], label="IR,Object", color="blue")
ax.plot(date, gps_c04.data["corr.snowTemp[C]"], label="Snow", color="red")
#ax.legend()
# Measured data from 23.01.2022

gps_c04.data['measuredTemp'] = np.nan

# =============================================================================
# idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:27:00')[0][0]
# idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:30:00')[0][0]
# gps_c04.data['measuredTemp'].iloc[idx_start:idx_end] = -5.4
# =============================================================================

idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:30:00')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:36:00')[0][0]
gps_c04.data['measuredTemp'].iloc[idx_start:idx_end] = -6.4

idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:36:00')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:39:00')[0][0]
gps_c04.data['measuredTemp'].iloc[idx_start:idx_end] = -5.6

idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:39:00')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:48:00')[0][0]
gps_c04.data['measuredTemp'].iloc[idx_start:idx_end] = -5.1

idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:48:00')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:04:00')[0][0]
gps_c04.data['measuredTemp'].iloc[idx_start:idx_end] = -5.0

idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:04:00')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:10:00')[0][0]
gps_c04.data['measuredTemp'].iloc[idx_start:idx_end] = -4.8

idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:10:00')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:24:00')[0][0]
gps_c04.data['measuredTemp'].iloc[idx_start:idx_end] = 0

ax.plot(date, gps_c04.data["measuredTemp"], label="measured hand", color="green")

#emissivity_snow = 0.95
#temp_corr = (((gps_c04.data['objectTemp[C]'] + 273.15)**4 - (gps_c04.data['ambientTemp[C]'] + 273.15)**4) / emissivity_snow + (gps_c04.data['ambientTemp[C]'] + 273.15)**4)**(1/4) - 273.15
#ax.plot(temp_corr, label='Corrected Temp')

ax.set_title("Temperature Measurement Seegrube 2022-01-23")
ax.set_xlabel("Time [UTC / H:M]")
ax.set_ylabel("Temperature [°C]")
ax.grid()
ax.legend()

fig.savefig("Temp_overview.png", dpi=600)

# Stationäre Messung Schneekerze
path_sk = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-05-30_static_temp_Seegrube\ds18b20_log.csv"
sk_data = pd.read_csv(path_sk, delimiter=(','))
sk_data = sk_data.replace(-9999, np.NaN)
sk_data["Date"] = pd.to_datetime(sk_data["Date"], format='%d/%m/%Y %H:%M:%S')
sk_data = sk_data.set_index("Date")
#data = sk_data.loc['2022-01-23 07:31:00': '2022-01-23 07:36:00']
# Noch keine stationäre Messung zu dieser Zeit!!!

#Handmessung
temp = [-6.4, -5.6, -5.1, -5.0, -4.8, -4.3, -3.6, 0]
height = [80, 70, 60, 50, 40, 30, 20, 0]


# =============================================================================
# fig, ax = plt.subplots()
# #ax.plot(temp, height, label='Handmessung', color='blue')
# 
# # Oberfläche
# idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:31:00')[0][0]
# idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:36:00')[0][0]
# temp_ir = gps_c04.data['objectTemp[C]'].iloc[idx_start:idx_end]
# height_ir = [80] * len(temp_ir)
# #ax.scatter(temp_ir, height_ir, color='red', label='IR orig.')
# 
# ax.boxplot(temp_ir, vert=False, positions=[80], showfliers=False)
# 
# # -10cm
# idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:36:30')[0][0]
# idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:39:00')[0][0]
# temp_ir = gps_c04.data['objectTemp[C]'].iloc[idx_start:idx_end]
# height_ir = [70] * len(temp_ir)
# #ax.scatter(temp_ir, height_ir, color='red')
# ax.boxplot(temp_ir, vert=False, positions=[70], showfliers=False)
# 
# # -20cm
# idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:39:30')[0][0]
# idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:48:00')[0][0]
# temp_ir = gps_c04.data['objectTemp[C]'].iloc[idx_start:idx_end]
# height_ir = [60] * len(temp_ir)
# #ax.scatter(temp_ir, height_ir, color='red')
# ax.boxplot(temp_ir, vert=False, positions=[60], showfliers=False)
# 
# #-30cm
# idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:48:00')[0][0]
# idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:04:00')[0][0]
# temp_ir = gps_c04.data['objectTemp[C]'].iloc[idx_start:idx_end]
# height_ir = [50] * len(temp_ir)
# #ax.scatter(temp_ir, height_ir, color='red')
# ax.boxplot(temp_ir, vert=False, positions=[50], showfliers=False)
# 
# #-40cm
# idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:04:00')[0][0]
# idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:10:00')[0][0]
# temp_ir = gps_c04.data['objectTemp[C]'].iloc[idx_start:idx_end]
# height_ir = [40] * len(temp_ir)
# #ax.scatter(temp_ir, height_ir, color='red')
# ax.boxplot(temp_ir, vert=False, positions=[40], showfliers=False)
# 
# ax.plot(temp, height, label='Handmessung', color='blue', marker='o')
# 
# 
# ax.set_xlabel("Temperatur [°C]")
# ax.set_ylabel("Snow Height [cm]")
# ax.grid()
# ax.legend()
# =============================================================================

gps_c04.get_snow_temp(1, 0.45)

fig, ax = plt.subplots()
#ax.plot(temp, height, label='Handmessung', color='blue')

# Oberfläche
idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:31:00')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:36:00')[0][0]

temp_ir = gps_c04.data['objectTemp[C]'].iloc[idx_start:idx_end]
#temp_corr = gps_c04.data['corr.objectTemp[C]'].iloc[idx_start:idx_end]
#snow_temp = gps_c04.data['corr.snowTemp[C]'].iloc[idx_start:idx_end]
snow_temp2 = gps_c04.data['corr.snowTemp[C]'].iloc[idx_start:idx_end]
#height_ir = [80] * len(temp_ir)
#ax.scatter(temp_ir, height_ir, color='red', label='IR orig.')

ax.boxplot(temp_ir, positions=[80], showfliers=False)
#ax.boxplot(temp_corr, positions=[80], showfliers=False)
#ax.boxplot(snow_temp, positions=[80], showfliers=False)
ax.boxplot(snow_temp2, positions=[80], showfliers=False)

# -10cm
idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:36:30')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:39:00')[0][0]
temp_ir = gps_c04.data['objectTemp[C]'].iloc[idx_start:idx_end]
#temp_corr = gps_c04.data['corr.objectTemp[C]'].iloc[idx_start:idx_end]
#snow_temp = gps_c04.data['corr.snowTemp[C]'].iloc[idx_start:idx_end]
snow_temp2 = gps_c04.data['corr.snowTemp[C]'].iloc[idx_start:idx_end]

#height_ir = [70] * len(temp_ir)
#ax.scatter(temp_ir, height_ir, color='red')
ax.boxplot(temp_ir, positions=[70], showfliers=False)
#ax.boxplot(temp_corr, positions=[70], showfliers=False)
#ax.boxplot(snow_temp, positions=[70], showfliers=False)
ax.boxplot(snow_temp2, positions=[70], showfliers=False)


# -20cm
idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:39:30')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:48:00')[0][0]
temp_ir = gps_c04.data['objectTemp[C]'].iloc[idx_start:idx_end]
#temp_corr = gps_c04.data['corr.objectTemp[C]'].iloc[idx_start:idx_end]
#snow_temp = gps_c04.data['corr.snowTemp[C]'].iloc[idx_start:idx_end]
snow_temp2 = gps_c04.data['corr.snowTemp[C]'].iloc[idx_start:idx_end]
#height_ir = [60] * len(temp_ir)

#ax.scatter(temp_ir, height_ir, color='red')
ax.boxplot(temp_ir, positions=[60], showfliers=False)
#ax.boxplot(temp_corr, positions=[60], showfliers=False)
#ax.boxplot(snow_temp, positions=[60], showfliers=False)
ax.boxplot(snow_temp2, positions=[60], showfliers=False)

#-30cm
idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:48:00')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:04:00')[0][0]
temp_ir = gps_c04.data['objectTemp[C]'].iloc[idx_start:idx_end]
#temp_corr = gps_c04.data['corr.objectTemp[C]'].iloc[idx_start:idx_end]
#snow_temp = gps_c04.data['corr.snowTemp[C]'].iloc[idx_start:idx_end]
snow_temp2 = gps_c04.data['corr.snowTemp[C]'].iloc[idx_start:idx_end]
#height_ir = [50] * len(temp_ir)

#ax.scatter(temp_ir, height_ir, color='red')
ax.boxplot(temp_ir, positions=[50], showfliers=False)
#ax.boxplot(temp_corr, positions=[50], showfliers=False)
#ax.boxplot(snow_temp, positions=[50], showfliers=False)
ax.boxplot(snow_temp2, positions=[50], showfliers=False)

#-40cm
idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:04:00')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:10:00')[0][0]
temp_ir = gps_c04.data['objectTemp[C]'].iloc[idx_start:idx_end]
#temp_corr = gps_c04.data['corr.objectTemp[C]'].iloc[idx_start:idx_end]
#snow_temp = gps_c04.data['corr.snowTemp[C]'].iloc[idx_start:idx_end]
snow_temp2 = gps_c04.data['corr.snowTemp[C]'].iloc[idx_start:idx_end]
#height_ir = [40] * len(temp_ir)

#ax.scatter(temp_ir, height_ir, color='red')
ax.boxplot(temp_ir, positions=[40], showfliers=False)
#ax.boxplot(temp_corr, positions=[40], showfliers=False)
#ax.boxplot(snow_temp, positions=[40], showfliers=False)
ax.boxplot(snow_temp2, positions=[40], showfliers=False)

# 0cm
idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:10:00')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:24:00')[0][0]
temp_ir = gps_c04.data['objectTemp[C]'].iloc[idx_start:idx_end]
#temp_corr = gps_c04.data['corr.objectTemp[C]'].iloc[idx_start:idx_end]
#snow_temp = gps_c04.data['corr.snowTemp[C]'].iloc[idx_start:idx_end]
snow_temp2 = gps_c04.data['corr.snowTemp[C]'].iloc[idx_start:idx_end]
# = [0] * len(temp_ir)

#ax.scatter(temp_ir, height_ir, color='red')
ax.boxplot(temp_ir, positions=[0], showfliers=False)
#ax.boxplot(temp_corr, positions=[0], showfliers=False)
#ax.boxplot(snow_temp, positions=[0], showfliers=False)
ax.boxplot(snow_temp2, positions=[0], showfliers=False)

ax.plot(height, temp, label='Handmessung', color='blue', marker='o')


ax.set_xlabel("Snow Height [cm]")
ax.set_ylabel("Temperatur [°C]")
ax.grid()
ax.legend()


# Parameterstudie
# =============================================================================
# 
# e_snow = np.arange(0.9, 1.01, 0.01)
# e_folie = np.arange(0.4, 0.7, 0.05)
# 
# err1 = []
# err2 = []
# err3 = []
# err4 = []
# idx = 0
# 
# for e_s in e_snow:
#     for e_f in e_folie:
#         gps_c04.get_snow_temp(e_s, e_f)
#         
#         # -10cm
#         idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:36:30')[0][0]
#         idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:39:00')[0][0]
#         snow_temp2 = gps_c04.data['corr.snowTemp2[C]'].iloc[idx_start:idx_end]
#         error1 = -5.6 - np.mean(snow_temp2)
#         err1.append(error1)
#         if abs(error1) < 1:
#             print("E_snow = {} and E_Folie = {}".format(e_s, e_f))
#             
#         # -20cm
#         idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:39:30')[0][0]
#         idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:48:00')[0][0]
#         snow_temp2 = gps_c04.data['corr.snowTemp2[C]'].iloc[idx_start:idx_end]
#         error2 = -5.1 - np.mean(snow_temp2)
#         err2.append(error2)
#         if abs(error2) < 1:
#             print("E_snow = {} and E_Folie = {}".format(e_s, e_f))
#             
#         #-30cm
#         idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:48:00')[0][0]
#         idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:04:00')[0][0]
#         snow_temp2 = gps_c04.data['corr.snowTemp2[C]'].iloc[idx_start:idx_end]
#         error3 = -5.0 - np.mean(snow_temp2)
#         err3.append(error3)
#         if abs(error3) < 1:
#             print("E_snow = {} and E_Folie = {}".format(e_s, e_f))
#             
#         #-40cm
#         idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:04:00')[0][0]
#         idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:10:00')[0][0]
#         snow_temp2 = gps_c04.data['corr.snowTemp2[C]'].iloc[idx_start:idx_end]
#         error4 = -4.8 - np.mean(snow_temp2)
#         err4.append(error4)
#         if abs(error4) < 1:
#             print("E_snow = {} and E_Folie = {}".format(e_s, e_f))
#             
#         if idx == 61:
#             print(e_s, e_f)
#         idx += 1
# 
# fig, ax = plt.subplots()
#             
# ax.plot(err1)
# ax.plot(err2)
# ax.plot(err3)
# ax.plot(err4)
# 
# err = np.array(err1) + np.array(err2) + np.array(err3) + np.array(err4)
# =============================================================================

#Ergebnis E_snow = 1 und e_Folie = 0.45

  