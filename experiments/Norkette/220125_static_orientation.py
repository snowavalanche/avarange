# -*- coding: utf-8 -*-
"""
Created on Tue Sep  6 14:14:58 2022

@author: neuhauser
"""
import matplotlib.pyplot as plt
import numpy as np

from classes.AvaNode_IMU_Class import AvaNode_IMU

def RotXYZ2RotationMatrix(rot):
    c0 = np.cos(rot[0])
    s0 = np.sin(rot[0])
    c1 = np.cos(rot[1])
    s1 = np.sin(rot[1])
    c2 = np.cos(rot[2])
    s2 = np.sin(rot[2])
    
    return np.array([[c1*c2,-c1 * s2,s1],
                  [s0*s1*c2 + c0 * s2, -s0 * s1*s2 + c0 * c2,-s0 * c1],
                  [-c0 * s1*c2 + s0 * s2,c0*s1*s2 + s0 * c2,c0*c1 ]])

imu04 = AvaNode_IMU()
path_04 = r"../../data/2022-01-25_Nordkette_GPS_snow/AvaNodes/C04/Leader/220125_C04_steady_Leader.txt"

imu04.path = path_04
imu04.read_data_pd()


imu06 = AvaNode_IMU()
path_06 = r"../../data/2022-01-25_Nordkette_GPS_snow/AvaNodes/C06/Leader/C06LData002.txt"

imu06.path = path_06
imu06.read_data_pd()


imu07 = AvaNode_IMU()
path_07 = r"../../data/2022-01-25_Nordkette_GPS_snow/AvaNodes/C07/Leader/C07LData001.txt"

imu07.path = path_07
imu07.read_data_pd()


# =============================================================================
# imu05 = AvaNode_IMU()
# path_05 = r"../../data/2022-01-25_Nordkette_GPS_snow/AvaNodes/C05/Leader/C05LData001.txt"
# 
# imu05.path = path_05
# imu05.read_data_pd()
# =============================================================================

A = [[0.94140003, -0.00131012, -0.21666981],[0.02837185, 1.22886708, -0.01556649],[-0.09622693, 0.02975633, 1.20456782]]
b = [-13.82616946, -17.91337605, -68.31359358]

mag04 = imu04.data[['mag_x','mag_y', 'mag_z']]
#for i in range(len(mag04)):
    #mag04_cal = A @ (mag04 + b)
mag04 = mag04.iloc[:1000, :]

for i in range(len(mag04)):
    mag04.iloc[i,:] = RotXYZ2RotationMatrix([180*np.pi/180,0,-90*np.pi/180]) @ mag04.iloc[i,:]

mag04Cal=(A @ (mag04 + b).T).T

for i in range(len(mag04)):
    mag04Cal.iloc[i,:] = RotXYZ2RotationMatrix([0, 0, np.pi]) @ mag04Cal.iloc[i,:]

imu04.mag_X = mag04Cal.iloc[:, 0]
imu04.mag_Y = mag04Cal.iloc[:, 1]
imu04.mag_Z = mag04Cal.iloc[:, 2]

# Plots

fig, ax = plt.subplots()
ax.plot(imu04.mag_X, color='r')
ax.plot(imu04.mag_Y, color='g')
ax.plot(imu04.mag_Z, color='b')

fig, ax = plt.subplots()
ax.plot(imu06.mag_X, color='r')
ax.plot(imu06.mag_Y, color='g')
ax.plot(imu06.mag_Z, color='b')

fig, ax = plt.subplots()
ax.plot(imu07.mag_X, color='r')
ax.plot(imu07.mag_Y, color='g')
ax.plot(imu07.mag_Z, color='b')
# =============================================================================
# 
# fig, ax = plt.subplots()
# ax.plot(imu05.mag_X, color='r')
# ax.plot(imu05.mag_Y, color='g')
# ax.plot(imu05.mag_Z, color='b')
# =============================================================================
