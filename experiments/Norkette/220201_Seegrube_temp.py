# -*- coding: utf-8 -*-
"""
Created on Tue May 31 10:15:32 2022

@author: neuhauser
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from classes.AvaNode_Temp_Class import AvaNode_TEMP

path = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-01_Nordkette_temp\C15\GPS\C15Gps001.txt"
gps_c04 = AvaNode_TEMP()
gps_c04.path = path
gps_c04.read_data()

fig, ax = plt.subplots()
ax.plot(gps_c04.data["ambientTemp[C]"], label="IR,Ambient", color="orange")
ax.plot(gps_c04.data["objectTemp[C]"], label="IR,Object", color="blue")

# Measured data from 23.01.2022

# =============================================================================
# gps_c04.data['measuredTemp'] = np.nan
# 
# idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:27:00')[0][0]
# idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:30:00')[0][0]
# gps_c04.data['measuredTemp'].iloc[idx_start:idx_end] = -5.4
# 
# idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:30:00')[0][0]
# idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:36:00')[0][0]
# gps_c04.data['measuredTemp'].iloc[idx_start:idx_end] = -6.4
# 
# idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:36:00')[0][0]
# idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:39:00')[0][0]
# gps_c04.data['measuredTemp'].iloc[idx_start:idx_end] = -5.6
# 
# idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:39:00')[0][0]
# idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:48:00')[0][0]
# gps_c04.data['measuredTemp'].iloc[idx_start:idx_end] = -5.1
# 
# idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:48:00')[0][0]
# idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:04:00')[0][0]
# gps_c04.data['measuredTemp'].iloc[idx_start:idx_end] = -5.0
# 
# idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:04:00')[0][0]
# idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:10:00')[0][0]
# gps_c04.data['measuredTemp'].iloc[idx_start:idx_end] = -4.8
# 
# idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:10:00')[0][0]
# idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:24:00')[0][0]
# gps_c04.data['measuredTemp'].iloc[idx_start:idx_end] = 0
# 
# ax.plot(gps_c04.data["measuredTemp"], label="measured hand", color="red")
# 
# emissivity_snow = 0.95
# temp_corr = (((gps_c04.data['objectTemp[C]'] + 273.15)**4 - (gps_c04.data['ambientTemp[C]'] + 273.15)**4) / emissivity_snow + (gps_c04.data['ambientTemp[C]'] + 273.15)**4)**(1/4) - 273.15
# ax.plot(temp_corr, label='Corrected Temp')
# 
# ax.legend()
# =============================================================================

# Stationäre Messung Schneekerze
path_sk = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-05-30_static_temp_Seegrube\ds18b20_log.csv"
sk_data = pd.read_csv(path_sk, delimiter=(','))
sk_data = sk_data.replace(-9999, np.NaN)
sk_data["Date"] = pd.to_datetime(sk_data["Date"], format='%d/%m/%Y %H:%M:%S')
sk_data = sk_data.set_index("Date")
#data = sk_data.loc['2022-01-23 07:31:00': '2022-01-23 07:36:00']
# Noch keine stationäre Messung zu dieser Zeit!!!

#Handmessung
temp = [-6.4, -5.6, -5.1, -5.0, -4.8, -4.3, -3.6, 0]
height = [80, 70, 60, 50, 40, 30, 20, 0]


fig, ax = plt.subplots()
#ax.plot(temp, height, label='Handmessung', color='blue')

#ToDo: make strings to datetime object

# Oberfläche
idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:31:00')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:36:00')[0][0]
temp_ir = gps_c04.data['objectTemp[C]'].iloc[idx_start:idx_end]
height_ir = [80] * len(temp_ir)
#ax.scatter(temp_ir, height_ir, color='red', label='IR orig.')

ax.boxplot(temp_ir, vert=False, positions=[80], showfliers=False)

# -10cm
idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:36:30')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:39:00')[0][0]
temp_ir = gps_c04.data['objectTemp[C]'].iloc[idx_start:idx_end]
height_ir = [70] * len(temp_ir)
#ax.scatter(temp_ir, height_ir, color='red')
ax.boxplot(temp_ir, vert=False, positions=[70], showfliers=False)

# -20cm
idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:39:30')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:48:00')[0][0]
temp_ir = gps_c04.data['objectTemp[C]'].iloc[idx_start:idx_end]
height_ir = [60] * len(temp_ir)
#ax.scatter(temp_ir, height_ir, color='red')
ax.boxplot(temp_ir, vert=False, positions=[60], showfliers=False)

#-30cm
idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 07:48:00')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:04:00')[0][0]
temp_ir = gps_c04.data['objectTemp[C]'].iloc[idx_start:idx_end]
height_ir = [50] * len(temp_ir)
#ax.scatter(temp_ir, height_ir, color='red')
ax.boxplot(temp_ir, vert=False, positions=[50], showfliers=False)

#-40cm
idx_start = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:04:00')[0][0]
idx_end = np.where(gps_c04.data['timestamp'] == '2022-01-23 08:10:00')[0][0]
temp_ir = gps_c04.data['objectTemp[C]'].iloc[idx_start:idx_end]
height_ir = [40] * len(temp_ir)
#ax.scatter(temp_ir, height_ir, color='red')
ax.boxplot(temp_ir, vert=False, positions=[40], showfliers=False)

ax.plot(temp, height, label='Handmessung', color='blue', marker='o')


ax.set_xlabel("Temperatur [°C]")
ax.set_ylabel("Snow Height [cm]")
ax.grid()
ax.legend()