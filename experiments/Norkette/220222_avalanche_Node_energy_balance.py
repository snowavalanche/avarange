# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 11:56:08 2022

@author: neuhauser
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from datetime import datetime

from classes.GPS_Class import GPSData
from classes.IMU_Class import ImuData

# Reading Data

#C07
gps_c07 = GPSData()
path_c07 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C07\GPS\220222_C07_avalanche_GPS.txt"
gps_c07.path = path_c07
gps_c07.read_data()


c_07_mass = 1.401 #kg

#Time Filter
start_idx = np.where(gps_c07.data_pos["timestamp"] == "2022-02-22 07:52:22")[0][0]
end_idx = np.where(gps_c07.data_pos["timestamp"] == "2022-02-22 07:53:10")[0][0]
gps_c07.data_pos = gps_c07.data_pos.iloc[start_idx:end_idx, :]
gps_c07.data = gps_c07.data.iloc[start_idx:end_idx, :]

gps_c07_time = gps_c07.data['iTow[ms]'] / 1000 - gps_c07.data['iTow[ms]'].iloc[0]/1000

#C09
gps_c09 = GPSData()
path_c09 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C09\GPS\220222_C09_avalanche_GPS.txt"
gps_c09.path = path_c09
gps_c09.read_data()
c_09_mass = 0.811 #kg

#Time Filter
start_idx = np.where(gps_c09.data_pos["timestamp"] == "2022-02-22 07:52:22")[0][0]
end_idx = np.where(gps_c09.data_pos["timestamp"] == "2022-02-22 07:53:10")[0][0]
gps_c09.data_pos = gps_c09.data_pos.iloc[start_idx:end_idx, :]
gps_c09.data = gps_c09.data.iloc[start_idx:end_idx, :]

gps_c09_time = gps_c09.data['iTow[ms]'] / 1000 - gps_c09.data['iTow[ms]'].iloc[0]/1000

#C10
gps_c10 = GPSData()
path_c10 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C10\GPS\220222_C10_avalanche_GPS.txt"
gps_c10.path = path_c10
gps_c10.read_data()
c_10_mass = 0.811 #kg

#Time Filter
start_idx = np.where(gps_c10.data_pos["timestamp"] == "2022-02-22 07:52:22")[0][0]
end_idx = np.where(gps_c10.data_pos["timestamp"] == "2022-02-22 07:53:10")[0][0]
gps_c10.data_pos = gps_c10.data_pos.iloc[start_idx:end_idx, :]
gps_c10.data = gps_c10.data.iloc[start_idx:end_idx, :]

# Calculations
gps_c10_time = gps_c10.data['iTow[ms]'] / 1000 - gps_c10.data['iTow[ms]'].iloc[0]/1000
date_c07 = dates.date2num(gps_c07.data['timestamp'])
c07_v_tot = np.linalg.norm([gps_c07.data["velD[mm/s]"].values, gps_c07.data["velE[mm/s]"].values, gps_c07.data["velN[mm/s]"].values], axis=0)/1000

c07_pot_e = np.zeros_like(gps_c07.data["height[m]"])
c07_kin_e = np.zeros_like(gps_c07.data["height[m]"])
c07_max_kin_e = np.zeros_like(c07_kin_e)

for i in range(len(gps_c07.data["height[m]"])):
    c07_pot_e[i] = c_07_mass * 9.81 * (gps_c07.data["height[m]"].iloc[i] - gps_c07.data["height[m]"].iloc[-1])
    c07_kin_e[i] = .5 * c_07_mass * c07_v_tot[i]**2
    
c07_kin_e_plot =  c07_kin_e +  c07_pot_e    
for i in range(len(gps_c07.data["height[m]"])-1):  
    #c07_max_kin_e[i+1] = c07_max_kin_e[i] + c_07_mass * 9.81 * (gps_c07.data["height[m]"].iloc[i] - gps_c07.data["height[m]"].iloc[i+1])
    c07_max_kin_e[i+1] = c_07_mass * 9.81 * (gps_c07.data["height[m]"].iloc[0] - gps_c07.data["height[m]"].iloc[i+1])
    #c07_kin_e[i+1] = c07_kin_e[i] + .5 * c_07_mass * c07_v_tot[i]**2
#c07_kin_e = .5 * c07_v_tot**2 * c_07_mass
c07_dis_e = c07_pot_e[0] - c07_pot_e -  c07_kin_e
c07_e_balance = c07_pot_e +  c07_kin_e + c07_dis_e

date_c09 = dates.date2num(gps_c09.data['timestamp'])
c09_v_tot = np.linalg.norm([gps_c09.data["velD[mm/s]"].values, gps_c09.data["velE[mm/s]"].values, gps_c09.data["velN[mm/s]"].values], axis=0)/1000

date_c10 = dates.date2num(gps_c10.data['timestamp'])
c10_v_tot = np.linalg.norm([gps_c10.data["velD[mm/s]"].values, gps_c10.data["velE[mm/s]"].values, gps_c10.data["velN[mm/s]"].values], axis=0)/1000


# Plot
fig, (ax_gnss) = plt.subplots()
# Define the date format
date_form = dates.DateFormatter("%H:%M:%S")
#ax_gnss.xaxis.set_major_formatter(date_form)
#fig.autofmt_xdate(rotation=45)

ax_gnss.plot(gps_c07_time, c07_pot_e, color='g', label='C07 Pot. Energy')
ax_gnss.plot(gps_c07_time, c07_kin_e_plot, color='r', label='C07 Kin. Energy')
#ax_gnss.plot(date_c07, c07_max_kin_e, color='b', label='C07 Max. Kin. Energy')
ax_gnss.plot(gps_c07_time, c07_dis_e, color='magenta', label='C07 Dis. Energy')
ax_gnss.plot(gps_c07_time, c07_e_balance, color='black', label='C07 Energy Balance')


#ax_gnss.plot(date_c09, c09_v_tot, color='r', label='C09')
#ax_gnss.plot(date_c10, c10_v_tot, color='orange', label='C10')

# Plot Add-ons
#ax_gnss.fill_between(date_particles, particles[:46, 1], particles[:46, 2], color='lightgray', label='Simulation')

ax_gnss.set_xlabel('Time [UTC]')
ax_gnss.set_ylabel('Energy [kg * m²/s²]')
ax_gnss.set_title('Avalanche 22.02.2022 - Energy Balance ...')

#ax_gnss.set_xlim([datetime(2022,2,22,7,52,20), datetime(2022,2,22,7,53,10)])

ax_gnss.grid()
ax_gnss.legend()

#save_path = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange\docs\Conferences\ALERT2022\pics\220222_Sim_Nodes_mu02.png'
#fig.savefig(save_path, dpi=300)

data = np.array([c07_pot_e, c07_kin_e_plot, c07_dis_e])
data = data.T
np.savetxt("c07.csv", data, delimiter=",", header=["Epot", "Ekin", "Edis"])
