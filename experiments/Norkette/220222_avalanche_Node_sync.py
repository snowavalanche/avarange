# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 11:56:08 2022

@author: neuhauser
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from datetime import datetime

from classes.GPS_Class import GPSData
from classes.IMU_Class import ImuData


#C07
gps_c07 = GPSData()
path_c07 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C07\GPS\220222_C07_avalanche_GPS.txt"
gps_c07.path = path_c07
gps_c07.read_data()
gps_c07_time = gps_c07.data['iTow[ms]'] / 1000 - gps_c07.data['iTow[ms]'].iloc[0]/1000
gps_07_time = gps_c07.data['iTow[ms]'] / 1000 - gps_c07.data['iTow[ms]'].iloc[0]/1000


imu_c07 = ImuData()
imu_path_c07 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C07\Leader\220222_C07_avalanche_Leader.txt'
imu_c07.path = imu_path_c07
imu_c07.read_data_pd()

# Settings
#imu_07_start_idx = 40*400
#imu_07_end_idx = 85*400
#imu_07_time = imu_c07.time[imu_07_start_idx:imu_07_end_idx]

#Time Filter
start_idx = np.where(gps_c07.data_pos["timestamp"] == "2022-02-22 07:52:22")[0][0]
end_idx = np.where(gps_c07.data_pos["timestamp"] == "2022-02-22 07:53:10")[0][0]

#gps_c07.data_pos = gps_c07.data_pos.iloc[start_idx:end_idx, :]
#gps_c07.data = gps_c07.data.iloc[start_idx:end_idx, :]

#gps_c07.plot_vel()
#gps_c07.check_velocity()
#gps_c07.export_in_radar_cs()

#transform = get_coord_transform(4326, 31254) #transform in cartesian coordinates to add the path
#lat = gps_c07.data['latitude[deg]'].values
#long = gps_c07.data['longitude[deg]'].values
#north, east, z = transform.TransformPoint(lat[0], long[0], gps_c07.data['height[m]'].values[0])
#gps_c07.save_to_vtk("07")

#C09
gps_c09 = GPSData()
path_c09 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C09\GPS\220222_C09_avalanche_GPS.txt"
gps_c09.path = path_c09
gps_c09.read_data()
#gps_c09.plot_vel()
#gps_c09.export_in_radar_cs()

#Time Filter
start_idx = np.where(gps_c09.data_pos["timestamp"] == "2022-02-22 07:52:22")[0][0]
end_idx = np.where(gps_c09.data_pos["timestamp"] == "2022-02-22 07:53:10")[0][0]
gps_c09.data_pos = gps_c09.data_pos.iloc[start_idx:end_idx, :]
gps_c09.data = gps_c09.data.iloc[start_idx:end_idx, :]
#gps_c09.save_to_vtk("09")

#C10
gps_c10 = GPSData()
path_c10 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C10\GPS\220222_C10_avalanche_GPS.txt"
gps_c10.path = path_c10
gps_c10.read_data()
#gps_c10.plot_vel()
#gps_c10.export_in_radar_cs()

# Get max values
start = datetime(2022,2,22,7,52,22)
end = datetime(2022,2,22,7,53,10)

# =============================================================================
# max_pos = gps_c10.data_pos['v_total_pos'].loc[(gps_c10.data_pos['timestamp'] > start) & (gps_c10.data_pos['timestamp'] < end)].max()
# max_doppler = gps_c10.data_pos['v_total'].loc[(gps_c10.data_pos['timestamp'] > start) & (gps_c10.data_pos['timestamp'] < end)].max()
# mean_pDOP = gps_c10.data_pos['pDop[]'].loc[(gps_c10.data_pos['timestamp'] > start) & (gps_c10.data_pos['timestamp'] < end)].mean()
# diff = abs(gps_c10.data_pos['v_total_pos'] - gps_c10.data_pos['v_total'])
# mean_diff = diff.mean()
# =============================================================================

#Time Filter
start_idx = np.where(gps_c10.data_pos["timestamp"] == "2022-02-22 07:52:22")[0][0]
end_idx = np.where(gps_c10.data_pos["timestamp"] == "2022-02-22 07:53:10")[0][0]

gps_c10.data_pos = gps_c10.data_pos.iloc[start_idx:end_idx, :]
gps_c10.data = gps_c10.data.iloc[start_idx:end_idx, :]
#gps_c10.save_to_vtk("10")

date_c07 = dates.date2num(gps_c07.data['timestamp'])
c07_v_tot = np.linalg.norm([gps_c07.data["velD[mm/s]"].values, gps_c07.data["velE[mm/s]"].values, gps_c07.data["velN[mm/s]"].values], axis=0)/1000



date_c09 = dates.date2num(gps_c09.data['timestamp'])
c09_v_tot = np.linalg.norm([gps_c09.data["velD[mm/s]"].values, gps_c09.data["velE[mm/s]"].values, gps_c09.data["velN[mm/s]"].values], axis=0)/1000

date_c10 = dates.date2num(gps_c10.data['timestamp'])
c10_v_tot = np.linalg.norm([gps_c10.data["velD[mm/s]"].values, gps_c10.data["velE[mm/s]"].values, gps_c10.data["velN[mm/s]"].values], axis=0)/1000

# Read Particle Data from Sim



#path_to_sim = r'C:\git_rep\AvaFrame\avaframe\data\seilbahnrinne\Outputs\com1DFA\particlesCSV'
#files = os.listdir(path_to_sim)
#files.sort(key=lambda f: int(''. join(filter(str. isdigit, f))))


#particles = np.zeros((len(files), 3)) # Time, min_v, max_v
#time = 0

# =============================================================================
# for file in files:
#     data = pd.read_csv(path_to_sim + '/' + file)
#     particles[time, 0] = time
#     particles[time, 1] = data['velocityMagnitude'].min()
#     particles[time, 2] = data['velocityMagnitude'].max()
#     time += 1
#     
# date_particles = date_c07[20::10]
# =============================================================================

# Plot
fig, (ax_gnss) = plt.subplots()
# Define the date format
date_form = dates.DateFormatter("%H:%M:%S")
ax_gnss.xaxis.set_major_formatter(date_form)
fig.autofmt_xdate(rotation=45)

ax_gnss.plot(date_c07, c07_v_tot, color='#00f1ee', label='C07')
ax_gnss.plot(date_c09, c09_v_tot, color='#ff04ff', label='C09')
ax_gnss.plot(date_c10, c10_v_tot, color='#ffff00', label='C10')

# Plot Add-ons
#ax_gnss.fill_between(date_particles, particles[:46, 1], particles[:46, 2], color='lightgray', label='Simulation')

ax_gnss.set_xlabel('Time [UTC]')
ax_gnss.set_ylabel('Velocity [m/s]')
#ax_gnss.set_title('Avalanche 22.02.2022 - Velocities: AvaNodes - Simulation')

ax_gnss.set_xlim([datetime(2022,2,22,7,52,20), datetime(2022,2,22,7,53,10)])

ax_gnss.grid()
ax_gnss.legend()

save_path = r'C:\git_rep\dissertation\papers\GPS_paper\figures\exp\220222_avalanche_GPS_velocities.png'
fig.savefig(save_path, dpi=300)



# =============================================================================
# # Plot
# fig, (ax_gnss, ax_acc, ax_gyro) = plt.subplots(3,1, figsize=(20, 30))
# # Define the date format
# date_form = dates.DateFormatter("%H:%M:%S")
# ax_gnss.xaxis.set_major_formatter(date_form)
# fig.autofmt_xdate(rotation=45)
# 
# 
# 
# ax_gnss.plot(gps_07_time, c07_v_tot, color='g', label='C07')
# #ax_gnss.plot(date_c09, c09_v_tot, color='r', label='C09')
# #ax_gnss.plot(date_c10, c10_v_tot, color='orange', label='C10')
# 
# # Plot Add-ons
# #ax_gnss.fill_between(date_particles, particles[:46, 1], particles[:46, 2], color='lightgray', label='Simulation')
# 
# ax_gnss.set_xlabel('Time [UTC]')
# ax_gnss.set_ylabel('Velocity [m/s]')
# #ax_gnss.set_title('Avalanche 22.02.2022 - Velocities: AvaNodes - Simulation')
# 
# #ax_gnss.set_xlim([datetime(2022,2,22,7,52,20), datetime(2022,2,22,7,53,10)])
# 
# ax_gnss.grid()
# ax_gnss.legend()
# 
# # Plot Acc
# ax_acc.plot(imu_c07.time, imu_c07.acc_tot - 9.81, color='g', label='C07')
# 
# # Plot Gyro
# ax_gyro.plot(imu_c07.time, imu_c07.gyro_tot, color='g', label='C07')
# 
# 
# 
# ax_acc.set_ylim([-50, 220])
# ax_gyro.set_ylim([-50,2550])
# 
# 
# # X lim
# ax_gnss.set_xlim([40, 85])
# ax_acc.set_xlim([40, 85])
# ax_gyro.set_xlim([40, 85])
# =============================================================================
