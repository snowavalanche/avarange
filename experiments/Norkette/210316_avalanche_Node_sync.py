# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 11:56:08 2022

@author: neuhauser
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from datetime import datetime

from classes.AvaNode_GNSS_Class import AvaNode_GNSS

#C01
gps_c01 = AvaNode_GNSS()
path_c01 = r"../../data/2021-03-16_Nordkette_avalanche/C01/GPS/ava210316_C01_GPS.txt"
gps_c01.path = path_c01
gps_c01.read_data()
gps_c01.plot_vel(False, False, True)
maxi = np.amax(gps_c01.data_pos['v_total_pos'])

#C03
gps_c03 = AvaNode_GNSS()
path_c03 = r"../../data/2021-03-16_Nordkette_avalanche/C03/GPS/ava210316_C03_GPS.txt"
gps_c03.path = path_c03
gps_c03.read_data()
gps_c03.plot_vel(False, False, True)

c01_v_tot = gps_c01.data_pos['v_total_pos']
date_c01 = dates.date2num(gps_c01.data['timestamp'])

c03_v_tot = gps_c03.data_pos['v_total_pos']
date_c03 = dates.date2num(gps_c03.data['timestamp'])

linewidth = 0.5

fig, ax = plt.subplots()
# Define the date format
date_form = dates.DateFormatter("%H:%M:%S")
ax.xaxis.set_major_formatter(date_form)
fig.autofmt_xdate(rotation=45)

ax.plot(date_c01, c01_v_tot, color='red', label='C01', linewidth=linewidth)
ax.plot(date_c03, c03_v_tot, color='blue', label='C03', linewidth=linewidth)

ax.set_xlabel('Time [UTC]')
ax.set_ylabel('Velocity [m/s]')
ax.set_title('Avalanche 16.03.2021 - Velocities AvaNodes')

ax.set_xlim([datetime(2021,3,16,9,2,10), datetime(2021,3,16,9,3,10)])

ax.grid()
ax.legend()

#fig.savefig('210316_avalanche_GNSS_velocities.png', dpi=300)