# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 11:56:08 2022

@author: neuhauser
"""
from datetime import datetime

from classes.GPS_Class import GPSData


# =============================================================================
# # Experiment 15.03.2021
# # C01
# #Seilbahnrinne
# gps_c01 = GPSData()
# path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-15_Nordkette_avalanche\C01\GPS\ava210315_C01.txt'
# gps_c01.path = path_c01
# gps_c01.read_data()
# start = datetime(2021,3,15,7,51,15)
# end = datetime(2021,3,15,7,52,30)
# gps_c01.plot_vel(True,False,True, start, end)
# max_pos = gps_c01.data_pos['v_total_pos'].loc[(gps_c01.data_pos['timestamp'] > start) & (gps_c01.data_pos['timestamp'] < end)].max()
# #max_doppler = gps_c01.data_pos['v_total'].loc[(gps_c01.data_pos['timestamp'] > start) & (gps_c01.data_pos['timestamp'] < end)].max()
# #mean_pDOP = gps_c01.data_pos['pDop[]'].loc[(gps_c01.data_pos['timestamp'] > start) & (gps_c01.data_pos['timestamp'] < end)].mean()
#  
# 
# #Juliusrinne
# gps_c01 = GPSData()
# path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-15_Nordkette_avalanche\C01\GPS\210315_julius_C01.txt'
# gps_c01.path = path_c01
# gps_c01.read_data()
# start = datetime(2021,3,15,8,32,30)
# end = datetime(2021,3,15,8,35,0)
# gps_c01.plot_vel(True, False, True, start, end)
# =============================================================================

# =============================================================================
# # Experiment 23.01.2022
# 
# gps_c01 = GPSData()
# path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-01-23_Nordkette_avalanche\C01\GPS\ava220123_C01.txt'
# gps_c01.path = path_c01
# gps_c01.read_data()
# start = datetime(2022,1,23,7,30,20)
# end = datetime(2022,1,23,7,31,30)
# gps_c01.plot_vel(True,False,True, start, end)
# max_pos = gps_c01.data_pos['v_total_pos'].max()
# max_doppler = gps_c01.data_pos['v_total'].max()
# mean_pDOP = gps_c01.data_pos['pDop[]'].mean()
# diff = abs(gps_c01.data_pos['v_total_pos'] - gps_c01.data_pos['v_total'])
# mean_diff = diff.mean()
# =============================================================================

# =============================================================================
# # Experiment 03.02.2022
# 
# gps_c10 = GPSData()
# path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-03_Nordkette_avalanche\C10\GPS\ava220203_C10_2avalanches.txt'
# gps_c10.path = path_c10
# gps_c10.read_data()
# #Seilbahnrinne
# start = datetime(2022,2,3,6,58,20)
# end = datetime(2022,2,3,6,59,40)
# gps_c10.plot_vel(True,False,True,start,end)
# #Juliusrinne
# start = datetime(2022,2,3,7,40,40)
# end = datetime(2022,2,3,7,41,30)
# gps_c10.plot_vel(True,False,True,start,end)
# 
# # Get max vel from Seilbahnrinne
# start = datetime(2022,2,3,6,58,20)
# end = datetime(2022,2,3,6,59,40)
# max_pos = gps_c10.data_pos['v_total_pos'].loc[(gps_c10.data_pos['timestamp'] > start) & (gps_c10.data_pos['timestamp'] < end)].max()
# max_doppler = gps_c10.data_pos['v_total'].loc[(gps_c10.data_pos['timestamp'] > start) & (gps_c10.data_pos['timestamp'] < end)].max()
# mean_pDOP = gps_c10.data_pos['pDop[]'].loc[(gps_c10.data_pos['timestamp'] > start) & (gps_c10.data_pos['timestamp'] < end)].mean()
# diff = abs(gps_c10.data_pos['v_total_pos'] - gps_c10.data_pos['v_total'])
# mean_diff = diff.mean()
# 
# #gps_c10.export_in_radar_cs()
# =============================================================================

# Experiment 07.02.2022
# troubles with gps signal...solved...
gps_c10 = GPSData()
path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-07_Nordkette_avalanche\C10\GPS\ava220207_C10_selfrelease.txt'
gps_c10.path = path_c10
gps_c10.read_data()
start = datetime(2022,2,7,7,57,30)
end = datetime(2022,2,7,7,57,50)
gps_c10.plot_vel(False,False,True)
# Avalanche in this time period!
start = datetime(2022,2,7,7,37,20)
end = datetime(2022,2,7,7,41,20)
gps_c10.plot_vel(False,False,True, start, end)

max_pos = gps_c10.data_pos['v_total_pos'].loc[(gps_c10.data_pos['timestamp'] > start) & (gps_c10.data_pos['timestamp'] < end)].max()
max_doppler = gps_c10.data_pos['v_total'].loc[(gps_c10.data_pos['timestamp'] > start) & (gps_c10.data_pos['timestamp'] < end)].max()
mean_pDOP = gps_c10.data_pos['pDop[]'].loc[(gps_c10.data_pos['timestamp'] > start) & (gps_c10.data_pos['timestamp'] < end)].mean()
diff = abs(gps_c10.data_pos['v_total_pos'] - gps_c10.data_pos['v_total'])
mean_diff = diff.mean()
