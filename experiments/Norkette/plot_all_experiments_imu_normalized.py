# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 11:56:08 2022

@author: neuhauser
"""

#%% Libs
import matplotlib.pyplot as plt
from datetime import datetime
import numpy as np

from classes.GPS_Class import GPSData
from classes.IMU_Class import ImuData

#%% Settings

save_path = r"C:\git_rep\dissertation\papers\GPS_paper\figures\exp"
# Colors
color_imu = 'black'
color_doppler = 'red'
color_position = 'blue'
color_rotation = 'green'
# Linestyles
ls_imu = 'solid'
ls_pos = 'dashed'
ls_doppler = 'dotted'
# Save
save = False
# Smoothing
kernel_size = 160
kernel = np.ones(kernel_size) / kernel_size

#%% Read Data

# Experiment 15.03.2021
# C01
# Seilbahnrinne

imu_c01 = ImuData()
imu_path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-15_Nordkette_avalanche\C01\Leader\ava210315_C01_Leader.txt'
imu_c01.path = imu_path_c01
imu_c01.read_data_pd()

# Settings
imu_start_idx = 14800
imu_end_idx = 32400

imu_time = imu_c01.time[imu_start_idx:imu_end_idx]
imu_time = (imu_time - imu_time.min())/ (imu_time.max() - imu_time.min())
imu_time_210315 = imu_time

# Smoothing
imu_acc_210315 = imu_c01.acc_tot[imu_start_idx:imu_end_idx]
imu_acc_210315 = np.convolve(imu_acc_210315, kernel, mode='same')

imu_rot_210315 = imu_c01.gyro_tot[imu_start_idx:imu_end_idx]
imu_rot_210315 = np.convolve(imu_rot_210315, kernel, mode='same')

# Normalization
imu_acc_210315 = (imu_c01.acc_tot[imu_start_idx:imu_end_idx] - 9.81) / (imu_c01.acc_tot[imu_start_idx:imu_end_idx] - 9.81).max()
imu_rot_210315 = imu_c01.gyro_tot[imu_start_idx:imu_end_idx] / imu_c01.gyro_tot[imu_start_idx:imu_end_idx].max()


# Experiment 16.03.2021
# C01
# Seilbahnrinne
imu_c01 = ImuData()
imu_path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-16_Nordkette_avalanche\C01\Leader\ava210316_C01_Leader.txt'
imu_c01.path = imu_path_c01
imu_c01.read_data_pd()

# Settings
imu_start_idx = 38*400
imu_end_idx = 85*400

imu_time = imu_c01.time[imu_start_idx:imu_end_idx]
imu_time = (imu_time - imu_time.min())/ (imu_time.max() - imu_time.min())
imu_time_210316 = imu_time

# Smoothing 
imu_acc_210316 = imu_c01.acc_tot[imu_start_idx:imu_end_idx]
imu_acc_210316 = np.convolve(imu_acc_210316, kernel, mode='same')               
imu_rot_210316 = imu_c01.gyro_tot[imu_start_idx:imu_end_idx]
imu_rot_210316 = np.convolve(imu_rot_210316, kernel, mode='same')
               
# Nomralization                
imu_acc_210316 = (imu_c01.acc_tot[imu_start_idx:imu_end_idx] - 9.81) / (imu_c01.acc_tot[imu_start_idx:imu_end_idx] - 9.81).max()
imu_rot_210316 = imu_c01.gyro_tot[imu_start_idx:imu_end_idx] / imu_c01.gyro_tot[imu_start_idx:imu_end_idx].max()


# C03
# Seilbahnrinne
imu_c03 = ImuData()
imu_path_c03 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-03-16_Nordkette_avalanche\C03\Leader\ava210316_C03_Leader.txt'
imu_c03.path = imu_path_c03
imu_c03.read_data_pd()

# Settings
imu_start_idx = 70*400
imu_end_idx = 115*400

imu_time = imu_c03.time[imu_start_idx:imu_end_idx]
imu_time = (imu_time - imu_time.min())/ (imu_time.max() - imu_time.min())
imu_time_210316_c03 = imu_time

# Smoothing
imu_acc_210316_c03 = imu_c03.acc_tot[imu_start_idx:imu_end_idx]
imu_acc_210316_c03 = np.convolve(imu_acc_210316_c03, kernel, mode='same')
imu_rot_210316_c03 = imu_c03.gyro_tot[imu_start_idx:imu_end_idx]
imu_rot_210316_c03 = np.convolve(imu_rot_210316_c03, kernel, mode='same')

# Nomralization
imu_acc_210316_c03 = (imu_c03.acc_tot[imu_start_idx:imu_end_idx] - 9.81) / (imu_c03.acc_tot[imu_start_idx:imu_end_idx] - 9.81).max()
imu_rot_210316_c03 = imu_c03.gyro_tot[imu_start_idx:imu_end_idx] / imu_c03.gyro_tot[imu_start_idx:imu_end_idx].max()


# Experiment 23.01.2022
# C01
imu_c01 = ImuData()
imu_path_c01 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-01-23_Nordkette_avalanche\C01\Leader\ava220123_C01_Leader.txt'
imu_c01.path = imu_path_c01
imu_c01.read_data_pd()

# Settings
imu_start_idx = 45*400
imu_end_idx = 95*400

imu_time = imu_c01.time[imu_start_idx:imu_end_idx]
imu_time = (imu_time - imu_time.min())/ (imu_time.max() - imu_time.min())
imu_time_220123 = imu_time

# Smoothing
imu_acc_220123 = imu_c01.acc_tot[imu_start_idx:imu_end_idx]
imu_acc_220123 = np.convolve(imu_acc_220123, kernel, mode='same')
imu_rot_220123 = imu_c01.gyro_tot[imu_start_idx:imu_end_idx]
imu_rot_220123 = np.convolve(imu_rot_220123, kernel, mode='same')

# Normalization
imu_acc_220123 = (imu_c01.acc_tot[imu_start_idx:imu_end_idx] - 9.81) / (imu_c01.acc_tot[imu_start_idx:imu_end_idx] - 9.81).max()
imu_rot_220123 = imu_c01.gyro_tot[imu_start_idx:imu_end_idx] / imu_c01.gyro_tot[imu_start_idx:imu_end_idx].max()


# Experiment 03.02.2022
# C 10
# Seilbahnrinne
imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-03_Nordkette_avalanche\C10\Leader\ava220203_seilbahn_C10_imu.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Settings
imu_start_idx = 135*400
imu_end_idx = 190*400

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
imu_time = (imu_time - imu_time.min())/ (imu_time.max() - imu_time.min())
imu_time_220203 = imu_time

# Smoothing
imu_acc_220203 = imu_c10.acc_tot[imu_start_idx:imu_end_idx]
imu_acc_220203 = np.convolve(imu_acc_220203, kernel, mode='same')                 
imu_rot_220203 = imu_c10.gyro_tot[imu_start_idx:imu_end_idx]
imu_rot_220203 = np.convolve(imu_rot_220203, kernel, mode='same')                 

# Nomalization               
imu_acc_220203 = (imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81) / (imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81).max()
imu_rot_220203 = imu_c10.gyro_tot[imu_start_idx:imu_end_idx] / imu_c10.gyro_tot[imu_start_idx:imu_end_idx].max()


# Experiment 22.02.2022
# C07
imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C07\Leader\220222_C07_avalanche_Leader.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Settings
imu_start_idx = 40*400
imu_end_idx = 85*400

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
imu_time = (imu_time - imu_time.min())/ (imu_time.max() - imu_time.min())
imu_time_220222_c07 = imu_time

# Smoothing
imu_acc_220222_c07 = imu_c10.acc_tot[imu_start_idx:imu_end_idx]
imu_acc_220222_c07 = np.convolve(imu_acc_220222_c07, kernel, mode='same')
imu_rot_220222_c07 = imu_c10.gyro_tot[imu_start_idx:imu_end_idx]
imu_rot_220222_c07 = np.convolve(imu_rot_220222_c07, kernel, mode='same')

# Normalization
imu_acc_220222_c07 = (imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81) / (imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81).max()
imu_rot_220222_c07 = imu_c10.gyro_tot[imu_start_idx:imu_end_idx] / imu_c10.gyro_tot[imu_start_idx:imu_end_idx].max()


# C09
imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C09\Leader\220222_C09_avalanche_Leader.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Settings
imu_start_idx = 25*400
imu_end_idx = 70*400

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
imu_time = (imu_time - imu_time.min())/ (imu_time.max() - imu_time.min())
imu_time_220222_c09 = imu_time

# Smoothing
imu_acc_220222_c09 = imu_c10.acc_tot[imu_start_idx:imu_end_idx]
imu_acc_220222_c09 = np.convolve(imu_acc_220222_c09, kernel, mode='same')
imu_rot_220222_c09 = imu_c10.gyro_tot[imu_start_idx:imu_end_idx]
imu_rot_220222_c09 = np.convolve(imu_rot_220222_c09, kernel, mode='same')

# Nomralization
imu_acc_220222_c09 = (imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81) / (imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81).max()
imu_rot_220222_c09 = imu_c10.gyro_tot[imu_start_idx:imu_end_idx] / imu_c10.gyro_tot[imu_start_idx:imu_end_idx].max()


# C10
imu_c10 = ImuData()
imu_path_c10 = r'C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2022-02-22_Nordkette_avalanche\C10\Leader\220222_C10_avalanche_Leader.txt'
imu_c10.path = imu_path_c10
imu_c10.read_data_pd()

# Settings
imu_start_idx = 55*400
imu_end_idx = 100*400

imu_time = imu_c10.time[imu_start_idx:imu_end_idx]
imu_time = (imu_time - imu_time.min())/ (imu_time.max() - imu_time.min())
imu_time_220222_c10 = imu_time

# Smoothing
imu_acc_220222_c10 = imu_c10.acc_tot[imu_start_idx:imu_end_idx]
imu_acc_220222_c10 = np.convolve(imu_acc_220222_c10, kernel, mode='same')
imu_rot_220222_c10 = imu_c10.gyro_tot[imu_start_idx:imu_end_idx]
imu_rot_220222_c10 = np.convolve(imu_rot_220222_c10, kernel, mode='same')

# Nomralization
imu_acc_220222_c10 = (imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81) / (imu_c10.acc_tot[imu_start_idx:imu_end_idx] - 9.81).max()
imu_rot_220222_c10 = imu_c10.gyro_tot[imu_start_idx:imu_end_idx] / imu_c10.gyro_tot[imu_start_idx:imu_end_idx].max()



#%% Plot Data

fig, ((ax_acc, ax_rot)) = plt.subplots(2,1, figsize=(30, 15))
 
# Plots
ax_acc.plot(imu_time_210315, imu_acc_210315)
ax_rot.plot(imu_time_210315, imu_rot_210315)

ax_acc.plot(imu_time_210316, imu_acc_210316)
ax_rot.plot(imu_time_210316, imu_rot_210316)

ax_acc.plot(imu_time_210316_c03, imu_acc_210316_c03)
ax_rot.plot(imu_time_210316_c03, imu_rot_210316_c03)

ax_acc.plot(imu_time_220123, imu_acc_220123)
ax_rot.plot(imu_time_220123, imu_rot_220123)

ax_acc.plot(imu_time_220203, imu_acc_220203)
ax_rot.plot(imu_time_220203, imu_rot_220203)

ax_acc.plot(imu_time_220222_c07, imu_acc_220222_c07)
ax_rot.plot(imu_time_220222_c07, imu_rot_220222_c07)

ax_acc.plot(imu_time_220222_c07, imu_acc_220222_c09)
ax_rot.plot(imu_time_220222_c07, imu_rot_220222_c09)

ax_acc.plot(imu_time_220222_c07, imu_acc_220222_c10)
ax_rot.plot(imu_time_220222_c07, imu_rot_220222_c10)

# =============================================================================
# fig, ((ax_acc, ax_rot)) = plt.subplots(2,1, figsize=(30, 15))
# 
# 
# # Plots
# acc1, = ax_acc.plot(imu_time, imu_acc_210315, color=color_imu, linestyle=ls_imu, label='IMU Acc')
# ax_rot.plot(imu_time, imu_rot_210315)
# # Title
# =============================================================================
#ax_acc.set_title('Accelerometer')
#ax_gyro.set_title('Gyrometer')
#ax_vel.set_title('Velocity')
#ax_dist.set_title('Distance')
# =============================================================================
# fig.suptitle('Avalanche 15.03.2021 - AvaNode C01 - Seilbahnrinne')
# 
# # Axes
# ax_acc.set_ylabel('Acceleration [m/s²]')
# #ax_acc.set_xlabel('Time [sec]')
# ax_gyro.set_ylabel('Rotation [°/s]')
# 
# ax_vel.set_ylabel('Velocity [m/s]')
# ax_dist.set_ylabel('Distance [m]')
# 
# ax_dist.set_xlabel('Time [sec]')
# 
# # Legend
# ax_acc.legend()
# ax_acc.legend([acc1, acc2, gyro1], ["IMU Acc", "Pos. Acc", "Rotation"])
# 
# # Save
# 
# fig.tight_layout()
# if save:
#     fig.savefig(save_path + "/210315_Seilbahnrinne_C01_alldata.png", dpi=300)
# # Time if needed
# start = datetime(2021,3,15,7,51,15)
# end = datetime(2021,3,15,7,52,30)
# # Compare Plot
# fig_compare, ax_comp = plt.subplots()
# s = gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['tot_dist'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
# v = gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)] / gps_c01.data_pos['v_total_pos'].iloc[int(imu_start_idx/40):int(imu_end_idx/40)].max()
# time = gps_time - gps_time.iloc[0]
# time /= time.max()
# 
# ax_comp.scatter(time,v, label='2021.03.15 - Seilbahn')
# ax_comp.set_xlabel('s/s_max')
# ax_comp.set_xlabel('t/t_max')
# ax_comp.set_ylabel('v/v_max')
# 
# =============================================================================