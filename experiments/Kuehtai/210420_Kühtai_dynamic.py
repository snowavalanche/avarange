# -*- coding: utf-8 -*-
"""
Created on Fri Apr  8 11:14:16 2022

@author: neuhauser
"""

import numpy as np

import pandas as pd
from matplotlib import pyplot as plt
import matplotlib.dates as dates

from geopy import distance
from datetime import datetime, timedelta

from classes.GPS_Class import GPSData

#Read Gps Input for Position Data and convert it in cartesian coordinates
#'Kühtai'
#Run 1
path_emlid_run1 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data/measurements/2021-04-20_Kühtai/GNSS_Emlid/DynaminRun1_Emlid.pos'
path_node_run1 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data\measurements/2021-04-20_Kühtai/dynamisch_Messung1/GPS/dynamic1.txt'
# Run 2
path_emlid_run2 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data/measurements/2021-04-20_Kühtai/GNSS_Emlid/DynaminRun2_Emlid.pos'
path_node_run2 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data\measurements/2021-04-20_Kühtai/dynamisch_Messung2/GPS/dynamic2.txt'
#Run3
path_emlid_run3 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data/measurements/2021-04-20_Kühtai/GNSS_Emlid/DynaminRun3_Emlid.pos'
path_node_run3 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data\measurements/2021-04-20_Kühtai/dynamisch_Messung3/GPS/dynamic3.txt'

# Read data to dataframe
# Run 1 - Snow Cover = 0
columns = ["GPST", "latitude[deg]", "longitude[deg]", "height[m]", "Q", "ns", "sdn[m]", "sde[m]", "sdu[m]", "sdne[m]", "sdeu", "sdun", "age[s]", "ratio"]
emlid_run1 = pd.read_csv(path_emlid_run1, header=10, names=columns, delim_whitespace=True)

node_run_1 = GPSData()
node_run_1.path = path_node_run1
node_run_1.read_data()

# Run 2 - Snow Cover = 10 cm
columns = ["GPST", "latitude[deg]", "longitude[deg]", "height[m]", "Q", "ns", "sdn[m]", "sde[m]", "sdu[m]", "sdne[m]", "sdeu", "sdun", "age[s]", "ratio"]
emlid_run2 = pd.read_csv(path_emlid_run2, header=10, names=columns, delim_whitespace=True)

node_run_2 = GPSData()
node_run_2.path = path_node_run2
node_run_2.read_data()

# Run 3 - Snow Cover = 20 cm
columns = ["GPST", "latitude[deg]", "longitude[deg]", "height[m]", "Q", "ns", "sdn[m]", "sde[m]", "sdu[m]", "sdne[m]", "sdeu", "sdun", "age[s]", "ratio"]
emlid_run3 = pd.read_csv(path_emlid_run3, header=10, names=columns, delim_whitespace=True)

node_run_3 = GPSData()
node_run_3.path = path_node_run3
node_run_3.read_data()

del path_emlid_run1, path_emlid_run2, path_emlid_run3, path_node_run1, path_node_run2, path_node_run3, columns

# Calculate local coordinates from WGS84 Coordinates

df_list = [emlid_run1, emlid_run2, emlid_run3, node_run_1.data, node_run_2.data, node_run_3.data]

for df in df_list:
    df["x"] = 0
    df["y"] = 0
    df["z"] = 0
    
    for i in range(1, df.shape[0]):
        delta_x_0 = (df["latitude[deg]"][0], df["longitude[deg]"][0])
        delta_x_1 = (df["latitude[deg]"][i], df["longitude[deg]"][0])
        dx = distance.distance(delta_x_0, delta_x_1).m
        if df["latitude[deg]"][i] > df["latitude[deg]"][0]:
            df["x"].iloc[i] = dx
        else:
            df["x"].iloc[i] = -dx
    
        delta_y_0 = (df["latitude[deg]"][0], df["longitude[deg]"][0])
        delta_y_1 = (df["latitude[deg]"][0], df["longitude[deg]"][i])
        dy = distance.distance(delta_y_0, delta_y_1).m
        if df["longitude[deg]"][i] > df["longitude[deg]"][0]:
            df["y"].iloc[i] = dy
        else:
            df["y"].iloc[i] = -dy
            
        delta_z = df['height[m]'][0] - df['height[m]'][i]
        df["z"].iloc[i] = delta_z

del delta_x_0, delta_x_1, delta_y_0, delta_y_1, delta_z, dx, dy, i, df


emlid_list =[emlid_run1, emlid_run2, emlid_run3] 
# Calculate velocities from coordinates
leapseconds = 18 # sec between GPST and UTC, 18 for 2021, source: https://de.wikipedia.org/wiki/GPS-Zeit
for emlid_run in emlid_list:
    emlid_run["date"] = emlid_run.index
    emlid_run["timestamp"] = pd.to_datetime(emlid_run["date"] + ' ' + emlid_run["GPST"])
    emlid_run["timestamp"] = emlid_run["timestamp"] - timedelta(seconds=leapseconds)

for df in df_list:
    
    dt = df["timestamp"].iloc[1] - df["timestamp"].iloc[0]    
    df["dt"] = dt.total_seconds()
    df["dx"] = df["x"].diff()
    df["dy"] = df["y"].diff()
    df["dz"] = df["z"].diff()
    
    df["vx"] = df["dx"] / df["dt"]
    df["vy"] = df["dy"] / df["dt"]
    df["vz"] = df["dz"] / df["dt"]

del dt, df   
 
# Differnce for total velocities

for df in df_list:
    df["v_tot"] = np.linalg.norm([df["vx"], df["vy"], df["vz"]], axis=0)

# Run 1
diff_v_doppler_run1 = []
diff_v_pos_run1 = []

for idx_emlid, time in enumerate(emlid_run1["timestamp"]):
    for idx_node, node_time in enumerate(node_run_1.data["timestamp"]):
        if node_time == time and emlid_run1["Q"].iloc[idx_emlid] == 1:
            diff_v_doppler_run1.append(emlid_run1["v_tot"].iloc[idx_emlid] - np.mean([node_run_1.data["v_total"].iloc[idx_node], node_run_1.data["v_total"].iloc[idx_node+1]]))
            diff_v_pos_run1.append(emlid_run1["v_tot"].iloc[idx_emlid] - np.mean([node_run_1.data["v_tot"].iloc[idx_node], node_run_1.data["v_tot"].iloc[idx_node+1]]))

# Run 2
diff_v_doppler_run2 = []
diff_v_pos_run2 = []

for idx_emlid, time in enumerate(emlid_run2["timestamp"]):
    for idx_node, node_time in enumerate(node_run_2.data["timestamp"]):
        if node_time == time and idx_node < len(node_run_2.data["timestamp"])-1 and emlid_run2["Q"].iloc[idx_emlid] == 1:
            diff_v_doppler_run2.append(emlid_run2["v_tot"].iloc[idx_emlid] - np.mean([node_run_2.data["v_total"].iloc[idx_node], node_run_2.data["v_total"].iloc[idx_node+1]]))
            diff_v_pos_run2.append(emlid_run2["v_tot"].iloc[idx_emlid] - np.mean([node_run_2.data["v_tot"].iloc[idx_node], node_run_2.data["v_tot"].iloc[idx_node+1]]))

# Run 3
diff_v_doppler_run3 = []
diff_v_pos_run3 = []

for idx_emlid, time in enumerate(emlid_run3["timestamp"]):
    for idx_node, node_time in enumerate(node_run_3.data["timestamp"]):
        if node_time == time and idx_node < len(node_run_3.data["timestamp"])-1 and emlid_run3["Q"].iloc[idx_emlid] == 1:
            diff_v_doppler_run3.append(emlid_run3["v_tot"].iloc[idx_emlid] - np.mean([node_run_3.data["v_total"].iloc[idx_node], node_run_3.data["v_total"].iloc[idx_node+1]]))
            diff_v_pos_run3.append(emlid_run3["v_tot"].iloc[idx_emlid] - np.mean([node_run_3.data["v_tot"].iloc[idx_node], node_run_3.data["v_tot"].iloc[idx_node+1]]))

diff_v_doppler_run2 = diff_v_doppler_run2[1:]
diff_v_pos_run2 = diff_v_pos_run2[1:]
diff_v_doppler_run3 = diff_v_doppler_run3[1:]
diff_v_pos_run3 = diff_v_pos_run3[1:]

# Plot for Overview

fig, ax = plt.subplots()
date_emlid = dates.date2num(emlid_run3['timestamp'])
date_node = dates.date2num(node_run_3.data['timestamp'])
# Define the date format
date_form = dates.DateFormatter("%H:%M:%S")
ax.xaxis.set_major_formatter(date_form)
fig.autofmt_xdate(rotation=45)

ax.plot(date_emlid, emlid_run3["v_tot"], label="Emlid")
#ax.plot(date_node, node_run_3.data["vx"], label="Node Pos")
ax.plot(date_node, node_run_3.data["v_total"], label="Doppler")

ax.legend()

# Boxplot Doppler Velocity
labels = ['0 cm', "10 cm", "20 cm"]  # '10 cm', '20 cm', 'Whole Dataset'
data = [diff_v_doppler_run1, diff_v_doppler_run2, diff_v_doppler_run3]
fig1, ax1 = plt.subplots()
ax1.boxplot(data,
            vert=True,  # vertical box alignment
            patch_artist=True,  # fill with color
            labels=labels,
            showfliers=False)  # will be used to label x-ticks
ax1.set_xlabel('Snowcoverage')
ax1.set_ylabel('Deviation [m/s]')
#ax1.set_xticklabels([5, 6, 7, 8, 9, 10])
ax1.grid()
ax1.set_title('Deviation of GPS Velocity during movement - AvaNode vs. RTK')
#fig1.savefig('Kühtai_Deviation_Velocity.png', dpi=300)

# Boxplot pos Velocity
labels = ['0 cm', "10 cm", "20 cm"]  # '10 cm', '20 cm', 'Whole Dataset'
data = [diff_v_pos_run1, diff_v_pos_run2, diff_v_pos_run3]
fig1, ax1 = plt.subplots()
ax1.boxplot(data,
            vert=True,  # vertical box alignment
            patch_artist=True,  # fill with color
            labels=labels,
            showfliers=False)  # will be used to label x-ticks
ax1.set_xlabel('Snowcoverage')
ax1.set_ylabel('Deviation [m/s]')
#ax1.set_xticklabels([5, 6, 7, 8, 9, 10])
ax1.grid()
ax1.set_title('Deviation of GPS Velocity during movement - AvaNode vs. RTK')
#fig1.savefig('Kühtai_Deviation_Velocity.png', dpi=300)

# ToDo: Plot standarddeviation of Emlid accuracy
