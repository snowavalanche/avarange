from math import cos, sin, asin, sqrt, radians
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def calc_distance(lat1, lon1, lat2, lon2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    https://en.wikipedia.org/wiki/Haversine_formula
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * asin(sqrt(a))
    km = 6371 * c
    return km

#Run 1
path_emlid_run1 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data/measurements/2021-04-20_Kühtai/GNSS_Emlid/DynaminRun1_Emlid.pos'
path_node_run1 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data\measurements/2021-04-20_Kühtai/dynamisch_Messung1/GPS/dynamic1.txt'
# Run 2
path_emlid_run2 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data/measurements/2021-04-20_Kühtai/GNSS_Emlid/DynaminRun2_Emlid.pos'
path_node_run2 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data\measurements/2021-04-20_Kühtai/dynamisch_Messung2/GPS/dynamic2.txt'
#Run3
path_emlid_run3 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data/measurements/2021-04-20_Kühtai/GNSS_Emlid/DynaminRun3_Emlid.pos'
path_node_run3 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data\measurements/2021-04-20_Kühtai/dynamisch_Messung3/GPS/dynamic3.txt'

#Read Files
run1_emlid_file = pd.read_csv(path_emlid_run1, header=9, sep='  ')
run1_node_file = pd.read_csv(path_node_run1)

run2_emlid_file = pd.read_csv(path_emlid_run2, header=9, sep='  ')
run2_node_file = pd.read_csv(path_node_run2)

run3_emlid_file = pd.read_csv(path_emlid_run3, header=9, sep='  ')
run3_node_file = pd.read_csv(path_node_run3)

# Run1
# Convert to arrays
lat_emlid = run1_emlid_file.iloc[:, 1].to_numpy()
long_emlid = run1_emlid_file.iloc[:, 2].to_numpy()

lat_node = run1_node_file.iloc[:, 4].to_numpy()
long_node = run1_node_file.iloc[:, 3].to_numpy()

nr_satellites = run1_node_file['numSV[]'].to_numpy()


idx_factor = len(lat_node)/len(lat_emlid)

distance_run1 = []
nr_satellites_run1 = []

box_plot_5 = []
box_plot_6 = []
box_plot_7 = []
box_plot_8 = []
box_plot_9 = []
box_plot_10 = []

for i in range(len(lat_emlid)):
    distance_m = calc_distance(lat_emlid[i], long_emlid[i], lat_node[int(i*idx_factor)], long_node[int(i*idx_factor)]) * 1000
    distance_run1.append(distance_m)
    nr_satellites_run1.append(nr_satellites[int(i*idx_factor)])
    if nr_satellites_run1[i] == 5:
        box_plot_5.append(distance_run1[i])
    if nr_satellites_run1[i] == 6:
        box_plot_6.append(distance_run1[i])

# Run2
# Convert to arrays
lat_emlid = run2_emlid_file.iloc[:, 1].to_numpy()
long_emlid = run2_emlid_file.iloc[:, 2].to_numpy()

lat_node = run2_node_file.iloc[:, 4].to_numpy()
long_node = run2_node_file.iloc[:, 3].to_numpy()

idx_factor = len(lat_node)/len(lat_emlid)
nr_satellites = run2_node_file['numSV[]'].to_numpy()

distance_run2 = []
nr_satellites_run2 = []

for i in range(len(lat_emlid)):
    distance_m = calc_distance(lat_emlid[i], long_emlid[i], lat_node[int(i*idx_factor)], long_node[int(i*idx_factor)]) * 1000
    distance_run2.append(distance_m)
    nr_satellites_run2.append(nr_satellites[int(i * idx_factor)])
    if nr_satellites_run2[i] == 8:
        box_plot_8.append(distance_run2[i])
    if nr_satellites_run2[i] == 9:
        box_plot_9.append(distance_run2[i])
    if nr_satellites_run2[i] == 10:
        box_plot_10.append(distance_run2[i])

# Run3
# Convert to arrays
lat_emlid = run3_emlid_file.iloc[:, 1].to_numpy()
long_emlid = run3_emlid_file.iloc[:, 2].to_numpy()

lat_node = run3_node_file.iloc[:, 4].to_numpy()
long_node = run3_node_file.iloc[:, 3].to_numpy()

idx_factor = len(lat_node) / len(lat_emlid)
nr_satellites = run3_node_file['numSV[]'].to_numpy()

distance_run3 = []
nr_satellites_run3 = []

for i in range(len(lat_emlid)):
    distance_m = calc_distance(lat_emlid[i], long_emlid[i], lat_node[int(i * idx_factor)],
                               long_node[int(i * idx_factor)]) * 1000
    distance_run3.append(distance_m)
    nr_satellites_run3.append(nr_satellites[int(i * idx_factor)])
    if nr_satellites_run2[i] == 8:
        box_plot_8.append(distance_run3[i])
    if nr_satellites_run2[i] == 7:
        box_plot_7.append(distance_run3[i])


fig, ax = plt.subplots()
ax.plot(distance_run1, color='r', label='Deviation Run 1')
ax.plot(nr_satellites_run1, color='r', label='Satellites Run 1', ls='--')
ax.plot(distance_run2, color='b', label='Deviation Run 2')
ax.plot(nr_satellites_run2, color='b', label='Satellites Run 2', ls='--')
ax.plot(distance_run3, color='g', label='Deviation Run 3')
ax.plot(nr_satellites_run3, color='g', label='Satellites Run 3', ls='--')
ax.legend()
ax.grid()
ax.set_xlabel('Index')
ax.set_ylabel('Deviation [m]')
ax.set_title('GPS Measurements - Kühtai 20.04.2021 - RTK vs. AvaNode')
fig.tight_layout()
#fig.savefig('GPS_Kühtai_20210420_Deviation.png', dpi=300)

data = [box_plot_5, box_plot_6, box_plot_7, box_plot_8, box_plot_9, box_plot_10]
labels = [5, 6, 7, 8, 9, 10]

fig1, ax1 = plt.subplots()
ax1.boxplot(data,
            vert=True,  # vertical box alignment
            patch_artist=True,  # fill with color
            labels=labels)  # will be used to label x-ticks
ax1.set_xlabel('Nr. of Satellites')
ax1.set_ylabel('Deviation [m]')
#ax1.set_xticklabels([5, 6, 7, 8, 9, 10])
ax1.grid()
ax1.set_title('Deviation of GPS Position during movement - AvaNode vs. RTK')
#fig1.savefig('Kühtai_Deviation_Position.png', dpi=300)

plt.show()
print('done')

df = pd.DataFrame(data, index=labels)
#df.to_csv('Kuethai_dynamic_nr_sat_deviation.csv')
print('...')


