# -*- coding: utf-8 -*-
"""
Created on Thu Nov 25 21:20:27 2021

@author: neuhauser
"""

from math import cos, sin, asin, sqrt, radians
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


#Run 1
path_emlid_run1 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data/measurements/2021-04-20_Kühtai/GNSS_Emlid/DynaminRun1_Emlid_local_coord.txt'
path_node_run1 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data\measurements/2021-04-20_Kühtai/dynamisch_Messung1/GPS/dynamic1_local_coord.txt'
# Run 2
path_emlid_run2 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data/measurements/2021-04-20_Kühtai/GNSS_Emlid/DynaminRun2_Emlid_local_coord.txt'
path_node_run2 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data\measurements/2021-04-20_Kühtai/dynamisch_Messung2/GPS/dynamic2_local_coord.txt'
#Run3
path_emlid_run3 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data/measurements/2021-04-20_Kühtai/GNSS_Emlid/DynaminRun3_Emlid_local_coord.txt'
path_node_run3 = 'C:/Users/neuhauser/OneDrive - Bundesforschungszentrum fuer Wald/20070-AvaRange-data\measurements/2021-04-20_Kühtai/dynamisch_Messung3/GPS/dynamic3_local_coord.txt'

gps_path_run1 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-04-20_Kühtai\dynamisch_Messung1\GPS\dynamic1.txt"
gps_path_run2 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-04-20_Kühtai\dynamisch_Messung2\GPS\dynamic2.txt"
gps_path_run3 = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2021-04-20_Kühtai\dynamisch_Messung3\GPS\dynamic3.txt"

emlid_run1 = np.loadtxt(path_emlid_run1, skiprows=1, delimiter=',')
emlid_run2 = np.loadtxt(path_emlid_run2, skiprows=1, delimiter=',')
emlid_run3 = np.loadtxt(path_emlid_run3, skiprows=1, delimiter=',')

node_run1 = np.loadtxt(path_node_run1, skiprows=1, delimiter=',')
node_run2 = np.loadtxt(path_node_run2, skiprows=1, delimiter=',')
node_run3 = np.loadtxt(path_node_run3, skiprows=1, delimiter=',')


#Run 1
idx = 0
idx_factor = len(node_run1)/len(emlid_run1)

vx_emlid_1 = np.zeros(len(emlid_run1))
vy_emlid_1 = np.zeros(len(emlid_run1))
vx_node_1 = np.zeros(len(emlid_run1))
vy_node_1 = np.zeros(len(emlid_run1))

while idx < len(emlid_run1)-1:
    vx_emlid_1[idx] = (emlid_run1[idx + 1, 1] - emlid_run1[idx, 1])/(emlid_run1[idx + 1, 0] - emlid_run1[idx, 0])
    vy_emlid_1[idx] = (emlid_run1[idx + 1, 2] - emlid_run1[idx, 2])/(emlid_run1[idx + 1, 0] - emlid_run1[idx, 0])
    
    vx_node_1[idx] = (node_run1[int(idx * idx_factor) + 1, 1] - node_run1[int(idx * idx_factor), 1])/(node_run1[int(idx * idx_factor) + 1, 0] - node_run1[int(idx * idx_factor), 0])
    vy_node_1[idx] = (node_run1[int(idx * idx_factor) + 1, 2] - node_run1[int(idx * idx_factor), 2])/(node_run1[int(idx * idx_factor) + 1, 0] - node_run1[int(idx * idx_factor), 0])
    
    idx += 1

v_tot_emlid1 = np.sqrt(vx_emlid_1 ** 2 + vy_emlid_1 ** 2)
v_tot_node1 = np.sqrt(vx_node_1 ** 2 + vy_node_1 ** 2)     
dv_run1 = v_tot_emlid1 - v_tot_node1

plt.plot(v_tot_emlid1 - v_tot_node1)

#Run 2
idx = 0
idx_factor = len(node_run2)/len(emlid_run2)

vx_emlid_2 = np.zeros(len(emlid_run2))
vy_emlid_2 = np.zeros(len(emlid_run2))
vx_node_2 = np.zeros(len(emlid_run2))
vy_node_2 = np.zeros(len(emlid_run2))

while idx < len(emlid_run2)-1:
    vx_emlid_2[idx] = (emlid_run2[idx + 1, 1] - emlid_run2[idx, 1])/(emlid_run2[idx + 1, 0] - emlid_run2[idx, 0])
    vy_emlid_2[idx] = (emlid_run2[idx + 1, 2] - emlid_run2[idx, 2])/(emlid_run2[idx + 1, 0] - emlid_run2[idx, 0])
    
    vx_node_2[idx] = (node_run2[int(idx * idx_factor) + 1, 1] - node_run2[int(idx * idx_factor), 1])/(node_run2[int(idx * idx_factor) + 1, 0] - node_run2[int(idx * idx_factor), 0])
    vy_node_2[idx] = (node_run2[int(idx * idx_factor) + 1, 2] - node_run2[int(idx * idx_factor), 2])/(node_run2[int(idx * idx_factor) + 1, 0] - node_run2[int(idx * idx_factor), 0])
    
    idx += 1

v_tot_emlid2 = np.sqrt(vx_emlid_2 ** 2 + vy_emlid_2 ** 2)
v_tot_node2 = np.sqrt(vx_node_2 ** 2 + vy_node_2 ** 2)     
dv_run2 = v_tot_emlid2 - v_tot_node2

plt.plot(v_tot_emlid2 - v_tot_node2)

#Run 3
idx = 0
idx_factor = len(node_run3)/len(emlid_run3)

vx_emlid_3 = np.zeros(len(emlid_run3))
vy_emlid_3 = np.zeros(len(emlid_run3))
vx_node_3 = np.zeros(len(emlid_run3))
vy_node_3 = np.zeros(len(emlid_run3))

while idx < len(emlid_run3)-1:
    vx_emlid_3[idx] = (emlid_run3[idx + 1, 1] - emlid_run3[idx, 1])/(emlid_run3[idx + 1, 0] - emlid_run3[idx, 0])
    vy_emlid_3[idx] = (emlid_run3[idx + 1, 2] - emlid_run3[idx, 2])/(emlid_run3[idx + 1, 0] - emlid_run3[idx, 0])
    
    vx_node_3[idx] = (node_run3[int(idx * idx_factor) + 1, 1] - node_run3[int(idx * idx_factor), 1])/(node_run3[int(idx * idx_factor) + 1, 0] - node_run3[int(idx * idx_factor), 0])
    vy_node_3[idx] = (node_run3[int(idx * idx_factor) + 1, 2] - node_run3[int(idx * idx_factor), 2])/(node_run3[int(idx * idx_factor) + 1, 0] - node_run3[int(idx * idx_factor), 0])
    
    idx += 1

v_tot_emlid3 = np.sqrt(vx_emlid_3 ** 2 + vy_emlid_3 ** 2)
v_tot_node3 = np.sqrt(vx_node_3 ** 2 + vy_node_3 ** 2)     

dv_run3 = v_tot_emlid3 - v_tot_node3

plt.plot(v_tot_emlid3 - v_tot_node3)

dv = dv_run1
dv = np.append(dv, dv_run2)


data = [dv_run1, dv_run2, dv_run3, dv]
labels = ['0 cm', '10 cm', '20 cm', 'Whole Dataset']

fig1, ax1 = plt.subplots()
ax1.boxplot(data,
            vert=True,  # vertical box alignment
            patch_artist=True,  # fill with color
            labels=labels)  # will be used to label x-ticks
ax1.set_xlabel('Snowcoverage')
ax1.set_ylabel('Deviation [m/s]')
#ax1.set_xticklabels([5, 6, 7, 8, 9, 10])
ax1.grid()
ax1.set_title('Deviation of GPS Velocity during movement - AvaNode vs. RTK')
#fig1.savefig('Kühtai_Deviation_Velocity.png', dpi=300)
