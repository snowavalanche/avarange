# -*- coding: utf-8 -*-
"""
Created on Mon Jun  7 16:20:27 2021

@author: michael neuhauser
"""

# Imports
import sys 
import os
import numpy as np
from collections import Counter
import matplotlib.pyplot as plt

from pyqtgraph import PlotWidget
import pyqtgraph as pg

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QFileDialog

from AvaRange_DataViewer import Ui_MainWindow
from classes.AvaNode_GNSS_Class import AvaNode_GNSS
from classes.AvaNode_IMU_Class import AvaNode_IMU


class DataViewer_EXEC():
    
    def __init__(self):    
        # Create Application
        app = QtWidgets.QApplication(sys.argv) 
        MainWindow = QtWidgets.QMainWindow()
        self.MainWindow = MainWindow
        self.ui = Ui_MainWindow()
        self.ui.setupUi(MainWindow)
        self.MainWindow.showMaximized()
        
        # setup GUI
        # Add Menu Triggers
        self.ui.actionAmoled.triggered.connect(lambda: self.change_color("amoled"))
        self.ui.actionAqua.triggered.connect(lambda: self.change_color("aqua"))
        self.ui.actionConsole.triggered.connect(lambda: self.change_color("console"))
        self.ui.actionDarkOrange_2.triggered.connect(lambda: self.change_color("darkorange"))
        self.ui.actionElegantDark.triggered.connect(lambda: self.change_color("elegantdark"))
        self.ui.actionManjaro.triggered.connect(lambda: self.change_color("manjaro"))
        self.ui.actionMaterialDark.triggered.connect(lambda: self.change_color("materialdark"))
        self.ui.actionUbuntu.triggered.connect(lambda: self.change_color("ubuntu"))
        # set color scheme
        # DarkOrange
        self.is_dark = True
        self.color = '#ffa500'
        self.backgroundcolor = '#323232'
        #GNSS Tab
        self.pw_gps = PlotWidget()
        self.pw_gps_horiz = PlotWidget()
        self.pw_gps_velocity = PlotWidget()
        self.setup_gps_tab()
        # IMU Tab
        self.pw_imu_acc = PlotWidget()
        self.pw_imu_gyro = PlotWidget()
        self.pw_imu_mag = PlotWidget()
        self.setup_imu_tab()
        # Sync Tab ... if neccesary 
        #self.setup_sync_tab() 
        # Temperature Tab
        self.pw_temp = PlotWidget()
        self.setup_temp_tab()
        
        # call in functions
        self.work_dir = os.getcwd()

        #setup colors
        self.pen_yellow = pg.mkPen(color=(255,165,0, 255), width=1)
        
        #setup data storage and variables
        self.gps = AvaNode_GNSS()
        self.imu = AvaNode_IMU()

        self.is_gps = False
        self.is_imu = False
        self.is_cali = False
        self.is_calc = False
        
        # Connections to open File
        self.ui.actionOpen.triggered.connect(self.open_file)
        self.ui.gps_open_Button.clicked.connect(self.open_gnss_file)
        self.ui.imu_open_Button.clicked.connect(self.open_imu_file)
        # Sliders
        self.ui.gps_fslider.sliderMoved.connect(self.gps_fslider_moved)
        self.ui.gps_rslider.sliderMoved.connect(self.gps_rslider_moved)
        self.ui.gps_fslider.sliderReleased.connect(self.gps_fslider_released)
        self.ui.gps_rslider.sliderReleased.connect(self.gps_rslider_released)
        self.ui.gps_fslider.valueChanged.connect(self.gps_fslider_valueChanged)
        self.ui.gps_rslider.valueChanged.connect(self.gps_rslider_valueChanged)
        self.ui.gps_front_index_box.valueChanged.connect(self.update_front_time)
        self.ui.gps_end_index_box.valueChanged.connect(self.update_end_time)
        # Connections IMU Page
        self.ui.imu_fslider.sliderReleased.connect(self.imu_fslider_moved)
        self.ui.imu_rslider.sliderReleased.connect(self.imu_rslider_moved)
        self.ui.imu_fslider.sliderReleased.connect(self.imu_fslider_released)
        self.ui.imu_rslider.sliderReleased.connect(self.imu_rslider_released)
        self.ui.imu_fslider.valueChanged.connect(self.imu_fslider_valueChanged)
        self.ui.imu_rslider.valueChanged.connect(self.imu_rslider_valueChanged)

        # Connections Sync Page
        self.ui.syncButton.clicked.connect(self.sync)
        self.ui.saveButton.clicked.connect(self.save)



        # show the constructed window
        MainWindow.show()
        sys.exit(app.exec_())
        
    # Change Color funtions
    def change_color(self, color:str):
        # Amoled
        if color == 'amoled':
            with open(r'.\resources\QSS-master\Amoled.qss', 'r', encoding='utf-8') as file:
                stylesheet = file.read()
            self.backgroundcolor = '#000000'
            self.color = '#a9b7c6'
        # Aqua
        if color == 'aqua':
            with open(r'.\resources\QSS-master\Aqua.qss', 'r', encoding='utf-8') as file:
                stylesheet = file.read()
                self.backgroundcolor = '#ececec'
                self.color = '#000000'
         # Console
        if color == 'console':
             with open(r'.\resources\QSS-master\ConsoleStyle.qss', 'r', encoding='utf-8') as file:
                 stylesheet = file.read()
                 self.backgroundcolor = '#000000'
                 self.color = '#ececec'
        # DarkOrange
        if color == 'darkorange':
            with open(r'.\resources\QSS-master\DarkOrange.qss', 'r', encoding='utf-8') as file:
                stylesheet = file.read()  
                self.backgroundcolor = '#323232'
                self.color = '#ffa500'
        # ElegantDark
        if color == 'elegantdark':
            with open(r'.\resources\QSS-master\ElegantDark.qss', 'r', encoding='utf-8') as file:
                stylesheet = file.read()  
                self.backgroundcolor = '#525252'
                self.color = '#000000'
        # ElegantDark
        if color == 'manjaro':
            with open(r'.\resources\QSS-master\ManjaroMix.qss', 'r', encoding='utf-8') as file:
                stylesheet = file.read()  
                self.backgroundcolor = '#151a1e'
                self.color = '#d3dae3'
        # ElegantDark
        if color == 'materialdark':
            with open(r'.\resources\QSS-master\MaterialDark.qss', 'r', encoding='utf-8') as file:
                stylesheet = file.read()  
                self.backgroundcolor = '#1e1d23'
                self.color = '#a9b7c6'
        # ElegantDark
        if color == 'ubuntu':
            with open(r'.\resources\QSS-master\Ubuntu.qss', 'r', encoding='utf-8') as file:
                stylesheet = file.read()  
                self.backgroundcolor = '#f0f0f0'
                self.color = '#514841'
        # Set StyleSheet to MainWindow
        self.MainWindow.setStyleSheet(stylesheet)
        # Update PlotWidgets
        # GNSS PlotWidget
        self.setup_gps_plotwidgets()
        # IMU PLotWidget
        self.setup_imu_plotwidgets()
        # Temperature Plotwidgets
        self.setup_temp_plotwidgets()
        
    # Functions for Tab setup

    def setup_gps_tab(self):

        self.ui.gps_h1.addWidget(self.pw_gps, 0)
        self.ui.gps_h1.addWidget(self.pw_gps_velocity, 1)
        self.ui.gps_h1.addWidget(self.pw_gps_horiz, 2)      
        self.setup_gps_plotwidgets()
        
    def setup_gps_plotwidgets(self):
        
        backgroundcolor = self.backgroundcolor
        color_title = self.color
        
        self.pw_gps.setBackground(backgroundcolor)
        self.pw_gps_velocity.setBackground(backgroundcolor)
        self.pw_gps_horiz.setBackground(backgroundcolor)
        
        self.pw_gps.setTitle('Altitude', color=color_title)
        self.pw_gps.setLabel('bottom', 'Index', color=color_title)
        self.pw_gps.setLabel('left', 'Altitude [m]', color=color_title)

        self.pw_gps_velocity.setTitle('Velocity', color=color_title)
        self.pw_gps_velocity.setLabel('bottom', 'Index', color=color_title)
        self.pw_gps_velocity.setLabel('left', 'Velocity [m/s]', color=color_title)

        self.pw_gps_horiz.setTitle('Position', color=color_title)
        self.pw_gps_horiz.setLabel('bottom', 'Long', color=color_title)
        self.pw_gps_horiz.setLabel('left', 'Lat', color=color_title)
          
    def setup_imu_tab(self):

        self.ui.imu_acc_Layout.addWidget(self.pw_imu_acc, 0)
        self.ui.imu_gyro_Layout.addWidget(self.pw_imu_gyro, 1)
        self.ui.imu_mag_Layout.addWidget(self.pw_imu_mag, 2)
        
        self.setup_imu_plotwidgets()
             
    def setup_imu_plotwidgets(self):
        
        backgroundcolor = self.backgroundcolor
        color_title = self.color
        
        self.pw_imu_acc.setBackground(backgroundcolor)
        self.pw_imu_gyro.setBackground(backgroundcolor)
        self.pw_imu_mag.setBackground(backgroundcolor)
        
        self.pw_imu_acc.setTitle('Accerlerometer', color=color_title)
        self.pw_imu_acc.setLabel('bottom', 'Index', color=color_title)
        self.pw_imu_acc.setLabel('left', 'Acceleratons [m/s²]', color=color_title)
        
        self.pw_imu_gyro.setTitle('Gyrometer', color=color_title)
        self.pw_imu_gyro.setLabel('bottom', 'Index', color=color_title)
        self.pw_imu_gyro.setLabel('left', 'Rotation rates [°/s]', color=color_title)
        
        self.pw_imu_mag.setTitle('Magnetometer', color=color_title)
        self.pw_imu_mag.setLabel('bottom', 'Index', color=color_title)
        self.pw_imu_mag.setLabel('left', 'Magnetic Flux Density [µT]', color=color_title)
        
        
        
    def setup_temp_tab(self):
        self.ui.temp_Layout.addWidget(self.pw_temp, 0)
        self.setup_temp_plotwidgets()
        
    def setup_temp_plotwidgets(self):
        
        backgroundcolor = self.backgroundcolor
        color_title = self.color
        self.pw_temp.setBackground(backgroundcolor)
        
        self.pw_temp.setTitle('Temperature', color=color_title)
        self.pw_temp.setLabel('bottom', 'Index', color=color_title)
        self.pw_temp.setLabel('left', 'Temp. [°C]', color=color_title)

    # Functions to open files
    def open_gnss_file(self):
        try:
            path = QFileDialog.getOpenFileName(None, 'Open AvaNode GPS Data',
                                                self.work_dir,
                                               "txt (*.txt);;csv (*.csv);;All Files (*.*)")[0]

            self.gps = AvaNode_GNSS()
            self.work_dir = os.path.split(os.path.split(path)[0])[0]
            self.gps.work_dir = os.path.split(path)[0]
            self.gps.path = path
            self.gps.read_data()
            print("GNSS Data read")
            self.is_gps = True
            if self.gps.temp_bool:
                self.plot_temp()
            print("Temperature plotted")
            self.plot_velocity()
            print("GNSS Velocity plotted")
            self.plot_altitude()
            print("Altitude plotted")
            self.plot_gps()
            print("GNSS Data plotted")
            
            
            self.gps.sync_gps()
            print("GNSS Synced")
        except:
            print("Couldn't open GNSS File!")
        
    def open_imu_file(self):
        try:
            path = QFileDialog.getOpenFileName(None, 'Open AvaNode IMU Data',
                                                self.work_dir,
                                               "txt (*.txt);;All Files (*.*)")[0]
            self.work_dir = os.path.split(os.path.split(path)[0])[0]
            self.imu.work_dir = os.path.split(path)[0]
            self.imu.path = path
            self.imu.read_data_pd()
            print("Read IMU data.")
            self.plot_imu()
            print("Plotted IMU data.")
            self.is_imu = True
            self.sync_imu()
            print("Synchronized IMU data.")

        except:
            print("Couldn't open imu File!")


    def open_file(self):

        self.work_dir = QFileDialog.getExistingDirectory(None, 'Select Data Folder',
                                            self.work_dir,
                                           QtWidgets.QFileDialog.ShowDirsOnly)
        print(self.work_dir)
        self.gps.path = self.work_dir + '/GPS/'
        self.gps.read_data()
        self.plot_gps()
        self.gps.sync_gps()


    # Plot Functions

    def plot_imu(self):
        # Clear all plot widgets
        self.pw_imu_acc.clear()
        self.pw_imu_gyro.clear()
        self.pw_imu_mag.clear()
        # Setup pens for drawing
        width = 0.4
        alpha = 255
        xpen = pg.mkPen(color=(255, 0, 0, alpha), width=width)
        ypen = pg.mkPen(color=(0, 255, 0, alpha), width=width, alpha=alpha)
        zpen = pg.mkPen(color=(0, 255, 255, alpha), width=width, alpha=alpha)
        tpen = pg.mkPen(color=(255, 255, 255, alpha), width=width, alpha=alpha)
        # Acceleration
        self.pw_imu_acc.plot(self.imu.acc_X, pen=xpen, name='X')
        self.pw_imu_acc.plot(self.imu.acc_Y, pen=ypen, name='Y')
        self.pw_imu_acc.plot(self.imu.acc_Z, pen=zpen, name='Z')
        self.pw_imu_acc.plot(self.imu.acc_tot, pen=tpen, name='Total')
        self.pw_imu_acc.showGrid(x=True, y=True)
        self.pw_imu_acc.addLegend()
        # Gyrometer
        self.pw_imu_gyro.plot(self.imu.gyro_X, pen=xpen, name='X')
        self.pw_imu_gyro.plot(self.imu.gyro_Y, pen=ypen, name='Y')
        self.pw_imu_gyro.plot(self.imu.gyro_Z, pen=zpen, name='Z')
        self.pw_imu_gyro.showGrid(x=True, y=True)
        self.pw_imu_gyro.addLegend()
        # Magnetometer
        self.pw_imu_mag.plot(self.imu.mag_X, pen=xpen, name='X')
        self.pw_imu_mag.plot(self.imu.mag_Y, pen=ypen, name='Y')
        self.pw_imu_mag.plot(self.imu.mag_Z, pen=zpen, name='Z')
        self.pw_imu_mag.plot(self.imu.mag_tot, pen=tpen, name='Total')
        self.pw_imu_mag.showGrid(x=True, y=True)
        self.pw_imu_mag.addLegend()
        # Index Lines
        self.f_line_imu_acc = pg.InfiniteLine(0, 90, pen=self.pen_yellow)
        self.e_line_imu_acc = pg.InfiniteLine(len(self.imu.acc_X), 90, pen=self.pen_yellow)
        self.f_line_imu_gyro = pg.InfiniteLine(0, 90, pen=self.pen_yellow)
        self.e_line_imu_gyro = pg.InfiniteLine(len(self.imu.gyro_X), 90, pen=self.pen_yellow)
        self.f_line_imu_mag = pg.InfiniteLine(0, 90, pen=self.pen_yellow)
        self.e_line_imu_mag = pg.InfiniteLine(len(self.imu.mag_X), 90, pen=self.pen_yellow)
        self.pw_imu_acc.addItem(self.f_line_imu_acc)
        self.pw_imu_acc.addItem(self.e_line_imu_acc)
        self.pw_imu_gyro.addItem(self.f_line_imu_gyro)
        self.pw_imu_gyro.addItem(self.e_line_imu_gyro)
        self.pw_imu_mag.addItem(self.f_line_imu_mag)
        self.pw_imu_mag.addItem(self.e_line_imu_mag)
        # Setup Sliders
        self.ui.imu_fslider.setMaximum(len(self.imu.acc_X))
        self.ui.imu_rslider.setMaximum(len(self.imu.acc_X))
        self.ui.imu_rslider.setValue(len(self.imu.acc_X)-1)
    
    def plot_altitude(self):
        self.pw_gps.clear()
        alpha=255
        width=1
        alt_pen = pg.mkPen(color=(255, 0, 0, alpha), width=width)
        
        height = self.gps.data_pos['height[m]'].to_numpy()
        # Search for same iTow
        index = np.where(self.gps.data[self.gps.itow] == self.gps.data_pos[self.gps.itow].iloc[0])[0][0]
        indizes = np.linspace(index, index+len(height)-1, len(height))
        self.pw_gps.plot(indizes, height, pen=alt_pen)
        
        self.f_line = pg.InfiniteLine(index, 90, pen=self.pen_yellow)
        self.e_line = pg.InfiniteLine(len(height), 90, pen=self.pen_yellow)
     
        #Setup Sliders
        self.ui.gps_fslider.setMinimum(index)
        self.ui.gps_rslider.setMinimum(index)
        self.ui.gps_fslider.setMaximum(len(height)+index-1)
        self.ui.gps_rslider.setMaximum(len(height)+index-1)
        self.ui.gps_rslider.setValue(len(height))
        self.ui.gps_f_index.setMaximum(len(height)+index-1)
        self.ui.gps_f_index.setValue(index)
        self.f_line.setValue(index)
        self.ui.gps_r_index.setMaximum(len(height)+index-1)
        self.ui.gps_r_index.setValue(len(height)+index-1)
        self.e_line.setValue(len(height)+index-1)
        self.ui.gps_front_index_box.setMaximum(len(height)+index-1)
        self.ui.gps_end_index_box.setMaximum(len(height)+index-1)
        self.ui.gps_f_time.setText(self.gps.data_pos[self.gps.utc].iloc[0])
        self.ui.gps_r_time.setText(self.gps.data_pos[self.gps.utc].iloc[-1])
        
    def plot_gps(self):
        #Plot GPS Data
        # Clear old plots
        self.pw_gps_horiz.clear()
       
        # Setup variables
        lat = self.gps.data_pos['latitude[deg]'].to_numpy()
        long = self.gps.data_pos['longitude[deg]'].to_numpy()
        self.lat_index = [lat[0], lat[-1]]
        self.long_index = [long[0], long[-1]]
        #Setup brush
        symbolBrush = pg.mkBrush(0, 0, 255)
        symbolBrush_marked = pg.mkBrush(255, 0, 0)
        # Plot GPS Data      
        self.pw_gps_horiz.plot(long, lat, symbol='o', pen=None, symbolBrush=symbolBrush, symbolSize=7)
        # Plot Slider Positions and setup Sliders
        self.point = self.pw_gps_horiz.plot(self.long_index, self.lat_index, symbol='t', pen=None, symbolBrush=symbolBrush_marked, symbolSize=20)
        self.pw_gps.addItem(self.f_line)
        self.pw_gps.addItem(self.e_line)

    def plot_velocity(self):
        self.pw_gps_velocity.clear()
        
        alpha=255
        width=1
        vel_pen = pg.mkPen(color=(0, 255, 0, alpha), width=width)
        
        height = self.gps.data_pos['height[m]'].to_numpy()
        # Search for same iTow
        index = np.where(self.gps.data[self.gps.itow] == self.gps.data_pos[self.gps.itow].iloc[0])[0][0]
        indizes = np.linspace(index, index+len(height)-1, len(height))
        
        if self.gps.vel_bool:
            velocity = self.gps.data_pos["v_total"].to_numpy()
        else:
            self.gps.calc_pos_vel()
            velocity = self.gps.data_pos["v_total_pos"].to_numpy()
        print(type(velocity))
        #print(indizes)
        self.pw_gps_velocity.plot(indizes, velocity, pen=vel_pen)
        
        self.f_line_v = pg.InfiniteLine(index, 90, pen=self.pen_yellow)
        self.e_line_v = pg.InfiniteLine(len(height), 90, pen=self.pen_yellow)
        self.pw_gps_velocity.addItem(self.f_line_v)
        self.pw_gps_velocity.addItem(self.e_line_v)
        # Setup SpinBoxes        
        self.f_line_v.setValue(index)
        self.e_line_v.setValue(len(height)+index-1)
        
    def plot_temp(self):
        # Clear all plot widgets
        self.pw_temp.clear()
        self.pw_temp.addLegend()
        # Setup pens for drawing
        width = 1
        alpha = 255
        ambient_pen = pg.mkPen(color=(255, 255, 0, alpha), width=width)
        object_pen = pg.mkPen(color=(0, 255, 255, alpha), width=width, alpha=alpha)
        # Temperature
        self.pw_temp.plot(self.gps.data['ambientTemp[C]'], pen=ambient_pen, name='Ambient Temp.')
        self.pw_temp.plot(self.gps.data['objectTemp[C]'], pen=object_pen, name='Object Temp.')
        #self.pw_temp.plot(self.imu.acc_Y_cali, pen=ypen, name='Y')
        #self.pw_temp.plot(self.imu.acc_Z_cali, pen=zpen, name='Z')
        #self.pw_temp.plot(self.imu.acc_tot, pen=tpen, name='Total')
        self.pw_temp.showGrid(x=True, y=True)
        
        
    # Slider Handling
    ## GNSS

    def gps_fslider_moved(self):
        value = self.ui.gps_fslider.value()
        self.ui.gps_rslider.setMinimum(value)
        if self.is_gps:
            self.f_line.setValue(value)
            self.f_line_v.setValue(value)
            self.point.clear()
            self.lat_index[0] = self.gps.data['latitude[deg]'].iloc[value]
            self.long_index[0] = self.gps.data['longitude[deg]'].iloc[value]
            self.point.setData(self.long_index, self.lat_index)

    def gps_rslider_moved(self):
        value = self.ui.gps_rslider.value()
        self.ui.gps_fslider.setMaximum(value)
        if self.is_gps:
            self.e_line.setValue(value)
            self.e_line_v.setValue(value)
            self.point.clear()
            self.lat_index[1] = self.gps.data['latitude[deg]'].iloc[value]
            self.long_index[1] = self.gps.data['longitude[deg]'].iloc[value]
            self.point.setData(self.long_index, self.lat_index)
        
    def gps_fslider_released(self):
        value = self.ui.gps_fslider.value()
        self.ui.gps_front_index_box.setValue(int(value))
        if self.is_gps:
            self.ui.gps_f_time.setText(self.gps.data[self.gps.utc].iloc[value])
        if self.is_imu and not value == 0:
            imu_value = self.sync_imu_index(int(value))
            self.ui.imu_fslider.setValue(imu_value)

    def gps_rslider_released(self):
        value = self.ui.gps_rslider.value()
        self.ui.gps_end_index_box.setValue(int(value))
        if self.is_gps:
            self.ui.gps_r_time.setText(self.gps.data[self.gps.utc].iloc[value-1])
        if self.is_imu and not value == 0:
            imu_value = self.sync_imu_index(int(value))
            self.ui.imu_rslider.setValue(imu_value)

    def gps_fslider_valueChanged(self):
        value = self.ui.gps_fslider.value()
        self.ui.gps_rslider.setMinimum(value)
        self.ui.gps_front_index_box.setValue(int(value))
        if self.is_gps:
            self.f_line.setValue(value)
            self.f_line_v.setValue(value)
            self.point.clear()
            self.lat_index[0] = self.gps.data['latitude[deg]'].iloc[value]
            self.long_index[0] = self.gps.data['longitude[deg]'].iloc[value]
            self.point.setData(self.long_index, self.lat_index)

    def gps_rslider_valueChanged(self):
        value = self.ui.gps_rslider.value()
        self.ui.gps_fslider.setMaximum(value)
        self.ui.gps_end_index_box.setValue(int(value))
        if self.is_gps:
            self.e_line.setValue(value)
            self.e_line_v.setValue(value)
            self.point.clear()
            self.lat_index[1] = self.gps.data['latitude[deg]'].iloc[value]
            self.long_index[1] = self.gps.data['longitude[deg]'].iloc[value]
            self.point.setData(self.long_index, self.lat_index)
            
    ## imu

    def imu_fslider_moved(self):
        value = self.ui.imu_fslider.value()
        self.ui.imu_rslider.setMinimum(value)
        if self.is_imu:
            self.f_line_imu_acc.setValue(value)
            self.f_line_imu_gyro.setValue(value)
            self.f_line_imu_mag.setValue(value)

    def imu_rslider_moved(self):
        value = self.ui.imu_rslider.value()
        self.ui.imu_fslider.setMaximum(value)
        if self.is_imu:
            self.e_line_imu_acc.setValue(value)
            self.e_line_imu_gyro.setValue(value)
            self.e_line_imu_mag.setValue(value)

    def imu_fslider_released(self):
        value = self.ui.imu_fslider.value()
        if self.is_gps:
            gps_value = self.sync_gps_index(value, 'imu')
            self.ui.gps_fslider.setValue(gps_value)

    def imu_rslider_released(self):
        value = self.ui.imu_rslider.value()
        if self.is_gps:
            gps_value = self.sync_gps_index(value, 'imu')
            self.ui.gps_rslider.setValue(gps_value)

    def imu_fslider_valueChanged(self):
        value = self.ui.imu_fslider.value()
        self.ui.imu_rslider.setMinimum(value)
        if self.is_imu:
            self.f_line_imu_acc.setValue(value)
            self.f_line_imu_gyro.setValue(value)
            self.f_line_imu_mag.setValue(value)

    def imu_rslider_valueChanged(self):
        value = self.ui.imu_rslider.value()
        self.ui.imu_fslider.setMaximum(value)
        if self.is_imu:
            self.e_line_imu_acc.setValue(value)
            self.e_line_imu_gyro.setValue(value)
            self.e_line_imu_mag.setValue(value)
        
    # Sync Functions
    def sync_imu(self):
        print("Syncing the Signals...")
        if self.is_imu:
            startvalue = self.imu.sync[0]
            imu_peaks = 0
            imu_sync_counter = []

            counter_even = 0
            counter_odd = 1

            for value in self.imu.sync:
                if value == startvalue:
                    if value == 0:
                        imu_sync_counter.append(counter_even)
                    if value == 1:
                        imu_sync_counter.append(counter_odd)
                if value != startvalue:
                    imu_peaks += 1
                    if value == 0:
                        counter_even += 2
                        imu_sync_counter.append(counter_even)
                    if value == 1:
                        if imu_peaks > 1:
                            counter_odd += 2
                        imu_sync_counter.append(counter_odd)
                    startvalue = value
            self.imu.sync_counter = imu_sync_counter
            self.imu.is_synced = True
            print("imu has {} peaks!".format(imu_peaks))
            print("imu is synced.")

    def sync_gps(self):
        if self.is_gps:
            startvalue = self.gps.sync[0]
            gps_peaks = 0
            gps_sync_counter = []

            counter_even = 0
            counter_odd = 1

            for value in self.gps.sync:
                if value == startvalue:
                    if value == 0:
                        gps_sync_counter.append(counter_even)
                    if value == 1:
                        gps_sync_counter.append(counter_odd)
                if value != startvalue:
                    gps_peaks += 1
                    if value == 0:
                        counter_even += 2
                        gps_sync_counter.append(counter_even)
                    if value == 1:
                        if gps_peaks > 1:
                            counter_odd += 2
                        gps_sync_counter.append(counter_odd)
                    startvalue = value
            self.gps.sync_counter = gps_sync_counter
            self.gps.is_synced = True
            print("GPS has {} peaks!".format(gps_peaks))

    def sync_imu_index(self, index_gps):
        if self.is_gps:
            gps_counter_value = self.gps.sync_counter[index_gps]
            gps_counter_range = np.where(np.array(self.gps.sync_counter, copy=False) == gps_counter_value)[0]
        if self.is_imu and self.is_gps:
            imu_counter_range = np.where(np.array(self.imu.sync_counter, copy=False) == gps_counter_value)[0]
            imu_index = int(imu_counter_range[0] + ((len(imu_counter_range)/len(gps_counter_range)) * (index_gps - gps_counter_range[0])))
            #Plot sync Counters
            x = Counter(self.imu.sync_counter)
            x_sorted = sorted(x.items())
            y = Counter(self.gps.sync_counter)
            y_sorted = sorted(y.items())
            
            fig, ax = plt.subplots()
            ax.plot(*zip(*x_sorted[:-1]))
            ax.set_title("IMU Sync Counts")
            #fig.savefig(self.work_dir + "imu_sync_counts.png", dpi=300)
            
            fig1, ax1 = plt.subplots()
            ax1.plot(*zip(*y_sorted[1:-1]))
            ax1.set_title("GPS Sync Counts")
            #fig1.savefig(self.work_dir + "gps_sync_counts.png", dpi=300)
            
            return imu_index

    def sync_gps_index(self, index_imu:int, imu_string:str):
        if imu_string == 'imu':
            if self.is_imu:
                imu_counter_value = self.imu.sync_counter[index_imu]
                imu_counter_range = np.where(np.array(self.imu.sync_counter, copy=False) == imu_counter_value)[0]
            if self.is_gps and self.is_imu:
                gps_counter_range = np.where(np.array(self.gps.sync_counter, copy=False) == imu_counter_value)[0]
                gps_index = int(gps_counter_range[0] + len(gps_counter_range)/len(imu_counter_range) * (index_imu - imu_counter_range[0]+1))
                return gps_index

    def sync(self):
        front_gps = int(self.ui.gps_front_index_box.text())
        end_gps = int(self.ui.gps_end_index_box.text())
        if self.imu.is_synced:
            front_imu = self.sync_imu_index(front_gps)
            end_imu = self.sync_imu_index(end_gps)
            self.ui.imu_front.setText(str(front_imu))
            self.ui.imu_end.setText(str(end_imu))

    def update_front_time(self):
        try:
            value = self.ui.gps_front_index_box.value()
            if self.is_gps:
                time = self.gps.data[self.gps.utc].iloc[value]
                self.ui.gps_front_time.setText(time)
        except:
            print("...")

    def update_end_time(self):
        try:
            value = self.ui.gps_end_index_box.value()
            if self.is_gps:
                time = self.gps.data[self.gps.utc].iloc[value]
                self.ui.gps_end_time.setText(time)
        except:
            print("...")


    def save(self):
        name = self.ui.saveEdit.text()
        if len(name) == 0:
            name = 'Name'
        if self.is_gps:
            gps_start = self.ui.gps_front_index_box.value()
            gps_end = self.ui.gps_end_index_box.value()
            gps_data = self.gps.data.iloc[gps_start:gps_end, :]
            gps_data.to_csv(self.gps.work_dir + '/' + name + '_gnss.txt', index=False)
            print("Saved GNSS File!")
        if self.is_imu:
            imu_start = int(self.ui.imu_front.text())
            imu_end = int(self.ui.imu_end.text())
            imu_data = self.imu.data.iloc[imu_start:imu_end, :].astype(int)
            np.savetxt(self.imu.work_dir + '/' + name + '_imu.txt', imu_data, header=self.imu.header, fmt='%f', delimiter=',')
            print("Saved IMU File!")
        

if __name__ == '__main__':   
        DataViewer_EXEC()