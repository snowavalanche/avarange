# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'AvaRange_DataViewer.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(2363, 1522)
        MainWindow.setStyleSheet("QToolTip\n"
"{\n"
"     border: 1px solid black;\n"
"     background-color: #ffa02f;\n"
"     padding: 1px;\n"
"     border-radius: 3px;\n"
"     opacity: 100;\n"
"}\n"
"\n"
"QWidget\n"
"{\n"
"    color: #b1b1b1;\n"
"    background-color: #323232;\n"
"}\n"
"\n"
"QWidget:item:hover\n"
"{\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #ca0619);\n"
"    color: #000000;\n"
"}\n"
"\n"
"QWidget:item:selected\n"
"{\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"}\n"
"\n"
"QMenuBar::item\n"
"{\n"
"    background: transparent;\n"
"}\n"
"\n"
"QMenuBar::item:selected\n"
"{\n"
"    background: transparent;\n"
"    border: 1px solid #ffaa00;\n"
"}\n"
"\n"
"QMenuBar::item:pressed\n"
"{\n"
"    background: #444;\n"
"    border: 1px solid #000;\n"
"    background-color: QLinearGradient(\n"
"        x1:0, y1:0,\n"
"        x2:0, y2:1,\n"
"        stop:1 #212121,\n"
"        stop:0.4 #343434/*,\n"
"        stop:0.2 #343434,\n"
"        stop:0.1 #ffaa00*/\n"
"    );\n"
"    margin-bottom:-1px;\n"
"    padding-bottom:1px;\n"
"}\n"
"\n"
"QMenu\n"
"{\n"
"    border: 1px solid #000;\n"
"}\n"
"\n"
"QMenu::item\n"
"{\n"
"    padding: 2px 20px 2px 20px;\n"
"}\n"
"\n"
"QMenu::item:selected\n"
"{\n"
"    color: #000000;\n"
"}\n"
"\n"
"QWidget:disabled\n"
"{\n"
"    color: #404040;\n"
"    background-color: #323232;\n"
"}\n"
"\n"
"QAbstractItemView\n"
"{\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #4d4d4d, stop: 0.1 #646464, stop: 1 #5d5d5d);\n"
"}\n"
"\n"
"QWidget:focus\n"
"{\n"
"    /*border: 2px solid QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);*/\n"
"}\n"
"\n"
"QLineEdit\n"
"{\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #4d4d4d, stop: 0 #646464, stop: 1 #5d5d5d);\n"
"    padding: 1px;\n"
"    border-style: solid;\n"
"    border: 1px solid #1e1e1e;\n"
"    border-radius: 5;\n"
"}\n"
"\n"
"QPushButton\n"
"{\n"
"    color: #b1b1b1;\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"    border-width: 1px;\n"
"    border-color: #1e1e1e;\n"
"    border-style: solid;\n"
"    border-radius: 6;\n"
"    padding: 3px;\n"
"    font-size: 12px;\n"
"    padding-left: 5px;\n"
"    padding-right: 5px;\n"
"}\n"
"\n"
"QPushButton:pressed\n"
"{\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #2d2d2d, stop: 0.1 #2b2b2b, stop: 0.5 #292929, stop: 0.9 #282828, stop: 1 #252525);\n"
"}\n"
"\n"
"QComboBox\n"
"{\n"
"    selection-background-color: #ffaa00;\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"    border-style: solid;\n"
"    border: 1px solid #1e1e1e;\n"
"    border-radius: 5;\n"
"}\n"
"\n"
"QComboBox:hover,QPushButton:hover\n"
"{\n"
"    border: 2px solid QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"}\n"
"\n"
"\n"
"QComboBox:on\n"
"{\n"
"    padding-top: 3px;\n"
"    padding-left: 4px;\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #2d2d2d, stop: 0.1 #2b2b2b, stop: 0.5 #292929, stop: 0.9 #282828, stop: 1 #252525);\n"
"    selection-background-color: #ffaa00;\n"
"}\n"
"\n"
"QComboBox QAbstractItemView\n"
"{\n"
"    border: 2px solid darkgray;\n"
"    selection-background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"}\n"
"\n"
"QComboBox::drop-down\n"
"{\n"
"     subcontrol-origin: padding;\n"
"     subcontrol-position: top right;\n"
"     width: 15px;\n"
"\n"
"     border-left-width: 0px;\n"
"     border-left-color: darkgray;\n"
"     border-left-style: solid; /* just a single line */\n"
"     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
"     border-bottom-right-radius: 3px;\n"
" }\n"
"\n"
"QComboBox::down-arrow\n"
"{\n"
"     image: url(:/down_arrow.png);\n"
"}\n"
"\n"
"QGroupBox:focus\n"
"{\n"
"border: 2px solid QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"}\n"
"\n"
"QTextEdit:focus\n"
"{\n"
"    border: 2px solid QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"}\n"
"\n"
"QScrollBar:horizontal {\n"
"     border: 1px solid #222222;\n"
"     background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0.0 #121212, stop: 0.2 #282828, stop: 1 #484848);\n"
"     height: 7px;\n"
"     margin: 0px 16px 0 16px;\n"
"}\n"
"\n"
"QScrollBar::handle:horizontal\n"
"{\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #ffa02f, stop: 0.5 #d7801a, stop: 1 #ffa02f);\n"
"      min-height: 20px;\n"
"      border-radius: 2px;\n"
"}\n"
"\n"
"QScrollBar::add-line:horizontal {\n"
"      border: 1px solid #1b1b19;\n"
"      border-radius: 2px;\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"      width: 14px;\n"
"      subcontrol-position: right;\n"
"      subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::sub-line:horizontal {\n"
"      border: 1px solid #1b1b19;\n"
"      border-radius: 2px;\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"      width: 14px;\n"
"     subcontrol-position: left;\n"
"     subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::right-arrow:horizontal, QScrollBar::left-arrow:horizontal\n"
"{\n"
"      border: 1px solid black;\n"
"      width: 1px;\n"
"      height: 1px;\n"
"      background: white;\n"
"}\n"
"\n"
"QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal\n"
"{\n"
"      background: none;\n"
"}\n"
"\n"
"QScrollBar:vertical\n"
"{\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0.0 #121212, stop: 0.2 #282828, stop: 1 #484848);\n"
"      width: 7px;\n"
"      margin: 16px 0 16px 0;\n"
"      border: 1px solid #222222;\n"
"}\n"
"\n"
"QScrollBar::handle:vertical\n"
"{\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 0.5 #d7801a, stop: 1 #ffa02f);\n"
"      min-height: 20px;\n"
"      border-radius: 2px;\n"
"}\n"
"\n"
"QScrollBar::add-line:vertical\n"
"{\n"
"      border: 1px solid #1b1b19;\n"
"      border-radius: 2px;\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"      height: 14px;\n"
"      subcontrol-position: bottom;\n"
"      subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::sub-line:vertical\n"
"{\n"
"      border: 1px solid #1b1b19;\n"
"      border-radius: 2px;\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #d7801a, stop: 1 #ffa02f);\n"
"      height: 14px;\n"
"      subcontrol-position: top;\n"
"      subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::up-arrow:vertical, QScrollBar::down-arrow:vertical\n"
"{\n"
"      border: 1px solid black;\n"
"      width: 1px;\n"
"      height: 1px;\n"
"      background: white;\n"
"}\n"
"\n"
"\n"
"QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical\n"
"{\n"
"      background: none;\n"
"}\n"
"\n"
"QTextEdit\n"
"{\n"
"    background-color: #242424;\n"
"}\n"
"\n"
"QPlainTextEdit\n"
"{\n"
"    background-color: #242424;\n"
"}\n"
"\n"
"QHeaderView::section\n"
"{\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #616161, stop: 0.5 #505050, stop: 0.6 #434343, stop:1 #656565);\n"
"    color: white;\n"
"    padding-left: 4px;\n"
"    border: 1px solid #6c6c6c;\n"
"}\n"
"\n"
"QCheckBox:disabled\n"
"{\n"
"color: #414141;\n"
"}\n"
"\n"
"QDockWidget::title\n"
"{\n"
"    text-align: center;\n"
"    spacing: 3px; /* spacing between items in the tool bar */\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #323232, stop: 0.5 #242424, stop:1 #323232);\n"
"}\n"
"\n"
"QDockWidget::close-button, QDockWidget::float-button\n"
"{\n"
"    text-align: center;\n"
"    spacing: 1px; /* spacing between items in the tool bar */\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #323232, stop: 0.5 #242424, stop:1 #323232);\n"
"}\n"
"\n"
"QDockWidget::close-button:hover, QDockWidget::float-button:hover\n"
"{\n"
"    background: #242424;\n"
"}\n"
"\n"
"QDockWidget::close-button:pressed, QDockWidget::float-button:pressed\n"
"{\n"
"    padding: 1px -1px -1px 1px;\n"
"}\n"
"\n"
"QMainWindow::separator\n"
"{\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #161616, stop: 0.5 #151515, stop: 0.6 #212121, stop:1 #343434);\n"
"    color: white;\n"
"    padding-left: 4px;\n"
"    border: 1px solid #4c4c4c;\n"
"    spacing: 3px; /* spacing between items in the tool bar */\n"
"}\n"
"\n"
"QMainWindow::separator:hover\n"
"{\n"
"\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #d7801a, stop:0.5 #b56c17 stop:1 #ffa02f);\n"
"    color: white;\n"
"    padding-left: 4px;\n"
"    border: 1px solid #6c6c6c;\n"
"    spacing: 3px; /* spacing between items in the tool bar */\n"
"}\n"
"\n"
"QToolBar::handle\n"
"{\n"
"     spacing: 3px; /* spacing between items in the tool bar */\n"
"     background: url(:/images/handle.png);\n"
"}\n"
"\n"
"QMenu::separator\n"
"{\n"
"    height: 2px;\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #161616, stop: 0.5 #151515, stop: 0.6 #212121, stop:1 #343434);\n"
"    color: white;\n"
"    padding-left: 4px;\n"
"    margin-left: 10px;\n"
"    margin-right: 5px;\n"
"}\n"
"\n"
"QProgressBar\n"
"{\n"
"    border: 2px solid grey;\n"
"    border-radius: 5px;\n"
"    text-align: center;\n"
"}\n"
"\n"
"QProgressBar::chunk\n"
"{\n"
"    background-color: #d7801a;\n"
"    width: 2.15px;\n"
"    margin: 0.5px;\n"
"}\n"
"\n"
"QTabBar::tab {\n"
"    color: #b1b1b1;\n"
"    border: 1px solid #444;\n"
"    border-bottom-style: none;\n"
"    background-color: #323232;\n"
"    padding-left: 10px;\n"
"    padding-right: 10px;\n"
"    padding-top: 3px;\n"
"    padding-bottom: 2px;\n"
"    margin-right: -1px;\n"
"}\n"
"\n"
"QTabWidget::pane {\n"
"    border: 1px solid #444;\n"
"    top: 1px;\n"
"}\n"
"\n"
"QTabBar::tab:last\n"
"{\n"
"    margin-right: 0; /* the last selected tab has nothing to overlap with on the right */\n"
"    border-top-right-radius: 3px;\n"
"}\n"
"\n"
"QTabBar::tab:first:!selected\n"
"{\n"
" margin-left: 0px; /* the last selected tab has nothing to overlap with on the right */\n"
"\n"
"\n"
"    border-top-left-radius: 3px;\n"
"}\n"
"\n"
"QTabBar::tab:!selected\n"
"{\n"
"    color: #b1b1b1;\n"
"    border-bottom-style: solid;\n"
"    margin-top: 3px;\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:1 #212121, stop:.4 #343434);\n"
"}\n"
"\n"
"QTabBar::tab:selected\n"
"{\n"
"    border-top-left-radius: 3px;\n"
"    border-top-right-radius: 3px;\n"
"    margin-bottom: 0px;\n"
"}\n"
"\n"
"QTabBar::tab:!selected:hover\n"
"{\n"
"    /*border-top: 2px solid #ffaa00;\n"
"    padding-bottom: 3px;*/\n"
"    border-top-left-radius: 3px;\n"
"    border-top-right-radius: 3px;\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:1 #212121, stop:0.4 #343434, stop:0.2 #343434, stop:0.1 #ffaa00);\n"
"}\n"
"\n"
"QRadioButton::indicator:checked, QRadioButton::indicator:unchecked{\n"
"    color: #b1b1b1;\n"
"    background-color: #323232;\n"
"    border: 1px solid #b1b1b1;\n"
"    border-radius: 6px;\n"
"}\n"
"\n"
"QRadioButton::indicator:checked\n"
"{\n"
"    background-color: qradialgradient(\n"
"        cx: 0.5, cy: 0.5,\n"
"        fx: 0.5, fy: 0.5,\n"
"        radius: 1.0,\n"
"        stop: 0.25 #ffaa00,\n"
"        stop: 0.3 #323232\n"
"    );\n"
"}\n"
"\n"
"QCheckBox::indicator{\n"
"    color: #b1b1b1;\n"
"    background-color: #323232;\n"
"    border: 1px solid #b1b1b1;\n"
"    width: 9px;\n"
"    height: 9px;\n"
"}\n"
"\n"
"QRadioButton::indicator\n"
"{\n"
"    border-radius: 6px;\n"
"}\n"
"\n"
"QRadioButton::indicator:hover, QCheckBox::indicator:hover\n"
"{\n"
"    border: 1px solid #ffaa00;\n"
"}\n"
"\n"
"QCheckBox::indicator:checked\n"
"{\n"
"    image:url(:/images/checkbox.png);\n"
"}\n"
"\n"
"QCheckBox::indicator:disabled, QRadioButton::indicator:disabled\n"
"{\n"
"    border: 1px solid #444;\n"
"}")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName("tabWidget")
        self.GNSS = QtWidgets.QWidget()
        self.GNSS.setObjectName("GNSS")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.GNSS)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.gps_open_Button = QtWidgets.QPushButton(self.GNSS)
        self.gps_open_Button.setObjectName("gps_open_Button")
        self.horizontalLayout_2.addWidget(self.gps_open_Button)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.gps_h1 = QtWidgets.QHBoxLayout()
        self.gps_h1.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.gps_h1.setObjectName("gps_h1")
        self.verticalLayout.addLayout(self.gps_h1)
        self.gps_h2 = QtWidgets.QHBoxLayout()
        self.gps_h2.setSizeConstraint(QtWidgets.QLayout.SetMinimumSize)
        self.gps_h2.setObjectName("gps_h2")
        self.label = QtWidgets.QLabel(self.GNSS)
        self.label.setObjectName("label")
        self.gps_h2.addWidget(self.label)
        self.gps_fslider = QtWidgets.QSlider(self.GNSS)
        self.gps_fslider.setOrientation(QtCore.Qt.Horizontal)
        self.gps_fslider.setObjectName("gps_fslider")
        self.gps_h2.addWidget(self.gps_fslider)
        self.label_2 = QtWidgets.QLabel(self.GNSS)
        self.label_2.setObjectName("label_2")
        self.gps_h2.addWidget(self.label_2)
        self.gps_rslider = QtWidgets.QSlider(self.GNSS)
        self.gps_rslider.setMaximum(99)
        self.gps_rslider.setSliderPosition(99)
        self.gps_rslider.setOrientation(QtCore.Qt.Horizontal)
        self.gps_rslider.setObjectName("gps_rslider")
        self.gps_h2.addWidget(self.gps_rslider)
        self.verticalLayout.addLayout(self.gps_h2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_3 = QtWidgets.QLabel(self.GNSS)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_3.addWidget(self.label_3)
        self.gps_f_index = QtWidgets.QSpinBox(self.GNSS)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.gps_f_index.sizePolicy().hasHeightForWidth())
        self.gps_f_index.setSizePolicy(sizePolicy)
        self.gps_f_index.setMinimumSize(QtCore.QSize(38, 0))
        self.gps_f_index.setObjectName("gps_f_index")
        self.horizontalLayout_3.addWidget(self.gps_f_index)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem1)
        self.label_4 = QtWidgets.QLabel(self.GNSS)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_3.addWidget(self.label_4)
        self.gps_r_index = QtWidgets.QSpinBox(self.GNSS)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.gps_r_index.sizePolicy().hasHeightForWidth())
        self.gps_r_index.setSizePolicy(sizePolicy)
        self.gps_r_index.setObjectName("gps_r_index")
        self.horizontalLayout_3.addWidget(self.gps_r_index)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_5 = QtWidgets.QLabel(self.GNSS)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_4.addWidget(self.label_5)
        self.gps_f_time = QtWidgets.QLineEdit(self.GNSS)
        self.gps_f_time.setObjectName("gps_f_time")
        self.horizontalLayout_4.addWidget(self.gps_f_time)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem2)
        self.label_6 = QtWidgets.QLabel(self.GNSS)
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_4.addWidget(self.label_6)
        self.gps_r_time = QtWidgets.QLineEdit(self.GNSS)
        self.gps_r_time.setObjectName("gps_r_time")
        self.horizontalLayout_4.addWidget(self.gps_r_time)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        self.tabWidget.addTab(self.GNSS, "")
        self.IMU = QtWidgets.QWidget()
        self.IMU.setObjectName("IMU")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.IMU)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.imuLayout = QtWidgets.QVBoxLayout()
        self.imuLayout.setObjectName("imuLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.imu_open_Button = QtWidgets.QPushButton(self.IMU)
        self.imu_open_Button.setObjectName("imu_open_Button")
        self.horizontalLayout.addWidget(self.imu_open_Button)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem3)
        self.label_7 = QtWidgets.QLabel(self.IMU)
        self.label_7.setObjectName("label_7")
        self.horizontalLayout.addWidget(self.label_7)
        self.label_9 = QtWidgets.QLabel(self.IMU)
        self.label_9.setText("")
        self.label_9.setPixmap(QtGui.QPixmap("resources/icons/red_line.png"))
        self.label_9.setObjectName("label_9")
        self.horizontalLayout.addWidget(self.label_9)
        self.label_10 = QtWidgets.QLabel(self.IMU)
        self.label_10.setObjectName("label_10")
        self.horizontalLayout.addWidget(self.label_10)
        self.label_8 = QtWidgets.QLabel(self.IMU)
        self.label_8.setText("")
        self.label_8.setPixmap(QtGui.QPixmap("resources/icons/green_line.png"))
        self.label_8.setObjectName("label_8")
        self.horizontalLayout.addWidget(self.label_8)
        self.label_11 = QtWidgets.QLabel(self.IMU)
        self.label_11.setObjectName("label_11")
        self.horizontalLayout.addWidget(self.label_11)
        self.label_12 = QtWidgets.QLabel(self.IMU)
        self.label_12.setText("")
        self.label_12.setPixmap(QtGui.QPixmap("resources/icons/blue_line.png"))
        self.label_12.setObjectName("label_12")
        self.horizontalLayout.addWidget(self.label_12)
        self.label_13 = QtWidgets.QLabel(self.IMU)
        self.label_13.setObjectName("label_13")
        self.horizontalLayout.addWidget(self.label_13)
        self.label_14 = QtWidgets.QLabel(self.IMU)
        self.label_14.setText("")
        self.label_14.setPixmap(QtGui.QPixmap("resources/icons/white_line.png"))
        self.label_14.setObjectName("label_14")
        self.horizontalLayout.addWidget(self.label_14)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem4)
        self.imuLayout.addLayout(self.horizontalLayout)
        self.verticalLayout_5.addLayout(self.imuLayout)
        self.imu_acc_Layout = QtWidgets.QHBoxLayout()
        self.imu_acc_Layout.setObjectName("imu_acc_Layout")
        self.verticalLayout_5.addLayout(self.imu_acc_Layout)
        self.imu_gyro_Layout = QtWidgets.QHBoxLayout()
        self.imu_gyro_Layout.setObjectName("imu_gyro_Layout")
        self.verticalLayout_5.addLayout(self.imu_gyro_Layout)
        self.imu_mag_Layout = QtWidgets.QHBoxLayout()
        self.imu_mag_Layout.setObjectName("imu_mag_Layout")
        self.verticalLayout_5.addLayout(self.imu_mag_Layout)
        self.horizontalLayout_9 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_9.setObjectName("horizontalLayout_9")
        self.label_30 = QtWidgets.QLabel(self.IMU)
        self.label_30.setObjectName("label_30")
        self.horizontalLayout_9.addWidget(self.label_30)
        self.imu_fslider = QtWidgets.QSlider(self.IMU)
        self.imu_fslider.setOrientation(QtCore.Qt.Horizontal)
        self.imu_fslider.setObjectName("imu_fslider")
        self.horizontalLayout_9.addWidget(self.imu_fslider)
        self.label_29 = QtWidgets.QLabel(self.IMU)
        self.label_29.setObjectName("label_29")
        self.horizontalLayout_9.addWidget(self.label_29)
        self.imu_rslider = QtWidgets.QSlider(self.IMU)
        self.imu_rslider.setProperty("value", 99)
        self.imu_rslider.setOrientation(QtCore.Qt.Horizontal)
        self.imu_rslider.setObjectName("imu_rslider")
        self.horizontalLayout_9.addWidget(self.imu_rslider)
        self.verticalLayout_5.addLayout(self.horizontalLayout_9)
        self.horizontalLayout_11 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_11.setObjectName("horizontalLayout_11")
        self.spinBox = QtWidgets.QSpinBox(self.IMU)
        self.spinBox.setMaximum(10000000)
        self.spinBox.setObjectName("spinBox")
        self.horizontalLayout_11.addWidget(self.spinBox)
        self.spinBox_2 = QtWidgets.QSpinBox(self.IMU)
        self.spinBox_2.setMaximum(10000000)
        self.spinBox_2.setObjectName("spinBox_2")
        self.horizontalLayout_11.addWidget(self.spinBox_2)
        self.verticalLayout_5.addLayout(self.horizontalLayout_11)
        self.tabWidget.addTab(self.IMU, "")
        self.Temperature = QtWidgets.QWidget()
        self.Temperature.setObjectName("Temperature")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.Temperature)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.temp_Layout = QtWidgets.QHBoxLayout()
        self.temp_Layout.setObjectName("temp_Layout")
        self.verticalLayout_3.addLayout(self.temp_Layout)
        self.verticalLayout_7.addLayout(self.verticalLayout_3)
        self.tabWidget.addTab(self.Temperature, "")
        self.Sync = QtWidgets.QWidget()
        self.Sync.setObjectName("Sync")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.Sync)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.gps_front_index_box = QtWidgets.QSpinBox(self.Sync)
        self.gps_front_index_box.setObjectName("gps_front_index_box")
        self.gridLayout.addWidget(self.gps_front_index_box, 1, 1, 1, 1)
        self.label_25 = QtWidgets.QLabel(self.Sync)
        self.label_25.setObjectName("label_25")
        self.gridLayout.addWidget(self.label_25, 0, 2, 1, 1)
        self.label_23 = QtWidgets.QLabel(self.Sync)
        self.label_23.setObjectName("label_23")
        self.gridLayout.addWidget(self.label_23, 0, 0, 1, 1)
        self.gps_end_index_box = QtWidgets.QSpinBox(self.Sync)
        self.gps_end_index_box.setObjectName("gps_end_index_box")
        self.gridLayout.addWidget(self.gps_end_index_box, 1, 2, 1, 1)
        self.label_24 = QtWidgets.QLabel(self.Sync)
        self.label_24.setObjectName("label_24")
        self.gridLayout.addWidget(self.label_24, 0, 1, 1, 1)
        self.gps_front_time = QtWidgets.QLineEdit(self.Sync)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.gps_front_time.sizePolicy().hasHeightForWidth())
        self.gps_front_time.setSizePolicy(sizePolicy)
        self.gps_front_time.setObjectName("gps_front_time")
        self.gridLayout.addWidget(self.gps_front_time, 2, 1, 1, 1)
        self.gps_end_time = QtWidgets.QLineEdit(self.Sync)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.gps_end_time.sizePolicy().hasHeightForWidth())
        self.gps_end_time.setSizePolicy(sizePolicy)
        self.gps_end_time.setObjectName("gps_end_time")
        self.gridLayout.addWidget(self.gps_end_time, 2, 2, 1, 1)
        self.verticalLayout_4.addLayout(self.gridLayout)
        self.syncButton = QtWidgets.QPushButton(self.Sync)
        self.syncButton.setMinimumSize(QtCore.QSize(0, 50))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(-1)
        font.setBold(True)
        font.setWeight(75)
        self.syncButton.setFont(font)
        self.syncButton.setFlat(False)
        self.syncButton.setObjectName("syncButton")
        self.verticalLayout_4.addWidget(self.syncButton)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label_26 = QtWidgets.QLabel(self.Sync)
        self.label_26.setObjectName("label_26")
        self.horizontalLayout_6.addWidget(self.label_26)
        self.imu_front = QtWidgets.QLineEdit(self.Sync)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.imu_front.sizePolicy().hasHeightForWidth())
        self.imu_front.setSizePolicy(sizePolicy)
        self.imu_front.setObjectName("imu_front")
        self.horizontalLayout_6.addWidget(self.imu_front)
        self.imu_end = QtWidgets.QLineEdit(self.Sync)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.imu_end.sizePolicy().hasHeightForWidth())
        self.imu_end.setSizePolicy(sizePolicy)
        self.imu_end.setObjectName("imu_end")
        self.horizontalLayout_6.addWidget(self.imu_end)
        self.verticalLayout_4.addLayout(self.horizontalLayout_6)
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.label_28 = QtWidgets.QLabel(self.Sync)
        self.label_28.setObjectName("label_28")
        self.horizontalLayout_8.addWidget(self.label_28)
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_8.addItem(spacerItem5)
        self.saveEdit = QtWidgets.QLineEdit(self.Sync)
        self.saveEdit.setObjectName("saveEdit")
        self.horizontalLayout_8.addWidget(self.saveEdit)
        self.saveButton = QtWidgets.QPushButton(self.Sync)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.saveButton.sizePolicy().hasHeightForWidth())
        self.saveButton.setSizePolicy(sizePolicy)
        self.saveButton.setObjectName("saveButton")
        self.horizontalLayout_8.addWidget(self.saveButton)
        self.verticalLayout_4.addLayout(self.horizontalLayout_8)
        spacerItem6 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_4.addItem(spacerItem6)
        self.tabWidget.addTab(self.Sync, "")
        self.verticalLayout_2.addWidget(self.tabWidget)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 2363, 26))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setGeometry(QtCore.QRect(269, 125, 168, 122))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.menuFile.sizePolicy().hasHeightForWidth())
        self.menuFile.setSizePolicy(sizePolicy)
        self.menuFile.setObjectName("menuFile")
        self.menuSettings = QtWidgets.QMenu(self.menubar)
        self.menuSettings.setGeometry(QtCore.QRect(303, 125, 168, 74))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.menuSettings.sizePolicy().hasHeightForWidth())
        self.menuSettings.setSizePolicy(sizePolicy)
        self.menuSettings.setObjectName("menuSettings")
        self.menuColor_Theme = QtWidgets.QMenu(self.menuSettings)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.menuColor_Theme.sizePolicy().hasHeightForWidth())
        self.menuColor_Theme.setSizePolicy(sizePolicy)
        self.menuColor_Theme.setObjectName("menuColor_Theme")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionNew = QtWidgets.QAction(MainWindow)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../snowballs-python-code/AvaNodeClass/resources/icons/new_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionNew.setIcon(icon)
        self.actionNew.setObjectName("actionNew")
        self.actionOpen = QtWidgets.QAction(MainWindow)
        self.actionOpen.setObjectName("actionOpen")
        self.actionExit = QtWidgets.QAction(MainWindow)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("../snowballs-python-code/AvaNodeClass/resources/icons/exit_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionExit.setIcon(icon1)
        self.actionExit.setObjectName("actionExit")
        self.actionDarkOrange = QtWidgets.QAction(MainWindow)
        self.actionDarkOrange.setObjectName("actionDarkOrange")
        self.actionAqua = QtWidgets.QAction(MainWindow)
        self.actionAqua.setObjectName("actionAqua")
        self.actionAmoled = QtWidgets.QAction(MainWindow)
        self.actionAmoled.setObjectName("actionAmoled")
        self.actionConsole = QtWidgets.QAction(MainWindow)
        self.actionConsole.setObjectName("actionConsole")
        self.actionDarkOrange_2 = QtWidgets.QAction(MainWindow)
        self.actionDarkOrange_2.setObjectName("actionDarkOrange_2")
        self.actionElegantDark = QtWidgets.QAction(MainWindow)
        self.actionElegantDark.setObjectName("actionElegantDark")
        self.actionManjaro = QtWidgets.QAction(MainWindow)
        self.actionManjaro.setObjectName("actionManjaro")
        self.actionMaterialDark = QtWidgets.QAction(MainWindow)
        self.actionMaterialDark.setObjectName("actionMaterialDark")
        self.actionUbuntu = QtWidgets.QAction(MainWindow)
        self.actionUbuntu.setObjectName("actionUbuntu")
        self.menuFile.addAction(self.actionNew)
        self.menuFile.addAction(self.actionOpen)
        self.menuFile.addAction(self.actionExit)
        self.menuColor_Theme.addAction(self.actionAmoled)
        self.menuColor_Theme.addAction(self.actionAqua)
        self.menuColor_Theme.addAction(self.actionConsole)
        self.menuColor_Theme.addAction(self.actionDarkOrange_2)
        self.menuColor_Theme.addAction(self.actionElegantDark)
        self.menuColor_Theme.addAction(self.actionManjaro)
        self.menuColor_Theme.addAction(self.actionMaterialDark)
        self.menuColor_Theme.addAction(self.actionUbuntu)
        self.menuSettings.addAction(self.menuColor_Theme.menuAction())
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuSettings.menuAction())

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        self.imu_fslider.valueChanged['int'].connect(self.spinBox.setValue)
        self.spinBox_2.valueChanged['int'].connect(self.imu_rslider.setValue)
        self.gps_rslider.valueChanged['int'].connect(self.gps_r_index.setValue)
        self.gps_r_index.valueChanged['int'].connect(self.gps_rslider.setValue)
        self.spinBox.valueChanged['int'].connect(self.imu_fslider.setValue)
        self.imu_rslider.valueChanged['int'].connect(self.spinBox_2.setValue)
        self.gps_f_index.valueChanged['int'].connect(self.gps_fslider.setValue)
        self.gps_fslider.valueChanged['int'].connect(self.gps_f_index.setValue)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "AvaRange - Data Viewer"))
        self.gps_open_Button.setText(_translate("MainWindow", "Open GNSS FIle"))
        self.label.setText(_translate("MainWindow", "Front"))
        self.label_2.setText(_translate("MainWindow", "End"))
        self.label_3.setText(_translate("MainWindow", "Front Index"))
        self.label_4.setText(_translate("MainWindow", "End Index"))
        self.label_5.setText(_translate("MainWindow", "Front Time"))
        self.label_6.setText(_translate("MainWindow", "End Time"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.GNSS), _translate("MainWindow", "GNSS"))
        self.imu_open_Button.setText(_translate("MainWindow", "Open IMU File"))
        self.label_7.setText(_translate("MainWindow", "X"))
        self.label_10.setText(_translate("MainWindow", "Y"))
        self.label_11.setText(_translate("MainWindow", "Z"))
        self.label_13.setText(_translate("MainWindow", "Total"))
        self.label_30.setText(_translate("MainWindow", "Front"))
        self.label_29.setText(_translate("MainWindow", "End"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.IMU), _translate("MainWindow", "IMU"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.Temperature), _translate("MainWindow", "Temperature"))
        self.label_25.setText(_translate("MainWindow", "End Index/Time"))
        self.label_23.setText(_translate("MainWindow", "GPS"))
        self.label_24.setText(_translate("MainWindow", "Front Index/Time"))
        self.syncButton.setText(_translate("MainWindow", "Synchronize"))
        self.label_26.setText(_translate("MainWindow", "IMU"))
        self.label_28.setText(_translate("MainWindow", "Filename"))
        self.saveButton.setText(_translate("MainWindow", "Save"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.Sync), _translate("MainWindow", "Sync"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menuSettings.setTitle(_translate("MainWindow", "Settings"))
        self.menuColor_Theme.setTitle(_translate("MainWindow", "Color Theme"))
        self.actionNew.setText(_translate("MainWindow", "New"))
        self.actionOpen.setText(_translate("MainWindow", "Open"))
        self.actionExit.setText(_translate("MainWindow", "Exit"))
        self.actionDarkOrange.setText(_translate("MainWindow", "DarkOrange"))
        self.actionAqua.setText(_translate("MainWindow", "Aqua"))
        self.actionAmoled.setText(_translate("MainWindow", "Amoled"))
        self.actionConsole.setText(_translate("MainWindow", "Console"))
        self.actionDarkOrange_2.setText(_translate("MainWindow", "DarkOrange"))
        self.actionElegantDark.setText(_translate("MainWindow", "ElegantDark"))
        self.actionManjaro.setText(_translate("MainWindow", "Manjaro"))
        self.actionMaterialDark.setText(_translate("MainWindow", "MaterialDark"))
        self.actionUbuntu.setText(_translate("MainWindow", "Ubuntu"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

