# README #
The BFW AvaRange repo includes all necessary tools to handle the recorded datasets from avalanche experiments with AvaNodes. While the most development was included into the AvaNode_GNSS_Class, it can also handle some basic operations for IMU and Temperature.

Installation
=============

Create venv (virtual python environment) [optional]
----------------------------------------

	sudo apt-get install python3-venv
	mkdir ~/venvs
	python -m venv ~/venvs/avarange

Clone repository
----------------

	git clone git@bitbucket.org:snowavalanche/avarange.git


Install Requirements
--------------------

	source ~/venvs/avarange/bin/activate
	pip install --upgrade pip
	pip install -r ./avarange/requirements.txt


Install local avarange package to virtualenv
--------------------------------

	pip install -e ./avarange/

========

## Structure

* classes: AvaNode_GNSS_Class, AvaNode_IMU_Class, AvaNode_TEMP_Class and AvaNode_Tools to handle the data
* data: datasets from avalanches on Nordkette, Seilbahnrinne, for testing scripts and classes
* dataviwer: GUI to look at GNSS, IMU and Temp data. To run the dataviewer execute main.py.
* experiments: scripts to plot data from datasets.