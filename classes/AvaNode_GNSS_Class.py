# -*- coding: utf-8 -*-
"""
Created on Mon Jun  7 17:00:48 2021

AvaNode GNSS data processing script
@author: neuhauser

This script processes AvaNode GNSS data from a specific file and provides 
various functionalities to analyze and plot the data.

Dependencies:
- os (handling paths and files)
- pandas (data manipulation)
- numpy (numerical operations)
- matplotlib.pyplot (plotting)
- matplotlib.dates (date formatting for plots)
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from datetime import datetime, time, date

from classes.AvaNode_Tools import get_coord_transform


class AvaNode_GNSS(object):
    
    def __init__(self):
        '''initalise the AvaNode_GNSS Class as follows: gnss = AvaNode_GNSS()
        then give it the path to the file: gnss.path = path_to_file/file.xxx
        afterwards you can read it in with gnss.read_data()'''

        self.path = ''
        self.file = ''
        self.work_dir = ''
        # in data the whole file is stored
        self.data = 0
        # in data_pos recives the filtered data with a 3D fix
        self.data_pos = 0        
        self.sync = 0
        self.sync_counter = []
        # self.gravi refers to the gravitational force at Notdkette, 
        self.gravi = 9.80665
        # The booleans are used for the check which data is avaible in the file
        self.is_synced = False #is the synch counter already calculated
        self.temp_bool = False # are there temperatures in this file
        self.vel_bool = False # are there dopller velocities in this file
        self.calc_pos_vel_bool = False
        self.pDop_bool = False


    def read_data(self):
        '''read in the data from the given path in a pandas dataframe, 
        while reading in the data it gives one information about the included 
        data in the file, like velocity or temperature.'''
        
        self.work_dir, self.file = os.path.split(self.path)
        
        print("Reading AvaNode GNSS File...")
        #Check how long header is
        skipheader = -1
        header = True
        with open(self.path) as f:
            while header:
                if f.readline()[0] == '#':
                    skipheader += 1
                else:
                    header = False       
        
        data = pd.read_csv(self.path, index_col=None, low_memory=False, skiprows=skipheader)
        print("Done!")
        print("Searching for Sync Keys...")
        # Search for sync keys:
        print("Sync Pulse...")
        if data.columns.str.contains('rising edge sync signal').any():
            sync_key = data.columns.str.contains('sync')  # Search header for sync
        elif data.columns.str.contains('gpsFlag').any():
            sync_key = data.columns.str.contains('gpsFlag')  
        elif data.columns.str.contains('sync signal').any():
            sync_key = data.columns.str.contains('sync')  # Search header for sync
        sync = data.columns[np.where(sync_key)[0]]
        sync = sync[0]
        self.sync = data[sync]
        print("OK")
        
        # Search for iTow
        print("iTow...")
        if data.columns.str.contains('ms').any():
            itow_key = data.columns.str.contains('ms')
            print("OK")
        itow = data.columns[np.where(itow_key)[0]]
        itow = itow[0]
        self.itow = itow
        
        
        #Search for UTC key:
        print("UTC...")
        if data.columns.str.contains('utc').any():
            utc_key = data.columns.str.contains('utc')
        if data.columns.str.contains('UTC ').any():
            utc_key = data.columns.str.contains('UTC')
        utc = data.columns[np.where(utc_key)[0]]
        utc = utc[0]
        self.utc = utc
        print("OK")
        
        # Start when we have fix Type 3 == 3D Position
        # Search for fix Type
        print("Fix Type...")
        if data.columns.str.contains('Type').any():
            type_key = data.columns.str.contains('Type')
            typ = data.columns[np.where(type_key)]
            typ = typ[0]
        print("OK")
        
        #Searching for pDOP
        print("pDOP...")
        if data.columns.str.contains('pDop').any():
            self.pDop_bool = True
        print("OK")
        
        #Searching for temp data
        print("Temperature...")
        if data.columns.str.contains('ambient').any():
            self.temp_bool = True
        print("OK")
        
        print("Velocity...")
        if data.columns.str.contains('velE').any():
            print("GNSS File contains doppler velocities.")
            self.vel_bool = True
        else:
            print("No velocities in this file!")
        
        #Add microeconds to date
        a = data[self.itow] % 1000
        a = a.astype(str)

        newdate = data[self.utc] + ":" + a
        data["date[ms]"] = newdate
        # Convert date column to date object for plot
        data['timestamp'] = pd.to_datetime(data["date[ms]"], 
                                           format='%Y:%m:%d:%H:%M:%S:%f')
        
        # Convert GNSS Data
        if (data['longitude[deg]'] > 90).any():
            data['longitude[deg]'] = data['longitude[deg]'] / 10000000
        if (data['latitude[deg]'] > 180).any():
            data['latitude[deg]'] = data['latitude[deg]'] / 10000000
            print("Converted Lat / Long ... ")
        data['height[m]'] = data['height[mm]'] / 1000
                      
        # Save sorted dataframe to preprocessed file:
        #pos.to_csv(self.path[:-4] + "_preprocessed.csv", index=False)
        try:
            self.data_pos = data.loc[data[typ] == 3]
        except:
            print("GNSS File contains no valuable data!")
        self.data = data
        if self.vel_bool:
            v_tot = np.sqrt((self.data['velN[mm/s]']/1000)**2 + 
                            (self.data['velE[mm/s]']/1000)**2 + 
                            (self.data['velD[mm/s]']/1000)**2)
            self.data['v_total'] = v_tot
            self.data_pos['v_total'] = v_tot
        
# =============================================================================
#         if self.pDop_bool:
#             self.data['pDop[]'] *= 0.01
#             self.data_pos['pDop[]'] *= 0.01
# =============================================================================
        print("GNSS finished!")
        
    def sync_gps(self):
        '''This Function creates a staircase funtion = gps_sync_counter, 
        starting with 0. This can then be used to compare it with the IMU 
        counter part and synchronize the data.'''
        
        startvalue = self.sync[0]
        gps_peaks = 0
        gps_sync_counter = []

        counter_even = 0
        counter_odd = 1

        for value in self.sync:
            if value == startvalue:
                if value == 0:
                    gps_sync_counter.append(counter_even)
                if value == 1:
                    gps_sync_counter.append(counter_odd)
            if value != startvalue:
                gps_peaks += 1
                if value == 0:
                    counter_even += 2
                    gps_sync_counter.append(counter_even)
                if value == 1:
                    if gps_peaks > 1:
                        counter_odd += 2
                    gps_sync_counter.append(counter_odd)
                startvalue = value
        self.sync_counter = gps_sync_counter
        self.is_synced = True
        print("GNSS has {} peaks!".format(gps_peaks))
        
        
    def calc_pos_vel(self):
        '''Calculates the position velocity from the GNSS positions in the 
        dataset,         with the package geopy.'''
        
        from geopy import distance
        
        # Calculating the distances between the GPS coordinates
        x = [0]
        y = [0]
        z = [0]
        dist_x = [0]
        dist_y = [0]
        dist_z = [0]
        
        dt = 0.1 # 10 Hz
        
        # lat = North/south // long = East/West
        for i in range(1, self.data_pos.shape[0]):
            delta_x_0 = (self.data_pos["latitude[deg]"].iloc[0], self.data_pos["longitude[deg]"].iloc[0])
            delta_x_1 = (self.data_pos["latitude[deg]"].iloc[i], self.data_pos["longitude[deg]"].iloc[0])
            delta_x_i = (self.data_pos["latitude[deg]"].iloc[i-1], self.data_pos["longitude[deg]"].iloc[0]) 
            x_i = distance.distance(delta_x_0, delta_x_1).m
            dx = distance.distance(delta_x_i, delta_x_1).m
            dist_x.append(dist_x[i-1] + dx)
            if self.data_pos["latitude[deg]"].iloc[i] > self.data_pos["latitude[deg]"].iloc[0]:
                x.append(x_i)
            else:
                x.append(-x_i)

            delta_y_0 = (self.data_pos["latitude[deg]"].iloc[0], self.data_pos["longitude[deg]"].iloc[0])
            delta_y_1 = (self.data_pos["latitude[deg]"].iloc[0], self.data_pos["longitude[deg]"].iloc[i])
            delta_y_i = (self.data_pos["latitude[deg]"].iloc[0], self.data_pos["longitude[deg]"].iloc[i-1])
            y_i = distance.distance(delta_y_0, delta_y_1).m
            dy = distance.distance(delta_y_i, delta_y_1).m
            dist_y.append(dist_y[i-1] + dy)
            if self.data_pos["longitude[deg]"].iloc[i] > self.data_pos["longitude[deg]"].iloc[0]:
                y.append(y_i)
            else:
                y.append(-y_i)
            
            self.data_pos['height[m]'] = self.data_pos['height[mm]'] / 1000
            z_i = self.data_pos['height[m]'].iloc[0] - self.data_pos['height[m]'].iloc[i]
            delta_z = abs(self.data_pos['height[m]'].iloc[i-1] - self.data_pos['height[m]'].iloc[i])
            z.append(z_i)
            dist_z.append(dist_z[i-1] + delta_z)
            
        gps_vx = np.zeros(len(x))
        gps_vy = np.zeros(len(x))
        gps_vz = np.zeros(len(x))

        for i in range(len(x)-1):
            gps_vx[i+1] = (x[i+1] - x[i]) / dt
            gps_vy[i+1] = (y[i+1] - y[i]) / dt
            gps_vz[i+1] = (z[i+1] - z[i]) / dt
            
        time = np.ones(len(x))
        for idx in range(len(time)):
            time[idx] = dt * idx
                    
        self.data_pos['v_total_pos'] = np.sqrt(gps_vx ** 2 + gps_vy ** 2 + gps_vz ** 2)
        self.data_pos["Time"] = time
        self.data_pos['X'] = x
        self.data_pos['Y'] = y
        self.data_pos['Z'] = z
        self.data_pos['x_dist'] = dist_x
        self.data_pos['y_dist'] = dist_y
        self.data_pos['z_dist'] = dist_z
        self.data_pos["h_dist"] = np.linalg.norm((dist_x,dist_y), axis=0)
        self.data_pos['tot_dist'] = np.linalg.norm((dist_x,dist_y,dist_z), axis=0)
        
        self.calc_pos_vel_bool = True
                
        
    def plot_vel(self, saveplot=False, plotindex=False, plot_pos_vel=False, starttime=0, endtime=0):
        """
        Plot GNSS velocities with date time.

        Parameters:
        - saveplot (bool): Save the plot as an image file (default: False)
        - plotindex (bool): Plot against the data index instead of timestamps (default: False)
        - plot_pos_vel (bool): Plot velocities calculated from position data (default: False)
        - starttime (str or int): Start time for the plot in hh:MM:SS or if 
            plotindex is set to true use the index where to cut(default: 0)
        - endtime (str or int): End time for the plot in hh:MM:SS or if 
            plotindex is set to true use the index where to cut(default: 0)
        """
        
        if not self.calc_pos_vel_bool:
            self.calc_pos_vel()
            self.calc_pos_vel_bool = True
        
        # Define the date format
        date = dates.date2num(self.data_pos['timestamp'])
        date_form = dates.DateFormatter("%H:%M:%S")
        
        if starttime == 0 and plotindex == 0:
            starttime = self.data_pos['timestamp'].iloc[0]
        elif type(starttime) == str:
            #Convert starttime from string to timestamp
            time_obj = datetime.strptime(starttime, "%H:%M:%S").time()
            date_obj = self.data_pos['timestamp'].iloc[0].date()
            datetime_obj = datetime.combine(date_obj, time_obj)
            start_idx = np.where(datetime_obj >= self.data_pos['timestamp'])[0][-1]
            starttime = self.data_pos['timestamp'].iloc[start_idx]

        if endtime == 0 and plotindex == 0:
            endtime = self.data_pos['timestamp'].iloc[-1]
        elif type(endtime) == str:
            #Convert endtime from string to timestamp
            time_obj = datetime.strptime(endtime, "%H:%M:%S").time()
            date_obj = self.data_pos['timestamp'].iloc[0].date()
            datetime_obj = datetime.combine(date_obj, time_obj)
            end_idx = np.where(datetime_obj <= self.data_pos['timestamp'])[0][0]
            endtime = self.data_pos['timestamp'].iloc[end_idx]
        elif plotindex and endtime == 0:
            endtime = len(self.data_pos['v_total'])
        
        linewidth = 1
        fig, ax = plt.subplots()
        if plotindex == False:
            ax.xaxis.set_major_formatter(date_form)
            fig.autofmt_xdate(rotation=45)
            
        if plot_pos_vel and plotindex:
            ax.plot(self.data_pos['v_total_pos'], color='g', linewidth=linewidth,
                    label='Position Velocity', linestyle='--')

        if plot_pos_vel and plotindex == False:
            ax.plot(date, self.data_pos['v_total_pos'], color='g', linewidth=linewidth,
                    label='Position Velocity', linestyle='--')
        
        if self.vel_bool:
            if plotindex:
                ax.plot(self.data_pos['v_total'], color='b', linewidth=linewidth,
                        label='Doppler Velocity')
                ax.set_xlabel('Index')
            else:
                ax.plot(date, self.data_pos['v_total'], color='b', linewidth=linewidth,
                        label='Doppler Velocity')
                ax.set_xlabel('Time [UTC]')
        
        if self.vel_bool and plot_pos_vel:
             ax.legend()
        ax.set_xlim([starttime, endtime])
        ax.set_ylim([-1, 30])
        ax.set_ylabel('Velocity [m/s]')
        ax.set_title(self.file[:-4] + " GNSS Velocities")
        ax.grid()
        
        if saveplot:
            fig.savefig(self.work_dir + '/' + self.file[:-4] + '_GNSS_vel.png', dpi=300)
            
    def export_in_radar_cs(self):
        '''Export the GNSS positions in radar coordinate system. The included 
        radar position is at Seegrube measurement station. A underlaying DEM is
        used, to correct the GNSS z-ccordinates with it.
        Output: csv file with columns [time, distance to radar, velocity]'''
        
        from osgeo import gdal
        from geopy import distance
        
        # Calc Distance to radar
        radar_pos = (47.306584, 11.380406) #lat, long radar seegrube towards seilbahnrinne
        radar_z = 1900.881 # z
        # Export for Radar Comparison
        filepath = r"./Nordkette_wgs84.tif"
        # Open the file:
        nordkette_raster = gdal.Open(filepath)

        # Check type of the variable 'raster'
        type(nordkette_raster)
        # Projection
        nordkette_raster.GetProjection()
        band = nordkette_raster.GetRasterBand(1)

        cols = nordkette_raster.RasterXSize
        rows = nordkette_raster.RasterYSize

        transform = nordkette_raster.GetGeoTransform()

        xOrigin = transform[0]
        yOrigin = transform[3]
        pixelWidth = transform[1]
        pixelHeight = -transform[5]

        data = band.ReadAsArray(0, 0, cols, rows)
        point_list = []
        for i in range(len(self.data["latitude[deg]"])):
            point_list.append([self.data["longitude[deg]"].iloc[i], self.data["latitude[deg]"].iloc[i]])
        
        # Get z coordinate from DEM, to avoid wrong GNSS altitudes
        z = []
        for point in point_list:
            col = int((point[0] - xOrigin) / pixelWidth)
            row = int((yOrigin - point[1] ) / pixelHeight)
            z.append(data[row][col])
        self.data["z_raster"] = z
        
        dist = []
        for i in range(len(self.data["latitude[deg]"])):
            pos = (self.data["latitude[deg]"].iloc[i], self.data["longitude[deg]"].iloc[i])
            horizontal_distance = distance.distance(pos, radar_pos).m
            vertical_distance = self.data["z_raster"].iloc[i] - radar_z
            dist.append(np.linalg.norm([horizontal_distance, vertical_distance]))
        # Export to csv
        if self.vel_bool:
            self.export_df = pd.DataFrame(list(zip(self.data_pos["timestamp"].to_list(),
                                                   dist, self.data_pos['v_total'].to_list())),
                                          columns=["Time", "Distance_to_radar[m]", "Doppler velocity[m/s]"])
        else:
            self.export_df = pd.DataFrame(list(zip(self.data_pos["timestamp"].to_list(),
                                                   dist, self.data_pos['v_total_pos'].to_list())),
                                          columns=["Time", "Distance_to_radar[m]", "Position velocity[m/s]"])
        self.export_df.to_csv("yymmdd_Cxx_radarCS.csv", index=None) #ToDo: Auto for date and export!!!
        
    def gnss_to_mercator(self):
        '''Takes the GNSS Positions in WGS84 (Epsg:4326) and transforms them into
        Mercator Projection : MGI / Austria GK West, EPSG:31254
        Returns North, East and Z in this Projection'''
        
        north = np.zeros_like(self.data['latitude[deg]'])
        east = np.zeros_like(north)
        z = np.zeros_like(north)
        
        transform = get_coord_transform(4326, 31254) #transform in cartesian coordinates to add the path
        for i in range(len(self.data['latitude[deg]'])):
            north[i], east[i], z[i] = transform.TransformPoint(self.data['latitude[deg]'].values[i], self.data['longitude[deg]'].values[i], self.data['height[m]'].values[i])
        return north, east, z
         
    def save_to_vtk(self, Node=str, path=str):
        '''Takes the GNSS Positions in WGS84 (Epsg:4326) and transforms them into
        Mercator Projection : MGI / Austria GK West, EPSG:31254, takes the altitude
        information from the nordkette DEM and saves this into a vtk file for 
        Paraview (https://www.paraview.org/).'''
        
        from pyevtk.hl import pointsToVTK
        from osgeo import gdal
        
        # Get Altitude from DHM because GPS is really wrong!
        filepath = r"./Nordkette_wgs84.tif"
        # Open the file:
        nordkette_raster = gdal.Open(filepath)

        # Check type of the variable 'raster'
        type(nordkette_raster)
        # Projection
        nordkette_raster.GetProjection()
        band = nordkette_raster.GetRasterBand(1)

        cols = nordkette_raster.RasterXSize
        rows = nordkette_raster.RasterYSize

        transform = nordkette_raster.GetGeoTransform()

        xOrigin = transform[0]
        yOrigin = transform[3]
        pixelWidth = transform[1]
        pixelHeight = -transform[5]

        data = band.ReadAsArray(0, 0, cols, rows)
        point_list = []
        for i in range(len(self.data["latitude[deg]"])):
            point_list.append([self.data["longitude[deg]"].iloc[i], self.data["latitude[deg]"].iloc[i]])

        z = []
        for point in point_list:
            col = int((point[0] - xOrigin) / pixelWidth)
            row = int((yOrigin - point[1] ) / pixelHeight)
            z.append(data[row][col])
            #print(row, col, data[row][col])
        self.data["z_raster"] = z
        
        north, east, height = self.gnss_to_mercator(self)
        for i in range(len(north)):
            x = np.array(east[i])
            y = np.array(north[i])
            z = np.array(self.data["z_raster"].iloc[i], dtype=np.float64)
            vel = self.data_pos["v_total"].iloc[i]
            vel = np.array(vel, ndmin=1)
            pointsToVTK(path + "\C{}\GNSS_points_{}".format(Node, i), x, y, z, data = {"u" : vel})

    def dict_for_avaframe(self, name, start, end, savebool=False):
        """converting the dataset to a dictionary used by avaframe"""
                       
        dictNode = {}
        startIdx = int(start * 10)
        endIdx = int(end * 10)
        gnss_time = (self.data_pos[self.itow] - self.data_pos[self.itow].iloc[0])/1000
        t = gnss_time[startIdx:endIdx] - gnss_time[startIdx]
        dictNode['t'] = t.to_list()
        n,e,z = self.gnss_to_mercator()
        dictNode['x'] = e[startIdx:endIdx]
        dictNode['y'] = n[startIdx:endIdx]
        dictNode['z'] = z[startIdx:endIdx]
        if self.vel_bool:
            dictNode['velocityMag'] = self.data_pos["v_total"].iloc[startIdx:endIdx].to_numpy()
        else:
            dictNode['velocityMag'] = self.data_pos["v_total_pos"].iloc[startIdx:endIdx].to_numpy()
        dist_xy = self.data_pos["h_dist"].iloc[startIdx:endIdx] - self.data_pos["h_dist"].iloc[startIdx]
        dictNode['trajectoryLengthXY'] = dist_xy.to_numpy()
        dist_xyz = self.data_pos["tot_dist"].iloc[startIdx:endIdx] - self.data_pos["tot_dist"].iloc[startIdx]
        dictNode['trajectoryLengthXYZ'] = dist_xyz.to_numpy()        
        
        if self.vel_bool:
            gps_acc_kernel_size = 40
            self.gps_acc_kernel = np.ones(gps_acc_kernel_size) / gps_acc_kernel_size
            self.gps_v_smooth_acc = np.convolve(self.data_pos["v_total"], self.gps_acc_kernel, mode='same')
            self.gnss_acc = np.diff(self.gps_v_smooth_acc) / np.diff(gnss_time)
            dictNode['uAcc'] = self.gnss_acc[startIdx:endIdx]
            dictNode['uy'] = self.data_pos['velN[mm/s]'].iloc[startIdx:endIdx].to_numpy()/1000
            dictNode['ux'] = self.data_pos['velE[mm/s]'].iloc[startIdx:endIdx].to_numpy()/1000
            dictNode['uz'] = self.data_pos['velD[mm/s]'].iloc[startIdx:endIdx].to_numpy()/1000
        else:
            dictNode['uAcc'] = np.full(len(dictNode['t']), np.nan)
            dictNode['uy'] = np.full(len(dictNode['t']), np.nan)
            dictNode['ux'] = np.full(len(dictNode['t']), np.nan)
            dictNode['uz'] = np.full(len(dictNode['t']), np.nan)
# =============================================================================
#         for key in dictNode:
#             if key != 't':
#                 dictNode[key] = dictNode[key].reshape(len(dictNode[key]), 1)
# =============================================================================
        
        import pickle
        if savebool:
            with open('{}.pickle'.format(name), 'wb') as handle:
                pickle.dump(dictNode, handle, protocol=pickle.HIGHEST_PROTOCOL)
        
        return dictNode
       
if __name__ == "__main__":
    # Example usage of the AvaNode_GNSS class
    gnss_data = AvaNode_GNSS()
    gnss_data.path = r"..\data\2023-03-15_Nordkette_avalanche\C10\GPS\ava230315_C10_seilbahn_gnss.txt"
    gnss_data.path = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2023-08-17_Nordkette_ranging_experiment\SB02\C10\GPS\C10Gps001.txt"
    gnss_data.read_data()
    gnss_data.sync_gps()
    gnss_data.calc_pos_vel()
    gnss_data.plot_vel()
    gnss_data.plot_vel(plotindex=True ,starttime=1000, endtime=2500, plot_pos_vel=True)
    
    n,e,z = gnss_data.gnss_to_mercator()
    start = 148+1.72
    end = 188+6.88
    dictNode = gnss_data.dict_for_avaframe("230315", start, end)
