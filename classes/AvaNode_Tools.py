# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 14:25:16 2021

AvaNode Tools, script for functions used by IMU and GNSS
@author: neuhauser
"""

import numpy as np
import pandas as pd


def get_coord_transform(source_epsg, target_epsg):
    
    from osgeo import osr
    
    '''
    Creates an OGR-framework coordinate transformation for use in projecting
    coordinates to a new coordinate reference system (CRS). Used as, e.g.:
        transform = get_coord_transform(source_epsg, target_epsg)
        transform.TransformPoint(x, y)
    Arguments:
        source_epsg     The EPSG code for the source CRS
        target_epsg     The EPSG code for the target CRS
    '''
    # Develop a coordinate transformation, if desired
    #transform = None
    source_ref = osr.SpatialReference()
    target_ref = osr.SpatialReference()
    source_ref.ImportFromEPSG(source_epsg)
    target_ref.ImportFromEPSG(target_epsg)
    return osr.CoordinateTransformation(source_ref, target_ref) 

def transform_imu_to_global(path, lat, long, altitude, date):
    
    from magnetic_field_calculator import MagneticFieldCalculator #Python API for British Geological Survey magnetic field calculator
    
    '''
    This functions is used to determine the magnetic declination and rotate the 
    given path with this angle.
    Input: input_path_csv_with_x_and_y, lat, long, altitude[km], date(yyyy-mm-dd)
    output: csv with new Fields, North and East are rotated and in the right projection.
    '''

    # Get the declination of the magnetic north pole to the geographic noth pole
    calculator = MagneticFieldCalculator()

    calculator = MagneticFieldCalculator(
        model='igrf',
        revision='current',
        custom_url='http://geomag.bgs.ac.uk/'
    )

    result = calculator.calculate(
        latitude=lat,
        longitude=long,
        altitude=altitude,
        date=date
    )

    declination = result['field-value']['declination']['value']
    print("Deklination = {} °".format(declination))

    theta = np.radians(declination)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array(((c, -s), (s, c)))

    transform = get_coord_transform(4326, 31287) #transform in cartesian coordinates to add the path
    north, east, z = transform.TransformPoint(lat, long)

    col = ['Time', 'x', 'y', 'z']
    inp = pd.read_csv(path, delimiter = ",", header=None, skiprows=1, names=col)

    inp['x_rot'] = inp['x']
    inp['y_rot'] = inp['y']

    for i in range(len(inp['x'])):
        vec = np.array((inp['x'].iloc[i], inp['y'].iloc[i]))
        x_rot, y_rot = R @ vec
        inp['x_rot'].iloc[i] = x_rot
        inp['y_rot'].iloc[i] = y_rot

    inp['North'], inp['East'] = north + inp['x_rot'], east + inp['y_rot']

    inp.to_csv(path[:-4] + '_wgs84_magdeclination.csv')
    