# -*- coding: utf-8 -*-
"""
Created on Mon Sep 11 17:20:17 2023

@author: neuhauser
"""

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from classes.AvaNode_GNSS_Class import AvaNode_GNSS

filePath = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2023-08-17_Nordkette_ranging_experiment\SB02\C09\GPS\SB02Data.csv"
filePathNode = r"C:\Users\neuhauser\OneDrive - Bundesforschungszentrum fuer Wald\20070-AvaRange-data\measurements\2023-08-17_Nordkette_ranging_experiment\SB02\C10\GPS\C10Gps001.txt"
dataSGNSS = pd.read_csv(filePath, header = [0], encoding = "ISO-8859-1")
#data = np.loadtxt(filePath, )


gnss_data = AvaNode_GNSS()
gnss_data.path = filePathNode
gnss_data.read_data()

dataNode = gnss_data.data_pos

fig, ax = plt.subplots()
ax.plot(dataSGNSS["iTOW"], dataSGNSS["Speed"], label='AvaNode Gen. III')
ax.plot(dataNode["iTow[ms]"]/1000, dataNode["v_total"], label='AvaNode Gen.II')
ax.legend()
ax.grid()
ax.set_xlabel('iTow (s)')
ax.set_ylabel('velocity (ms^-1)')

# =============================================================================
# import plotly.express as px
# 
# fig = px.line([dataSGNSS, dataNode],x=["iTOW", "iTow[ms]"], y=["Speed", "v_total"], title='GNSS Speed SB02')
# fig.show()
# =============================================================================
