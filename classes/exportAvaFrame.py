# -*- coding: utf-8 -*-
"""
Created on Wed Aug  2 17:19:03 2023

@author: neuhauser
"""

#%% Libs
import numpy as np
import pickle
from classes.AvaNode_GNSS_Class import AvaNode_GNSS

#%% Class measurement

class measurement:
    
    def __init__(self, name):
        self.name = name
        self.gps = AvaNode_GNSS()
        self.start = 0 # Starttime in secons
        self.end = 0 # Endtime in seconds
        
        # Smoothing Kernels

        gps_kernel_size = 2
        self.gps_kernel = np.ones(gps_kernel_size) / gps_kernel_size
        gps_acc_kernel_size = 40
        self.gps_acc_kernel = np.ones(gps_acc_kernel_size) / gps_acc_kernel_size
        
    def define_paths(self, gps_path):
        self.gps.path = gps_path
        
    def define_times(self, start, end):
        self.start = start
        self.end = end
        
    def read_data(self):
        self.gps.read_data()
        self.gps.calc_pos_vel()
        
    def process_data(self):
        
        if self.name[:2] == '21':
            self.gps_v = self.gps.data_pos['v_total_pos']
        else:
            self.gps_v = self.gps.data_pos['v_total']
        #self.gps.to_local()
        
        #Smooth GNSS Velocity for acc calc
        self.gps_v_smooth_acc = np.convolve(self.gps_v, self.gps_acc_kernel, mode='same')
        
        # Get GNSS Time
        if self.name[:2] == '21' or self.name == '220123':
            self.gps_time = self.gps.data_pos.iloc[:,0]/1000 - self.gps.data_pos.iloc[0,0]/1000
        else:
            self.gps_time = self.gps.data_pos.iloc[:,1]/1000 - self.gps.data_pos.iloc[0,1]/1000
        
        self.gps_time_normalized = (self.gps_time - self.start)/ (self.end - self.start)
        
        
        # Calculate GNSS Accelerations
        self.gnss_acc = np.diff(self.gps_v_smooth_acc) / np.diff(self.gps_time)
                
    
#%% Set Data

measurment_name_list = ['210315', '210316-C01', '210316-C03', '220123', '220203', 
                        '220222-C07', '220222-C09', '220222-C10', '230203-C06',
                        '230204-C07', '230315']

exportBool = [False, False, False, False, False, True, True, True, False, False, False]

gps_path_list = [r'..\data\2021-03-15_Nordkette_avalanche\C01\GPS\ava210315_C01.txt',
                 r'..\data\2021-03-16_Nordkette_avalanche\C01\GPS\ava210316_C01_GPS.txt',
                 r'..\data\2021-03-16_Nordkette_avalanche\C03\GPS\ava210316_C03_GPS.txt',
                 r'..\data\2022-01-23_Nordkette_avalanche\C01\GPS\ava220123_C01.txt',
                 r'..\data\2022-02-03_Nordkette_avalanche\C10\GPS\ava220203_seilbahn_C10_gnss.txt',
                 r'..\data\2022-02-22_Nordkette_avalanche\C07\GPS\220222_C07_avalanche_GPS.txt',
                 r'..\data\2022-02-22_Nordkette_avalanche\C09\GPS\220222_C09_avalanche_GPS.txt',
                 r'..\data\2022-02-22_Nordkette_avalanche\C10\GPS\220222_C10_avalanche_GPS.txt',
                 r'..\data\2023-02-03_Nordkette_avalanche\C06\GPS\ava230203_C06_seilbahn_gnss.txt',
                 r'..\data\2023-02-04_Nordkette_avalanche\C07\GPS\ava230204_C07_gnss.txt',
                 r'..\data\2023-03-15_Nordkette_avalanche\C10\GPS\ava230315_C10_seilbahn_gnss.txt']


start_time_list = [37.5, 39.16, 71.05, 48.75, 137.55, 45.415, 28.1152, 60.539,
                   21.32, 141.93, 149.72]
end_time_list = [82.55, 80.929, 106.89, 94.06, 184.627, 85-4.95, 65.18, 97.44,
                 66.77, 190.35, 194.88]

measurment_name_list = [value for value, condition in zip(measurment_name_list, exportBool) if condition]
gps_path_list = [value for value, condition in zip(gps_path_list, exportBool) if condition]
start_time_list = [value for value, condition in zip(start_time_list, exportBool) if condition]
end_time_list = [value for value, condition in zip(end_time_list, exportBool) if condition]

#%%Read Data
# Init Experiments in list with name
exp = []
for meas in measurment_name_list:
    exp.append(measurement(meas))

# Define path and Start/Endtime
for (meas, gps_path) in zip(exp, gps_path_list):
    meas.define_paths(gps_path)
# Read Data
for meas in exp:
    meas.read_data()
#%% Define Setting

for (meas, start, end) in zip(exp, start_time_list, end_time_list):
    meas.define_times(start, end)
    
#%% Process Data

for meas in exp:
    meas.process_data()

#%% Export Startpoints

pointList = []
for meas in exp:
    startIdx = int(meas.start * 10)
    startPoint = [meas.gps.data_pos['latitude[deg]'].iloc[startIdx], meas.gps.data_pos['longitude[deg]'].iloc[startIdx]]
    pointList.append(startPoint)
pointArray = np.array(pointList) 
#np.savetxt("Startpoints.csv", pointArray) 

#%% Setup dict for export
dictList = []
for meas in exp:

    dictNode = meas.gps.dict_for_avaframe(meas.name, meas.start, meas.end)
    dictList.append(dictNode)

max_length = 0
for meas in dictList:
    if len(meas['t']) > max_length:
        max_length = len(meas['t'])

#Prepare final dict
dictExport = {}
dictExport['t'] = np.arange(0, max_length/10, 0.1)
i = 0

for my_dict in dictList:   
    # Get the iterator for the keys of the dictionary
    key_iterator = iter(my_dict)
    # Skip the first key = 't'
    next(key_iterator)
    # Iterating through the remaining keys of the dictionary
    for key in key_iterator:
        filled_array = np.concatenate([my_dict[key], np.full(max_length - len(my_dict[key]), np.nan)])
        if i == 0:
            dictExport[key] = filled_array
        else:
            dictExport[key] = np.vstack([dictExport[key], filled_array])
    i += 1

for my_dict in dictList:   
    # Get the iterator for the keys of the dictionary
    key_iterator = iter(my_dict)
    # Skip the first key = 't'
    next(key_iterator)
    # Iterating through the remaining keys of the dictionary
    for key in key_iterator:
        dictExport[key] = dictExport[key].T

dictExport['label'] = measurment_name_list
        
with open('{}.pickle'.format("220222"), 'wb') as handle:
    pickle.dump(dictExport, handle, protocol=pickle.HIGHEST_PROTOCOL)

