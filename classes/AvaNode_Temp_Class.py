# -*- coding: utf-8 -*-
"""
Created on Wed Jul 19 09:16:48 2023

AvaNode Temperature data processing script
@author: neuhauser

Dependencies:
- os (handling paths and files)
- pandas (data manipulation)
- numpy (numerical operations)
- matplotlib.pyplot (plotting)
- matplotlib.dates (date formatting for plots)
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as dates


class AvaNode_TEMP(object):
    
    def __init__(self):
        '''initalise the AvaNode_TEMP Class as follows: temp = AvaNode_TEMP()
        then give it the path to the file: temp.path = path_to_file/file.xxx
        afterwards you can read it in with temp.read_data()'''

        self.path = ''
        self.file = ''
        self.work_dir = ''
        # in data the whole file is stored
        self.data = 0
        # in data_pos recives the filtered data with a 3D fix
        self.data_pos = 0        
        self.sync = 0
        self.sync_counter = []
        # self.gravi refers to the gravitational force at Notdkette, 
        self.gravi = 9.80665
        # The booleans are used for the check which data is avaible in the file
        self.is_synced = False
        self.temp_bool = False
        self.vel_bool = False
        self.calc_pos_vel_bool = False
        self.pDop_bool = False


    def read_data(self):
        '''read in the data from the given path in a pandas dataframe, 
        while reading in the data it gives one information about the included 
        data in the file, like velocity or temperature.'''
        
        self.work_dir, self.file = os.path.split(self.path)
        
        print("Reading AvaNode GNSS File...")
        #Check how long header is
        skipheader = -1
        header = True
        with open(self.path) as f:
            while header:
                if f.readline()[0] == '#':
                    skipheader += 1
                else:
                    header = False       
        
        data = pd.read_csv(self.path, index_col=None, low_memory=False, skiprows=skipheader)
        print("Done!")
        print("Searching for Sync Keys...")
        # Search for sync keys:
        print("Sync Pulse...")
        if data.columns.str.contains('rising edge sync signal').any():
            sync_key = data.columns.str.contains('sync')  # Search header for sync
        elif data.columns.str.contains('gpsFlag').any():
            sync_key = data.columns.str.contains('gpsFlag')  
        elif data.columns.str.contains('sync signal').any():
            sync_key = data.columns.str.contains('sync')  # Search header for sync
        sync = data.columns[np.where(sync_key)[0]]
        sync = sync[0]
        self.sync = data[sync]
        print("OK")
        
        # Search for iTow
        print("iTow...")
        if data.columns.str.contains('ms').any():
            itow_key = data.columns.str.contains('ms')
        itow = data.columns[np.where(itow_key)[0]]
        itow = itow[0]
        self.itow = itow
        print("OK")
        
        #Search for UTC key:
        print("UTC...")
        if data.columns.str.contains('utc').any():
            utc_key = data.columns.str.contains('utc')
        if data.columns.str.contains('UTC ').any():
            utc_key = data.columns.str.contains('UTC')
        utc = data.columns[np.where(utc_key)[0]]
        utc = utc[0]
        self.utc = utc
        print("OK")
        
        # Start when we have fix Type 3 == 3D Position
        # Search for fix Type
        print("Fix Type...")
        if data.columns.str.contains('Type').any():
            type_key = data.columns.str.contains('Type')
            typ = data.columns[np.where(type_key)]
            typ = typ[0]
        print("OK")
        
        #Searching for pDOP
        print("pDOP...")
        if data.columns.str.contains('pDop').any():
            self.pDop_bool = True
        print("OK")
        
        #Searching for temp data
        print("Temperature...")
        if data.columns.str.contains('ambient').any():
            self.temp_bool = True
            print("Temperature data found!")
        else:
            print("No Temperature data found in file!")
        
        
        #Add microeconds to date
        a = data[self.itow] % 1000
        a = a.astype(str)

        newdate = data[self.utc] + ":" + a
        data["date[ms]"] = newdate
        # Convert date column to date object
        data['timestamp'] = pd.to_datetime(data["date[ms]"], format='%Y:%m:%d:%H:%M:%S:%f')
        
        # Convert GPS Data
        if (data['longitude[deg]'] > 90).any():
            data['longitude[deg]'] = data['longitude[deg]'] / 10000000
        if (data['latitude[deg]'] > 180).any():
            data['latitude[deg]'] = data['latitude[deg]'] / 10000000
            print("Converted Lat / Long ... ")
        #data['height[m]'] = data['height[mm]'] / 1000
               
        self.data = data
        print("Temp finished!")
        
    def plot_temp(self, real_temp=1000, saveplot=False):
        """Plots the temperature data contained in the file
        Parameters:
            - real_temp = will be plotted as a horizontal line, when one has 
            information about the real or reference temperature
            - saveplot = (default=False) saves plot in working dir
        """
        
        linewidth = 1
        fig, ax = plt.subplots()
        
        if self.temp_bool:            
            date = dates.date2num(self.data["timestamp"])
            # Define the date format
            date_form = dates.DateFormatter("%H:%M:%S")
            ax.xaxis.set_major_formatter(date_form)
            #ax.xaxis.set_major_locator(dates.MinuteLocator(byminute=(0, 30)))
            #ax.xaxis.set_minor_locator(dates.MinuteLocator(byminute=(15, 45)))
                        
            ax.plot(date, self.data['ambientTemp[C]'], color='orange', label='Ambient Temp.', linewidth=linewidth)
            ax.plot(date, self.data['objectTemp[C]'], color='blue', label='Object Temp.', linewidth=linewidth)

            if real_temp < 1000:
                ax.axhline(real_temp, 0, max(date), color='g', linewidth=linewidth+0.5, label='Measured temp.')
                ax.set_ylim([real_temp -2, max(self.data['ambientTemp[C]'])+2])
            ax.set_xlabel('Time [UTC]')
            ax.set_ylabel('Temperature [°C]')
            ax.set_title('Temperature development')
            ax.grid()
            ax.legend()
            if saveplot:
                fig.savefig(self.work_dir + '/' + self.file[:-4] + '_temperatures.png', dpi=300)
            
        else:
            print("No temperature data found!")
            
    def calc_E(self, real_temp, saveplot=False):
        """
        Calculates the Emmissivity for the object, depending on the ambient temperature
        Parameters:
            - real_temp = will be plotted as a horizontal line, when one has 
            information about the real or reference temperature
            - saveplot = (default=False) saves plot in working dir
        Output:
            - linear prediction parameters for ambient temp.
            - linear prediction parameters for object temp.
            - linear prediction parameters for error
        """
        E = ((self.data['objectTemp[C]'] + 273.15)**4 - (self.data['ambientTemp[C]'] + 273.15)**4) / ((real_temp + 273.15)**4 - (self.data['ambientTemp[C]'] + 273.15)**4)
        print("Mean Value of E = ",  np.mean(E))
        
        # Approximate line through points
        predict_ambient = np.poly1d(np.polyfit(self.data['ambientTemp[C]'], E, 1))
        predict_object = np.poly1d(np.polyfit(self.data['objectTemp[C]'], E, 1))
        
        #print("Polynoms: ", predict)
        e_amb = predict_ambient(self.data['ambientTemp[C]'])
        e_obj = predict_object(self.data['objectTemp[C]'])
        
        real_t_amb = (((self.data['objectTemp[C]'] + 273.15)**4 - (self.data['ambientTemp[C]'] + 273.15)**4) / e_amb + (self.data['ambientTemp[C]'] + 273.15)**4)**(1/4) - 273.15
        real_t_obj = (((self.data['objectTemp[C]'] + 273.15)**4 - (self.data['ambientTemp[C]'] + 273.15)**4) / e_obj + (self.data['ambientTemp[C]'] + 273.15)**4)**(1/4) - 273.15
        
        c = (self.data['objectTemp[C]'] - real_temp) / self.data['ambientTemp[C]'] # Error / Ambient = c
        predict_c = np.poly1d(np.polyfit(self.data['ambientTemp[C]'], c, 2))
        print("Mean value c = ", np.mean(c))
        
        #Plot correltaiton Ambient Temp - E
        fig, ax = plt.subplots()
        ax.scatter(self.data['ambientTemp[C]'], E, marker='x', s=0.5, color='b', label='Emessivity')
        ax.plot(self.data['ambientTemp[C]'], e_amb, color='r')
        ax.set_title('Correlation Abmient Temp. - Emissivity')
        ax.set_xlabel('Ambient Temperature [°C]')
        ax.set_ylabel('Emissivity')
        if saveplot:
            fig.savefig(self.work_dir + '/' + self.file[:-4] + '_ambient_emissivity.png', dpi=300)
            
        #Plot correltaiton Object Temp - E
        fig, ax = plt.subplots()
        ax.scatter(self.data['objectTemp[C]'], E, marker='x', s=0.5, color='b')
        ax.plot(self.data['objectTemp[C]'], e_obj, color='r')
        ax.set_title('Correlation object Temp. - Emissivity')
        ax.set_xlabel('Object Temperature [°C]')
        ax.set_ylabel('Emissivity')
        if saveplot:
            fig.savefig(self.work_dir + '/' + self.file[:-4] + '_object_emissivity.png', dpi=300)
            
        #Plot correltaiton Error - Ambient
        fig, ax = plt.subplots()
        ax.scatter(self.data['ambientTemp[C]'], (self.data['objectTemp[C]'] - real_temp), marker='o', s=0.5, color='g', label='c') #ToDO: make plot error over ambient
        ax.set_title('Correlation object temp. error - ambient temp.')
        ax.set_xlabel('Ambient Temperature [°C]')
        ax.set_ylabel('Temp. Error [°C]')
        if saveplot:
            fig.savefig(self.work_dir + '/' + self.file[:-4] + '_object_emissivity.png', dpi=300)
        
        # Plot processed Temperature, mean value and measured value
        fig, ax = plt.subplots()
        
        date = dates.date2num(self.data[self.utc])
        # Define the date format
        date_form = dates.DateFormatter("%H:%M:%S")
        ax.xaxis.set_major_formatter(date_form)
        ax.xaxis.set_major_locator(dates.MinuteLocator(byminute=(0, 30)))
        ax.xaxis.set_minor_locator(dates.MinuteLocator(byminute=(15, 45)))
        
        ax.plot(date, real_t_amb, label='IR Object Temp with ambient corr.', color='r')
        ax.plot(date, real_t_obj, label='IR Object Temp with object corr.', color='b')
        ax.axhline(np.mean(real_t_amb),0,100000, color='r', label='mean value amb corr')
        ax.axhline(np.mean(real_t_obj),0,100000, color='g', label='mean value obj corr')
        ax.axhline(np.mean(real_temp),0,100000, color='black', label='mean temp. measured', linestyle='--')
        ax.set_xlabel=('Index')
        ax.set_ylabel=('Temperature [°C]')
        ax.legend()
        if saveplot:
            fig.savefig(self.work_dir + '/' + self.file[:-4] + '_IR_temperatures.png', dpi=300)

        return predict_ambient, predict_object, predict_c
    
    def calc_temp(self):
        """
        Calculates the real snow temperature with a given emmissivity of snow
        default: emissivity_snow = 1
        """
        
        emissivity_snow = 1
        #e_amb = predict_amb(self.data_pos['ambientTemp[C]'])
        #e_obj = predict_obj(self.data_pos['objectTemp[C]'])
        #e_amb = predict(self.data_pos['objectTemp[C]'] + self.data_pos['ambientTemp[C]']) / (self.data_pos['objectTemp[C]'] - self.data_pos['ambientTemp[C]'])
        #real_t_amb = (((self.data_pos['objectTemp[C]'] + 273.15)**4 - (self.data_pos['ambientTemp[C]'] + 273.15)**4) / e_amb + (self.data_pos['ambientTemp[C]'] + 273.15)**4)**(1/4) - 273.15
        #real_t_obj = (((self.data_pos['objectTemp[C]'] + 273.15)**4 - (self.data_pos['ambientTemp[C]'] + 273.15)**4) / e_obj + (self.data_pos['ambientTemp[C]'] + 273.15)**4)**(1/4) - 273.15
        temp_corr = (((self.data_pos['objectTemp[C]'] + 273.15)**4 - (self.data_pos['ambientTemp[C]'] + 273.15)**4) / emissivity_snow + (self.data_pos['ambientTemp[C]'] + 273.15)**4)**(1/4) - 273.15
        
        
        #t_c = self.data_pos['objectTemp[C]'] - 0.5573355725160639 * self.data_pos['ambientTemp[C]']
        #emm_folie = (temp_corr**4 - self.data_pos['ambientTemp[C]']**4)
        
        #c_predicted = predict_c(self.data_pos['ambientTemp[C]'])
        #t_c_predict = self.data_pos['objectTemp[C]'] - c_predicted * self.data_pos['ambientTemp[C]']
        #with emissivity = 0.9
        #t_c_em = temp_corr - 0.5573355725160639 * self.data_pos['ambientTemp[C]']
        #t_c_predict_em = real_t - c_predicted * self.data_pos['ambientTemp[C]']
        
# =============================================================================
#         # Measured data from 23.01.2022
#         self.data_pos_copy = self.data_pos
#         self.data_pos_copy['measuredTemp'] = np.nan
#         idx_start = np.where(self.data_pos_copy.iloc[:, 2] == '2022-01-23 07:27:00')[0][0]
#         idx_end = np.where(self.data_pos_copy.iloc[:, 2] == '2022-01-23 07:30:00')[0][0]
#         self.data_pos_copy['measuredTemp'].iloc[idx_start:idx_end] = -5.4
#         idx_start = np.where(self.data_pos_copy.iloc[:, 2] == '2022-01-23 07:30:00')[0][0]
#         idx_end = np.where(self.data_pos_copy.iloc[:, 2] == '2022-01-23 07:36:00')[0][0]
#         self.data_pos_copy['measuredTemp'].iloc[idx_start:idx_end] = -6.4
#         idx_start = np.where(self.data_pos_copy.iloc[:, 2] == '2022-01-23 07:36:00')[0][0]
#         idx_end = np.where(self.data_pos_copy.iloc[:, 2] == '2022-01-23 07:39:00')[0][0]
#         self.data_pos_copy['measuredTemp'].iloc[idx_start:idx_end] = -5.6
#         idx_start = np.where(self.data_pos_copy.iloc[:, 2] == '2022-01-23 07:39:00')[0][0]
#         idx_end = np.where(self.data_pos_copy.iloc[:, 2] == '2022-01-23 07:48:00')[0][0]
#         self.data_pos_copy['measuredTemp'].iloc[idx_start:idx_end] = -5.1
#         idx_start = np.where(self.data_pos_copy.iloc[:, 2] == '2022-01-23 07:48:00')[0][0]
#         idx_end = np.where(self.data_pos_copy.iloc[:, 2] == '2022-01-23 08:04:00')[0][0]
#         self.data_pos_copy['measuredTemp'].iloc[idx_start:idx_end] = -5.0
#         idx_start = np.where(self.data_pos_copy.iloc[:, 2] == '2022-01-23 08:04:00')[0][0]
#         idx_end = np.where(self.data_pos_copy.iloc[:, 2] == '2022-01-23 08:10:00')[0][0]
#         self.data_pos_copy['measuredTemp'].iloc[idx_start:idx_end] = -4.3
#         idx_start = np.where(self.data_pos_copy.iloc[:, 2] == '2022-01-23 08:10:00')[0][0]
#         idx_end = np.where(self.data_pos_copy.iloc[:, 2] == '2022-01-23 08:24:00')[0][0]
#         self.data_pos_copy['measuredTemp'].iloc[idx_start:idx_end] = 0
# =============================================================================
        
        
        #self.data_pos_copy.between_time('08:00:00', '08:05:00')
        
# =============================================================================
#         fig, ax = plt.subplots()
#         
#         date = dates.date2num(self.data_pos[self.utc])
#         # Define the date format
#         date_form = dates.DateFormatter("%H:%M")
#         #date_form = dates.DateFormatter("%m-%d")
#         ax.xaxis.set_major_formatter(date_form)
#         ax.xaxis.set_major_locator(dates.MinuteLocator(byminute=(0, 10, 20, 30, 40, 50)))
#         ax.xaxis.set_minor_locator(dates.MinuteLocator(byminute=(5, 15, 25, 35, 45, 55)))
#         
#         ax.plot(date, real_t_amb, label='emessivity amb correct.')
#         #ax.plot(date, real_t_obj, label='Temperatures from IR - Object')
#         #ax.plot(date, t_c, label='c mean')
#         ax.plot(date, t_c_predict, label='c_predicted')
#         #ax.plot(date, t_c_em, label='c mean; E=0.9')
#         ax.plot(date, t_c_predict_em, label='c_predicted; E=0.9')
#         ax.plot(date, self.data_pos_copy['measuredTemp'], label='Measured Temp.', color='black')
#         #ax.axhline(np.mean(real_t_amb),min(date),max(date), color='r', label='mean value')
#         #ax.axhline(np.mean(real_temp),0,100000, color='black', label='mean IR measured', linestyle='--')
#         ax.set_xlabel=('Time [UTC+1]')
#         ax.set_ylabel=('Temperature [°C]') # ToDo: Add measured temperatures
#         
#         ax.legend()
#         ax.grid(b=True, which='major', linestyle='-')
#         ax.grid(b=True, which='minor', linestyle='--')
#         plt.show()
# =============================================================================
        
    def get_snow_temp(self, e_snow=1, e_folie=0.45):
        """
        First try of calculating the real temperature with a given emmissivity 
        of the layer in between the measurement.
        Parameters:
            - e_snow (default=1)
            - e_folie (default=0.45), which is a folie for 2022 and a Germanium glas for 2023
        """
        
        self.data['corr.objectTemp[C]'] = (((self.data['objectTemp[C]'] + 273.15)**4 - (self.data['ambientTemp[C]'] + 273.15)**4) / e_snow + (self.data['ambientTemp[C]'] + 273.15)**4)**(1/4) - 273.15
        self.data['corr.snowTemp[C]'] = (((self.data['corr.objectTemp[C]'] + 273.15)**4 - (self.data['ambientTemp[C]'] + 273.15)**4) / e_folie + (self.data['ambientTemp[C]'] + 273.15)**4)**(1/4) - 273.15

        #self.data['corr.objectTemp[C]'] = (((self.data['objectTemp[C]'] + 273.15)**4 - (1-e_snow) * (self.data['ambientTemp[C]'] + 273.15)**4) / e_snow )**(1/4) - 273.15
        #self.data['corr.objectTemp[C]'] = self.data['objectTemp[C]'] - 0.5573355725160639 * self.data['ambientTemp[C]']

if __name__ == "__main__":
    # Example usage of the AvaNode_TEMP class
    temp_data = AvaNode_TEMP()
    temp_data.path = r"..\data\2023-03-15_Nordkette_avalanche\C10\GPS\ava230315_C10_seilbahn_gnss.txt"
    temp_data.read_data()
    temp_data.plot_temp()

